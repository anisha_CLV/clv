package Listner;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.relevantcodes.extentreports.ExtentTest;

import CommonModules.BaseClass;
import CommonModules.Configurations;
import CommonModules.ReportManager;
import CommonModules.Wait;

public class TestListner implements ITestListener{
	
	WebDriver driver=null;
	ExtentTest logger;

	@Override
	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		ReportManager.getReporter("").flush();
	}

	@Override
	public void onStart(ITestContext arg0) {
				
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult arg0) {
		System.out.println(arg0.getThrowable().toString());
		BaseClass.logfail(arg0.getThrowable().toString(),"");
		driver.quit();
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestStart(ITestResult arg0) {
		if(((HashMap)arg0.getParameters()[0]).containsKey("Browser"))
		{
		String browser = ((HashMap)arg0.getParameters()[0]).get("Browser").toString().toUpperCase();
		driver=BaseClass.getbrowser(browser);
		}
		else
		{
			driver=BaseClass.getbrowser("IE");
		}
		if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("QA"))
		{
		driver.get(Configurations.urlQA);
		}
		else
		{
			if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("RYDER"))
			{
				driver.get(Configurations.urlRyder);
			}
			if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("NYLAW"))
			{
				driver.get(Configurations.urlNylaw);
			}
			if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("MDMTA"))
			{
				driver.get(Configurations.urlMdmta);
			}
			if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("AMLJIA"))
			{
				driver.get(Configurations.urlAMLJIA);
			}
			if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("LACO"))
			{
				driver.get(Configurations.urlLACO);
			}

			if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("NMGSD"))
			{
				driver.get(Configurations.urlNMGSD);
			}
			if(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString().toUpperCase().contains("Townsend"))

			
			{
				driver.get(Configurations.urlTownsend);
			}
		}
		BaseClass.initElements();
		//System.out.println("Attribute: "+driver.findElement(By.id("customerCodeTextBox")).getAttribute("value"));
		if(arg0.getParameters().length>0)
		{
        BaseClass.setlogger(((HashMap)arg0.getParameters()[0]).get("TestName").toString(),
        		((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString());
        BaseClass.setDataBaseName(((HashMap)arg0.getParameters()[0]).get("CustomerCode").toString());
		}
}

	@Override
	public void onTestSuccess(ITestResult arg0) {
		driver.quit();
		BaseClass.endTest();
		Wait.waitFor(2);
	}
	
	

}
