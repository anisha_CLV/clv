package CommonModules;

import org.testng.annotations.Test;

import java.io.IOException;

import org.testng.annotations.DataProvider;

public class ClaimVisionDataProvider {
  
  @DataProvider(name="Login")
	public static Object[][] Login()
	{  
		return ExcelReader.getsheetdata("Login");
	}
  @DataProvider(name="IRF")
	public static Object[][] IRF()
	{  
		return ExcelReader.getsheetdata("IRF");
	}
  @DataProvider(name="Payment")
	public static Object[][] Payment()
	{  
		return ExcelReader.getsheetdata("Payment");
	}
  

  @DataProvider(name="Claim")
 	public static Object[][] Claim()
 	{  
 		return ExcelReader.getsheetdata("Claim");
 	}
  
  @DataProvider(name="Claimant")
	public static Object[][] Claimant()
	{  
		return ExcelReader.getsheetdata("Claimant");
	}
  
  @DataProvider(name="ClientStateSpecific")
	public static Object[][] ClientStateSpecific()
	{  
		return ExcelReader.getsheetdata("ClientStateSpecific");
	}
  
  @DataProvider(name="WageWorkRTW")
 	public static Object[][] WageWorkRTW()
 	{  
 		return ExcelReader.getsheetdata("WWRTW");
 	}
  
  @DataProvider(name="ClaimantIncident")
	public static Object[][] ClaimantIncident()
	{  
		return ExcelReader.getsheetdata("ClaimantIncident");
	}
  
  @DataProvider(name="EmployeIncident")
	public static Object[][] EmployeIncident()
	{  
		return ExcelReader.getsheetdata("EmployeIncident");
	}
  
  @DataProvider(name="MedicalTreatment")
	public static Object[][] MedicalTreatment()
	{  
		return ExcelReader.getsheetdata("MedicalTreatment");
	}
  
  @DataProvider(name="CompPolicyData")
	public static Object[][] CompPolicyData()
	{  
		return ExcelReader.getsheetdata("CompPolicy");
	}
  @DataProvider(name="ReleasePayment")
 	public static Object[][] ReleasePayment()
 	{  
 		return ExcelReader.getsheetdata("ReleasePayment");
 	}
  
  @DataProvider(name="FinancialSummary")
	public static Object[][] FinancialSummary()
	{  
		return ExcelReader.getsheetdata("FinancialSummary");
	}
  
  @DataProvider(name="ROB")
	public static Object[][] ROB()
	{  
		return ExcelReader.getsheetdata("ROB");
	}
  @DataProvider(name="Reserve")
	public static Object[][] Reserve()
	{  
		return ExcelReader.getsheetdata("Reserve");
	}
  @DataProvider(name="Vendor")
	public static Object[][] Vendor()
	{  
		return ExcelReader.getsheetdata("Vendor");
	}
  @DataProvider(name="SelectPaymentForCheck")
	public static Object[][] VendorMerge()
	{  
		return ExcelReader.getsheetdata("SelectPaymentForCheck");
	}
  
  @DataProvider(name="PostalCheckReversal")
	public static Object[][] PostalCheckReversal()
	{  
		return ExcelReader.getsheetdata("PostalCheckReversal");
	}
  
  @DataProvider(name="CheckWriting")
	public static Object[][] CheckWriting()
	{  
		return ExcelReader.getsheetdata("CheckWriting");

	}
}

