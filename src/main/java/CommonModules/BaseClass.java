package CommonModules;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import ClaimVisionPages.CheckWritingPage;
import ClaimVisionPages.ClaimPage;
import ClaimVisionPages.ClaimantIncidentPage;
import ClaimVisionPages.ClaimantPage;
import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.EmployeeIncidentPage;
import ClaimVisionPages.GroupSetupPage;
import ClaimVisionPages.IRFPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.MedicalTreatmentPage;
import ClaimVisionPages.PaymentPage;
import ClaimVisionPages.PaymentTransactionDetailPage;
import ClaimVisionPages.PostalCheckReversalPage;
import ClaimVisionPages.ROBPage;
import ClaimVisionPages.ReleasePaymentPage;
//import ClaimVisionPages.ReleasePaymentPage;
import ClaimVisionPages.ReservePage;

import ClaimVisionPages.SelectPaymentForCheckPage;
import ClaimVisionPages.VendorMergePage;
import ClaimVisionPages.VendorSetUpPage;
import ClaimVisionPages.WageWorkRTWPage;





public class BaseClass {
	static Select select=null;
	public static WebDriver driver=null;
	static WebElement element=null;
	static ExtentTest logger=null;
	static String databasename="";
	public static void setlogger(String testname, String customerCode)
	{
		
		logger=ReportManager.getReporter(customerCode).startTest(testname);		

	}
	public static void setDataBaseName(String customerCode)
	{
		switch(customerCode.toUpperCase())
		{
		case "PCISQA":
			databasename="QA_ClaimsVision";
			break;
		case "PCISRYDERQA":
			databasename="QA_Ryder";
			break;
		case "PCISAMLJIAQA":
			databasename="QA_Amljia";
			break;
		
		case "PCISALLSTATEQA":
			databasename="QA_Allstate";
			break;
		case "PCISMDMTAQA":
			databasename="QA_Mdmta";
			break;
		case "PCISNMGSDQA":
			databasename="QA_NMGSD";
			break;
		case "PCISNYLAWQA":
			databasename="QA_Nylaw";
			break;
		case "RYDERSIM":
			databasename="PreProdRyder";
			break;
		case "NMGSDSIM":
			databasename="PreProdNMGSD";
			break;
		case "NYLAWSIM":
			databasename="PreProdNYLaw";
			break;
		case "TOWNSEND":
			databasename="PreProdTownsend";
			break;
		case "MDMTASIM":
			databasename="PreProdMDMTA";
			break;
		case "AMLJIASIM":
			databasename="PreProdAMLJIA";
			break;
		case "LACO":
			databasename="PreProdLACO";
			break;
			default:
				logfail(logger, "CustomerCode should be valid", "CustomerCode is not valid: "+customerCode);
				break;
		}
	
	}
	
	public static void settext(WebElement element, String value) {
		
		System.out.println("Setting text: "+value);
		performaction("settext", element, value);
	}
		
	private static String formattext(String locator, Object...parameter) {
		// TODO Auto-generated method stub
		return MessageFormat.format(locator.replace("'", "''"), parameter);
	}
	private static WebElement getelement(String locator)
	{
            return driver.findElement(getBy(locator));
	}
	private static By getBy(String element)
	{
		String[] data=element.split("~");
		switch(data[0])
		{
		case "id":
		return By.id(data[1]);
		case "xpath":
			return By.xpath(data[1]);
		case "linkText":
			return By.linkText(data[1]);
		default:
			return null;
		}
		
	}
	
	
	public static WebDriver getbrowser(String browser)
	{
		if(browser.equalsIgnoreCase("IE")){
			System.out.println("IE browser function");
		DesiredCapabilities cap=DesiredCapabilities.internetExplorer();
		//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		cap.setCapability(InternetExplorerDriver.
		                 INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true); 
		//WebDriver driver = new InternetExplorerDriver(capabilities);
		 
		// Set ACCEPT_SSL_CERTS  variable to true
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability("initialBrowserUrl", "http://www.msn.com/?ocid=iehp");
					System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"//drivers//IEDriverServer.exe");
					try {
						Runtime.getRuntime().exec("taskkill /F /IM iexplore.exe");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					driver = new InternetExplorerDriver();
		}
		else if (browser.equalsIgnoreCase("Edge")){
			System.out.println("Edge browser function");
			System.setProperty("webdriver.edge.driver",System.getProperty("user.dir")+"//drivers//Edge//msedgedriver.exe");
			driver = new EdgeDriver();
		}
		else if (browser.equalsIgnoreCase("Chrome")){
			System.out.println("Chrome browser function");
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//drivers//chromedriver.exe");
			driver = new ChromeDriver();
		}
					driver.manage().window().maximize();
					return driver;
	}

	public static WebDriver getbrowserChrome()
	{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//drivers//chromedriver.exe");
				
					
					driver = new ChromeDriver();
					driver.manage().window().maximize();
					return driver;
	}
	
	public static WebDriver getbrowserEdge()
	{
			System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+"//drivers//Edge//msedgedriver.exe");
				
					
					driver = new EdgeDriver();
					driver.manage().window().maximize();
					return driver;
	}


		
	public static Integer generateRandomNumberAsInteger(int size) {
		return new Random().nextInt(size-1)+1;
	}
public static String getCurrentDatetimeNow()
	{ 
		  return  DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss").format(LocalDateTime.now()); 
	}
	public static String getCurrentDate()
	{ 
		  return LocalDate.now().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
	}
	public static String getCurrentDatePST()
	{ 
		  return LocalDate.now(ZoneId.of("America/Los_Angeles")).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
	}
	public static void endTest() {
		ReportManager.getReporter("").endTest(logger);
	}
	public static void logInfo(ExtentTest logger, String text, String data) {
		if (data.equals("")) {
			logger.log(LogStatus.INFO, text);
		} else {
			logger.log(LogStatus.INFO, text + ": <b>" + data + "</b>");
		}
	}
	public static void logInfo(String text, String data) {
		if (data.equals("")) {
			logger.log(LogStatus.INFO, text);
		} else {
			logger.log(LogStatus.INFO, text + ": <b>" + data + "</b>");
		}
	}

	public static void logpass(ExtentTest logger, String Expected, String Actual) {
		logger.log(LogStatus.PASS, "<b>Expected:</b>" + Expected + " <b>| Actual: </b> " + Actual + " <b><u><a href="
				+ TakeScreenshot.getscreenshot(driver) + " style='color:green'>  Pass</a></u></b>");
	}
	
	public static int daysBetweenDatesCycle(String date1, String date2, String CustomerCode) {
		int days=(((int)ChronoUnit.DAYS.between(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")),LocalDate.parse(date2, DateTimeFormatter.ofPattern("MM/dd/yyyy"))))+1);
		if(CustomerCode.toUpperCase().contains("NYLAW"))
		{
		do
		{
			if(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SATURDAY")||LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SUNDAY"))
			{
				days--;
			}
			date1=futuredatefromdate(date1, 1);
		}while(!date1.equals(date2));
		}
		return days;
		}

	public static void logfail(ExtentTest logger, String Expected, String Actual) {
		logger.log(LogStatus.FAIL, "<b>Expected:</b> " + Expected + " <b>| Actual: </b> " + Actual + " <b><u><a href="
				+ TakeScreenshot.getscreenshot(driver) + " style='color:red'>  Fail</a></u></b>");
	}
	public static void logfail(String Expected, String Actual) {
		logger.log(LogStatus.FAIL, "<b>Expected:</b> " + Expected + " <b>| Actual: </b> " + Actual + " <b><u><a href="
				+ TakeScreenshot.getscreenshot(driver) + " style='color:red'>  Fail</a></u></b>");
	}
	
	public static void logskip(String Actual) {
		logger.log(LogStatus.SKIP, Actual + " <b><u><a href="
				+ TakeScreenshot.getscreenshot(driver) + " style='color:blue'>Skip</a></u></b>");
	}
	
	
	public static void switchtoFrame(String locator){
		Wait.waitFor(3);
		performaction("switchtoframe",locator);
		
	}
	
	public static List<String> getqueryresult(String query) {
		Connection conn;
		ResultSet set = null;
		List<String> list=new ArrayList<>();
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conn = DriverManager.getConnection("jdbc:sqlserver://192.168.81.103", "pcisqa", "sP7ep41OWTDqqChhj5fD");
			System.out.println("connected");
			Statement stmt = null;
			System.out.println("Use "+databasename+";"+query);
            
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

			set = stmt.executeQuery("Use "+databasename+";"+query);
			set.last();
			System.out.println(set.getRow());
			if(set.getRow()>1)
			{
			set.absolute(BaseClass.generateRandomNumberAsInteger(set.getRow()));
			}
			else
			{
				set.absolute(1);
			}
			for(int i=1;i<=set.getMetaData().getColumnCount();i++)
			{
				if(set.getString(i)==null)
				{
					list.add("");
				}
				else
				{
					list.add(set.getString(i));
				}
			}
		} catch (Exception e) {
			Assert.fail("Error in query execution: "+query);
			
		}
		finally
		{
			System.out.println(list.size());
			return list;
		}
	}

	public static List<String> getCompletequeryresult(String query) {
		Connection conn;
		ResultSet set = null;
		List<String> list=new ArrayList<>();
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conn = DriverManager.getConnection("jdbc:sqlserver://192.168.81.103", "pcisqa", "sP7ep41OWTDqqChhj5fD");
			System.out.println("connected");
			Statement stmt = null;
			System.out.println("Use "+databasename+";"+query);
            
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

			set = stmt.executeQuery("Use "+databasename+";"+query);
			//System.out.println(set);
			//set.last();
			//System.out.println(set.getMetaData().getColumnCount());
			while(set.next())
			{
				for(int i=1;i<=set.getMetaData().getColumnCount();i++)
			
				{
					if(set.getString(i)==null)
					{
						list.add("");
					}
					else
					{
						list.add(set.getString(i));
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Error in query execution: "+query);
			
		}
		finally
		{
			System.out.println(list.size());
			return list;
		}
	}

	
	public static String getcurrencyvalue(Double value)
	{   
		String currency=value.toString();
		if(currency.split("\\.")[0].length()>3)
		{
			currency=currency.split("\\.")[0].substring(0,currency.split("\\.")[0].length()-3)+","+currency.split("\\.")[0].substring(currency.split("\\.")[0].length()-3)+"."+currency.split("\\.")[1];
		}
		return "$"+currency;
	}
	
	public static double getamount(String value)
	{
		return Double.parseDouble(value.replace(",", "").replace("$", ""));
	}
	public static String getModifiedDate(String date)
	{
		date = date.split(" ")[0];
		date = date.split("-")[1]+ "/"
				+date.split("-")[2]+ "/"
				+date.split("-")[0];
		return date;
	}
	
	public static String generatedateforrange(String fromdate, String todate) {
		   final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			  
	        final LocalDate fromdatevalue = LocalDate.parse(fromdate, formatter);
	        final LocalDate todatevalue = LocalDate.parse(todate, formatter);
		int days = (int)ChronoUnit.DAYS.between(fromdatevalue, todatevalue);
     System.out.println(days);
		fromdatevalue.plusDays(generateRandomNumberAsInteger(days));
		return fromdatevalue.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
	}
	
	
	public static String pastdatefromdate(String date, int days) {
		if (date.equals("")) {
			return LocalDate.now().minusDays(days).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		} else {
			LocalDate newdate = LocalDate.of(Integer.parseInt(date.split("/")[2]), Integer.parseInt(date.split("/")[0]),
					Integer.parseInt(date.split("/")[1])).minusDays(days);
			return newdate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		}
	}
	public static String pastdatefromdate(String date, int years,int days) {
		if (date.equals("")) {
			return LocalDate.now().minusYears(years).minusDays(days).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		} else {
			LocalDate newdate = LocalDate.of(Integer.parseInt(date.split("/")[2]), Integer.parseInt(date.split("/")[0]),
					Integer.parseInt(date.split("/")[1])).minusYears(years).minusDays(days);
			return newdate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		}
	}
	public static String futuredatefromdate(String date, int days) {
		if (date.equals("")) {
			return LocalDate.now().plusDays(days).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		} else {
			LocalDate newdate = LocalDate.of(Integer.parseInt(date.split("/")[2]), Integer.parseInt(date.split("/")[0]),
					Integer.parseInt(date.split("/")[1])).plusDays(days);
			return newdate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
		}
	}
	public static String pastdatefromTodayDate(int days) {
		
		return LocalDate.now().minusDays(days).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
}
	
	public static String pastdatefromTodayDate(int year,int days) {
		
		return LocalDate.now().minusYears(year).minusDays(days).format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
}	
	public static String randDouble(int min, int max) {

      double randomValue = min + (max - min) * new Random().nextDouble();
		DecimalFormat df = new DecimalFormat("####0.00");
		return  df.format(randomValue).toString();
	}
	public static String enterRandomNumber(int count) {
		final String temp = RandomStringUtils.randomNumeric(count);
		return temp;

	}
	
	public static void clickRandomWebElement(WebElement w1, WebElement w2) {
		ArrayList<WebElement> webElements = new ArrayList<WebElement>();
		webElements.add(w1);
		webElements.add(w2);
		int count = new Random().nextInt(2);
		while (!webElements.get(count).isSelected()) {
			webElements.get(count).click();

		}
	}
	public static int daysBetweenDates(String date1, String date2) {
		return (((int)ChronoUnit.DAYS.between(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")),LocalDate.parse(date2, DateTimeFormatter.ofPattern("MM/dd/yyyy"))))+1);
		}
	
	public static int selectRandomNumber(int w1, int w2) {
		ArrayList<Integer> randomnumber = new ArrayList<Integer>();
		randomnumber.add(w1);
		randomnumber.add(w2);
		return randomnumber.get(new Random().nextInt(2));
		
	}

	// Description : To generate random string of the size which is passed in
	// this method
	public static String stringGeneratorl(int size) {
		return RandomStringUtils.randomAlphabetic(size).toUpperCase();
	}
	public static Integer generateRandomNumberAsInteger(Integer from,Integer to) {
			return new Random().nextInt(to-from) + from;
	}

	public static String randInt(int min, int max) {

		int rand = (int) (min + (Math.random() * (max - min)));

		String randomValue = Integer.toString(rand);
		return randomValue;
	}
	
	public static String switchToNextWindow(String parent,String childTitle,int count) {
		Wait.waitforchildwindow(count);
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> iterate = windows.iterator();
		while (iterate.hasNext()) {
			String childWindow = iterate.next();

			if (!parent.equalsIgnoreCase(driver.getTitle().trim())) {
				driver.switchTo().window(childWindow);
				System.out.println(driver.getTitle());
				if(driver.getTitle().equals(childTitle))
				{
					return driver.getWindowHandle();
				}
			}
		}
		return "";

	}
	
	public void switchToAlert() {

		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();

		}

		catch (NoAlertPresentException e) {
			System.out.println(e.getMessage());
		}

	}

	public static String acceptalert() {
		String alerttext = "";
			Wait.waitForAlert(driver);
			Alert alert = driver.switchTo().alert();
			System.out.println(alert.getText());
			alerttext = alert.getText();

			alert.accept();

			logInfo("Alert should be displayed", "Alert is displayed with text: "+alerttext);

		logInfo("Alert is accepted","");
		return alerttext;
	}

	
	public static String getcurrentwindow() {
		return driver.getWindowHandle();
	}
	public static void switchToWindow(String window, Integer count) {
		Wait.waitforchildwindow(count);
		driver.switchTo().window(window);
		System.out.println(driver.getTitle());
		
	}
	public static void verifyText(String locator,String text,Object...parameter) {
		Assert.assertEquals(performaction("gettext",locator), text);
		logpass(logger, "Value for "+locator+" should be: <b>"+text+"</b>", "Value for "+locator+" is: <b>"+text+"</b>");
		
		
	}
	
	public static void logpass(String expected, String actual)
	{
		logpass(logger, expected, actual);

	}
	public static void closeWindow() {
		driver.close();
		
	}
	public static String getTitle() {
		return driver.getTitle();
	}
	
	public static boolean verifyEnableElement(WebElement e) {
		return e.isEnabled();
	}

	private static Object performaction(Object... value) {
		int counter = 0;
		while (counter <= 20) {
			try {
				
				if(value[0].toString().equalsIgnoreCase("waitfornotcontainValue"))
				{
					if(!((WebElement) value[1]).getAttribute("value").equals(value[2].toString()))
					{
						return true;
					}
					else
					{
						throw new Exception();
					}
						
				}
				if(value[0].toString().equalsIgnoreCase("waitfortextpresent"))
				{
					return ((WebElement) value[1]).getText().length()!=0;
				}
				if (value[0].toString().equalsIgnoreCase("clear")) {
					((WebElement) value[1]).clear();
					return true;
				}
				if (value[0].toString().equalsIgnoreCase("click")) {
					((WebElement) value[1]).click();
					return true;
				}
				if (value[0].toString().equalsIgnoreCase("scroll")) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",((WebElement) value[1]));
				return true;
			}
				if (value[0].toString().equalsIgnoreCase("selectbyvisibletext")) {
					new Select((WebElement) value[1]).selectByVisibleText(value[2].toString());
					return true;
				}
				if (value[0].toString().equalsIgnoreCase("getalloptions")) {
					return new Select((WebElement) value[1]).getOptions();
		
				}
				if (value[0].toString().equalsIgnoreCase("selectbyindex")) {
					select = new Select((WebElement) value[1]);
					try {
						select.selectByIndex((Integer) value[2]);
						return true;
					} catch (ArrayIndexOutOfBoundsException e) {
						select.selectByIndex(generateRandomNumberAsInteger(select.getOptions().size()));
						return true;
					}
				}
				if (value[0].toString().equalsIgnoreCase("gettext")) {
					if(((WebElement) value[1]).getText().equals(""))
					{
						throw new Exception();
					}
					else
					{
					return ((WebElement) value[1]).getText();
					}
				}
				if (value[0].toString().equalsIgnoreCase("getattribute")) {
					return ((WebElement) value[1]).getAttribute(value[2].toString());
				}
				if (value[0].toString().equalsIgnoreCase("settext")) {
					((WebElement) value[1]).clear();
					((WebElement) value[1]).sendKeys(value[2].toString());
					Wait.waitFor(2);
					if(((WebElement) value[1]).getAttribute("value").length()==0 && !value[2].toString().equals(""))
					{
						throw new Exception();
					}
					return true;
				}
				if (value[0].toString().equalsIgnoreCase("getfirstselectedoption")) {
					return new Select((WebElement) value[1]).getFirstSelectedOption().getText();
				}
				if (value[0].toString().equalsIgnoreCase("getfirstselectedoptionvalue")) {
					return new Select((WebElement) value[1]).getFirstSelectedOption().getAttribute("value");
				}

			} catch (Exception e) {
				Wait.waitFor((long) 1);
				if(counter==20)
				{
					System.out.println(e);
				}
				counter++;
			}
		}
		Assert.fail("Not able to " + value[0].toString() + " for element ");
		return false;
	}
	public static String getfirstselectedoption(WebElement element) {
		return (String) performaction("getfirstselectedoption", element);
	}
	public static String getfirstselectedoptionvalue(WebElement element) {
		return (String) performaction("getfirstselectedoptionvalue", element);
	}

	public static void clear(WebElement element) {
		performaction("clear", element);
	}

	public static void Scrolltoelement(WebElement element) {
		performaction("scroll",element);
	}
	public static void click(WebElement element) {
		performaction("click", element);
	}
	public static void verifyTitle(String title) {
		Wait.waitForTitle(driver, title);
		Assert.assertTrue(driver.getTitle().equals(title));
		logpass("Title should be: "+title,"Title is: "+title);

	}
	public static void verifyElement(WebElement locator) {
		Wait.waitforelement(driver,locator);
	}
	
	public static void initElements()
	{
	   LoginPage.getinstance().initElement(driver);
	   DashboardPage.getinstance().initElement(driver);
	   IRFPage.getinstance().initElement(driver);
	   ClaimantIncidentPage.getinstance().initElement(driver);
	   EmployeeIncidentPage.getinstance().initElement(driver);
	   MedicalTreatmentPage.getinstance().initElement(driver);
	   
	   ClaimPage.getinstance().initElement(driver);
	   ClaimantPage.getinstance().initElement(driver);
	   ReservePage.getinstance().initElement(driver);
	   PaymentPage.getinstance().initElement(driver);
	   ROBPage.getinstance().initElement(driver);
	   
	   GroupSetupPage.getinstance().initElement(driver);

	   WageWorkRTWPage.getinstance().initElement(driver);
	   ReleasePaymentPage.getinstance().initElement(driver);
	   SelectPaymentForCheckPage.getinstance().initElement(driver);
	   VendorSetUpPage.getinstance().initElement(driver);
	   VendorMergePage.getinstance().initElement(driver);
	   PostalCheckReversalPage.getinstance().initElement(driver);
	   CheckWritingPage.getinstance().initElement(driver);
	   PaymentTransactionDetailPage.getinstance().initElement(driver);

	}
	public static void selectByIndex(WebElement element) {
		performaction("selectbyindex",element);
		
	}
	public static String getattribute(WebElement element, String attribute) {
		return(String) performaction("getattribute",element,attribute);
	}
	public static WebDriver getDriver() {
		return driver;
		
	}
	public static String gettext(WebElement element) {
       return (String) performaction("gettext",element);
	}
	public static void selectByVisibleText(WebElement element, String value) {
		
		performaction("selectbyvisibletext",element,value);
		
	}
	public static boolean isVisible(WebElement element) {
		try
		{
		return element.isDisplayed();
		}
		catch(Exception e)
		{
			return false;
		}
	}
	public static void waitForvaluePresent(WebElement element) {
		Wait.waitForValuePresent(driver, element);
		
	}
	
	public static String modifyDate(String date)
	{
		if(date.charAt(0)=='0')
		{
			date=date.substring(1);
			if(date.charAt(2)=='0')
			{
				date=date.substring(0,2)+date.substring(3);
			}
			
		}
		else
		{
			if(date.charAt(3)=='0')
			{
				date=date.substring(0,3)+date.substring(4);
			}
		}
		return date;
	}
	public static void selectByIndex(WebElement element, int value) {
		performaction("selectbyindex",element,value);
		
	}
	
	public static int daysBetweenDatesWithoutWeekend(String date1, String date2) {
		int days=(((int)ChronoUnit.DAYS.between(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")),LocalDate.parse(date2, DateTimeFormatter.ofPattern("MM/dd/yyyy"))))+1);
		do
		{
			if(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SATURDAY")||LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SUNDAY"))
			{
				days--;
			}
			date1=futuredatefromdate(date1, 1);
		}while(!date1.equals(date2));
		return days;
		}
	
	public static int daysBetweenDatesExcludeHoliday(String date1, String date2, List<String> holidays) {
		int days=(((int)ChronoUnit.DAYS.between
				(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")),
						LocalDate.parse(date2, DateTimeFormatter.ofPattern("MM/dd/yyyy"))))+1);
		do
		{
			if(holidays.contains(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString()))
				{
					days--;
				}
			
			/*else if(noOfDaysWorked == "5"){
				if(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SATURDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SUNDAY"))
				{
					days--;
				}
			}
			else if(noOfDaysWorked == "4"){
				if(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("FRIDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SATURDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SUNDAY"))
				{
					days--;
				}
			}
			else if(noOfDaysWorked == "3"){
				if(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("THURSDAY")
						||LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("FRIDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SATURDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SUNDAY"))
				{
					days--;
				}
			}
			else if(noOfDaysWorked == "2"){
				if(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("WEDNESDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("THURSDAY")
						||LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("FRIDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SATURDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SUNDAY"))
				{
					days--;
				}
			}
			else if(noOfDaysWorked == "1"){
				if(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("TUESDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("WEDNESDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("THURSDAY")
						||LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("FRIDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SATURDAY")||
						LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")).getDayOfWeek().toString().equals("SUNDAY"))
				{
					days--;
				}
			}*/
			date1=futuredatefromdate(date1, 1);
		}while(!date1.equals(date2));
		
		return days;
	}
	
		public static int daysBetweenDatesWithWeekend(String date1, String date2) {
			return (((int)ChronoUnit.DAYS.between(LocalDate.parse(date1, DateTimeFormatter.ofPattern("MM/dd/yyyy")),LocalDate.parse(date2, DateTimeFormatter.ofPattern("MM/dd/yyyy"))))+1);
			}
	public static void waitFortextPresent(WebElement element) {
		performaction("waitfortextpresent",element);
		
	}
	public static void waitForNotContainValue(WebElement element, String value) {
		performaction("waitfornotcontainValue",element,value);
		
	}
	
	@SuppressWarnings("unchecked")
	public static List<WebElement> getAllOptions(WebElement element)
	{
		Wait.waitFor(2);
		return (List<WebElement>)performaction("getalloptions",element);
	}


}
	
