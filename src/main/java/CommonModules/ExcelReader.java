package CommonModules;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
 
 public static Object[][] getsheetdata(String sheetname){
		Object[][] data = null;
		HashMap<String, String> hm;
		try
		{
		File file = new File(System.getProperty("user.dir")+"//Utilities//testdata.xlsx");

		FileInputStream filestream = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(filestream);
		XSSFSheet sheet = workbook.getSheet(sheetname);

		int rowcount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		List<HashMap> list = new ArrayList<HashMap>();

		Row row = sheet.getRow(0);

		int totalrecords = row.getLastCellNum();

		int count = 0;
		for (int currentrowvalue = 1; currentrowvalue <= rowcount; currentrowvalue++) {
			hm = new HashMap<String, String>();

			for (int i = 0; i < totalrecords; i++) {
				try
				{
				hm.put(row.getCell(i).toString(), sheet.getRow(currentrowvalue).getCell(i).toString());
				}
				catch(Exception e)
				{
					hm.put(row.getCell(i).toString(),"");

				}

			}
			if (hm.get("Run").equalsIgnoreCase("YES") || hm.get("Run").equalsIgnoreCase("Login")) {
				list.add(hm);
			}

		}
		data = new Object[list.size()][1];

		Iterator itr = list.iterator();
		while (itr.hasNext()) {
			data[count][0] = itr.next();
			count++;
		}
		workbook.close();
		return data;
		}
		catch(IOException e)
		{
			System.out.println("Unable to Read DataFile");
			return null;
		}

	}

 }