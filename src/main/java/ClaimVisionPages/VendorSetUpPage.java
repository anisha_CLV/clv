package ClaimVisionPages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.TakeScreenshot;
import CommonModules.Wait;

public class VendorSetUpPage {


	private VendorSetUpPage() {
	}

	private static final VendorSetUpPage vendorSetup = new VendorSetUpPage();

	public static VendorSetUpPage getinstance() {
		return vendorSetup;
	}

	public void initElement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "VendorSetup_taxIDTextBox")
	private WebElement VendorNumber;
	
	@FindBy(id = "VendorSetup_childVendorNameTextBox")
	private WebElement vendorName;
	
	@FindBy(id = "VendorSetup_firstMailAddressTextBox")
	private WebElement mailingAddress1;
	
	@FindBy(id = "VendorSetup_secondMailAddressTextBox")
	private WebElement mailingAddress2;
	
	@FindBy(id = "VendorSetup_mailCityDropDownList")
	private WebElement mailingCity;
	
	@FindBy(id = "VendorSetup_mailStateDropDown")
	private WebElement mailingState;
	
	@FindBy(id = "VendorSetup_mailPostalTextBox")
	private WebElement mailingPostal;
	
	@FindBy(id = "VendorSetup_saveButton")
	private WebElement save;
	
	@FindBy(id = "VendorSetup_topValidationSummary")
	private WebElement summaryArea;
	
	@FindBy(id = "VendorSetup_taxIdTypeDropDown")
	private WebElement taxIDType;
	
	@FindBy(id = "VendorSetup_dateSentTextBox")
	private WebElement dateW9Sent;
	@FindBy(id = "VendorSetup_dateReceiveTextBox")
	private WebElement dateW9Received;
	@FindBy(id = "VendorSetup_noticesButton")
	private WebElement notices;
	@FindBy(id = "VendorSetup_billerTextBox")
	private WebElement vendor;
	@FindBy(id = "VendorSetup_lastOFACDateTextBox")
	private WebElement LastOFACDate;
	@FindBy(id = "VendorSetup_childCrossRefTextBox")
	private WebElement crossRef;
	@FindBy(id = "VendorSetup_childContactFirstNameTextBox")
	private WebElement contactFirstName;
	@FindBy(id = "VendorSetup_childContactMiddleNameTextBox")
	private WebElement contactMiddleName;
	@FindBy(id = "VendorSetup_childContactLastNameTextBox")
	private WebElement contactLastName;
	@FindBy(id = "VendorSetup_childPhoneTextBox")
	private WebElement phone;
	@FindBy(id = "VendorSetup_childFaxTextBox")
	private WebElement fax;
	@FindBy(id = "VendorSetup_childEmailTextBox")
	private WebElement email;
	@FindBy(id = "VendorSetup_childIrsTypeDropDown")
	private WebElement irsType;
	@FindBy(id = "VendorSetup_childNameTextBox")
	private WebElement name;
	@FindBy(id = "VendorSetup_childDbaTextBox")
	private WebElement dba;
	@FindBy(id = "VendorSetup_isMailForeignAddressCheckBox")
	private WebElement foreignAddress;
	@FindBy(id = "VendorSetup_sameAsParentCheckBox")
	private WebElement sameAsParent;
	
	@FindBy(id = "VendorSetup_sameAsMailingCheckBox")
	private WebElement sameAsMailing;
	@FindBy(id = "VendorSetup_firstPhysicalAddressTextBox")
	private WebElement physicalAddress1;
	@FindBy(id = "VendorSetup_secondPhysicalAddressTextBox")
	private WebElement physicalAddress2;
	@FindBy(id = "VendorSetup_physicalCityDropDownList")
	private WebElement physicalCity;
	@FindBy(id = "VendorSetup_physicalStateDropDown")
	private WebElement physicalState;
	@FindBy(id = "VendorSetup_physicalPostalTextBox")
	private WebElement physicalPostal;
	
	
	@FindBy(id = "VendorSetup_firstMailForeignAddressTextBox")
	private WebElement mailingForeignAddress1;
	@FindBy(id = "VendorSetup_secondMailForeignAddressTextBox")
	private WebElement mailingForeignAddress2;
	@FindBy(id = "VendorSetup_MailingForeignCity")
	private WebElement mailingForeignCity;
	@FindBy(id = "VendorSetup_MailingForeignCounty")
	private WebElement mailingForeignCounty;
	@FindBy(id = "VendorSetup_MailingForeignStateDDL")
	private WebElement mailingForeignState;
	@FindBy(id = "VendorSetup_MailingForeignZiCode")
	private WebElement mailingForeignZip;
	@FindBy(id = "VendorSetup_MailingForeignCountryDDL")
	private WebElement mailingForeignCountry;
	
	@FindBy(id = "VendorSetup_firstPhysicalForeignAddress")
	private WebElement physicalForeignAddress1;
	@FindBy(id = "VendorSetup_secondPhysicalForeignAddressTextBox")
	private WebElement physicalForeignAddress2;
	@FindBy(id = "VendorSetup_PhysicalForeignCity")
	private WebElement physicalForeignCity;
	@FindBy(id = "VendorSetup_PhysicalForeignCounty")
	private WebElement physicalForeignCounty;
	@FindBy(id = "VendorSetup_PhysicalForeignStateDDL")
	private WebElement physicalForeignState;
	@FindBy(id = "VendorSetup_PhysicalForeignZi")
	private WebElement physicalForeignZip;
	@FindBy(id = "VendorSetup_PhysicalForeignCountryDDL")
	private WebElement physicalForeignCountry;
	@FindBy(id = "VendorSetup_isUnauthorizedCheckBox")
	private WebElement unauthorized;
	@FindBy(id = "VendorSetup_isDeactivateCheckBox")
	private WebElement deactivate;
	@FindBy(id = "VendorSetup_VendorNoteTextBox")
	private WebElement note;
	@FindBy(id = "VendorSetup_VendorMiscTypeDropDownList")
	private WebElement miscType;
	@FindBy(id = "VendorSetup_VendorMiscTypeDescDropDownList")
	private WebElement miscDescription;
	@FindBy(id = "VendorSetup_ExcludeFromIRSReportingCheckBox")
	private WebElement excludeFromIRS;
	@FindBy(id = "VendorSetup_altVendorNumberTextBox")
	private WebElement altVendorNumber;
	@FindBy(id = "VendorSetup_medicalProviderCheckBox")
	private WebElement medicalProvider;
	@FindBy(id = "VendorSetup_additionalNamesCheckBox")
	private WebElement additionalNames;
	@FindBy(id = "paymentModesCheckBox")
	private WebElement paymentModes;
	@FindBy(id = "VendorSetup_cancelButton")
	private WebElement cancel;
	@FindBy(id = "VendorSetup_newVendorButton")
	private WebElement newVendor;
	@FindBy(id = "VendorSetup_vendorTypeDropDown")
	private WebElement vendorType;
	
    @FindBy(id="messageLabel")
    private WebElement message;
    
    @FindBy(id="closeButton")
    private WebElement closeButton;
    
    @FindBy(id="VendorSetup_nationalIdentifierTextBox")
    private WebElement nationalProviderIdentifier;
    
    @FindBy(id="VendorSetup_ProviderTypeDropList")
    private WebElement providerType;
    @FindBy(id="VendorSetup_addSpecialtyButton")
    private WebElement addSpeciality;
    @FindBy(id="VendorSetup_specialtyDataList_ctl00_specialtyDropDown")
    private WebElement specialty1;
    @FindBy(id="VendorSetup_specialtyDataList_ctl01_specialtyDropDown")
    private WebElement specialty2;
    @FindBy(id="VendorSetup_specialtyDataList_ctl00_boardCertifiedCheckBox")
    private WebElement boardCertificate1;
    @FindBy(id="VendorSetup_specialtyDataList_ctl01_boardCertifiedCheckBox")
    private WebElement boardCertificate2;
    @FindBy(id="VendorSetup_specialtyDataList_ctl00_boardEligibleCheckBox")
    private WebElement boardEligible1;
    @FindBy(id="VendorSetup_specialtyDataList_ctl01_boardEligibleCheckBox")
    private WebElement boardEligible2;
    @FindBy(id="VendorSetup_licenseTextBox")
    private WebElement license;
    @FindBy(id="VendorSetup_expirationDateTextBox")
    private WebElement expirationDate;
    @FindBy(id="VendorSetup_medicareProviderTextBox")
    private WebElement medicalProviderText;
    @FindBy(id="VendorSetup_deaTextBox")
    private WebElement DEA;
    @FindBy(id="VendorSetup_medicalSchoolTextBox")
    private WebElement medicalSchool;
    @FindBy(id="VendorSetup_deaExpirationDateTextBox")
    private WebElement DEAExpirationDate;
    @FindBy(id="VendorSetup_unauthorizedCheckBox")
    private WebElement medicalUnauthorized;
    @FindBy(id="VendorSetup_wmbeCertifiedDropDown")
    private WebElement WMBECertified;
    @FindBy(id="VendorSetup_dateVerifiedTextBox")
    private WebElement dateVerified;
    @FindBy(id="VendorSetup_addAttorneyNameButton")
    private WebElement addAditionalName;
    @FindBy(id="VendorSetup_attorneyDataList_ctl01_attorneyFirstNameTextBox")
    private WebElement firstName1;
    @FindBy(id="VendorSetup_attorneyDataList_ctl02_attorneyFirstNameTextBox")
    private WebElement firstName2;
    @FindBy(id="VendorSetup_attorneyDataList_ctl01_attorneyMiddleNameTextBox")
    private WebElement middleName1;
    @FindBy(id="VendorSetup_attorneyDataList_ctl02_attorneyMiddleNameTextBox")
    private WebElement middleName2;
    @FindBy(id="VendorSetup_attorneyDataList_ctl01_attorneyLastNameTextBox")
    private WebElement lastName1;
    @FindBy(id="VendorSetup_attorneyDataList_ctl02_attorneyLastNameTextBox")
    private WebElement lastName2;
    @FindBy(id="VendorSetup_attorneyDataList_ctl01_attorneyPhoneTextBox")
    private WebElement phone1;
    @FindBy(id="VendorSetup_attorneyDataList_ctl02_attorneyPhoneTextBox")
    private WebElement phone2;
    @FindBy(id="VendorSetup_attorneyDataList_ctl01_attorneyFaxTextBox")
    private WebElement fax1;
    @FindBy(id="VendorSetup_attorneyDataList_ctl02_attorneyFaxTextBox")
    private WebElement fax2;
    @FindBy(id="VendorSetup_attorneyDataList_ctl01_attorneyEmailTextBox")
    private WebElement email1;
    @FindBy(id="VendorSetup_attorneyDataList_ctl02_attorneyEmailTextBox")
    private WebElement email2;
    @FindBy(id="paymentModeCheckBoxList_0")
    private WebElement computerPrintedCheckBox;
    @FindBy(id="paymentModeCheckBoxList_1")
    private WebElement journalVoucherCheckBox;
    @FindBy(id="VendorSetup_DefaultPaymentModeDropdown")
    private WebElement paymentMode;
    @FindBy(id="VendorSetup_accountNumber1TextBox")
    private WebElement accountNumber1;
    @FindBy(id="VendorSetup_accountNumber2TextBox")
    private WebElement accountNumber2;
    @FindBy(id="VendorSetup_accountInfoTextBox")
    private WebElement accountInfo;
    @FindBy(id="VendorSetup_bankAccountTypeTextBox")
    private WebElement accountType;
    

	
	
	
	
	
	
	public void saveVendor(boolean isInvalid)
	{
		BaseClass.click(save);
		if(!isInvalid)
		{
			Wait.waitFortextPresent(BaseClass.getDriver(), message);
			Assert.assertEquals(BaseClass.gettext(message), "Saved successfully.");
		}
	}
	
	public List<WebElement> getErrorMessages()
	{
		Wait.waitFortextPresent(BaseClass.getDriver(), summaryArea);
		return summaryArea.findElements(By.tagName("li"));
	}
	
	public void selectTaxIDType(String taxIDValue)
	{
		BaseClass.selectByVisibleText(taxIDType, taxIDValue);
		BaseClass.logInfo("TaxID Type is selected", BaseClass.getfirstselectedoption(taxIDType));
	}
	
	public void enterDateW9Sent()
	{
		BaseClass.settext(dateW9Sent, BaseClass.pastdatefromTodayDate(20));
		BaseClass.logInfo("Date W-9 Sent is entered as", BaseClass.getattribute(dateW9Sent, "value"));
	}
	public void enterDateW9Received()
	{
		BaseClass.settext(dateW9Received, BaseClass.pastdatefromTodayDate(10));
		BaseClass.logInfo( "Date W-9 Received is entered as", BaseClass.getattribute(dateW9Received, "value"));
	}
	
	public void enterVendorName()
	{
		BaseClass.settext(vendorName, BaseClass.stringGeneratorl(5)+ " " + BaseClass.stringGeneratorl(5));
		BaseClass.logInfo( "Vendor Name is entered as", BaseClass.getattribute(vendorName, "value"));
	}
	
	public void enterLastOFACDate()
	{
		BaseClass.settext(LastOFACDate, BaseClass.pastdatefromTodayDate(8));
		BaseClass.logInfo( "Last OFAC Date is entered as", BaseClass.getattribute(LastOFACDate, "value"));
	}
	
	public void enterCrossRef()
	{
		BaseClass.settext(crossRef, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo( "Cross Ref is entered as", BaseClass.getattribute(crossRef, "value"));
	}
	public void enterContactName()
	{
		BaseClass.settext(contactFirstName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo( "Contact First Name is entered as", BaseClass.getattribute(contactFirstName, "value"));
		BaseClass.settext(contactMiddleName, BaseClass.stringGeneratorl(1));
		BaseClass.logInfo( "Contact Middle Name is entered as", BaseClass.getattribute(contactMiddleName, "value"));
		BaseClass.settext(contactLastName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo( "Contact Last Name is entered as", BaseClass.getattribute(contactLastName, "value"));
	}
	public void enterPhone()
	{
		BaseClass.settext(phone, BaseClass.enterRandomNumber(10)+Keys.TAB);
		BaseClass.logInfo( "Phone # is entered as", BaseClass.getattribute(phone, "value"));
	}
	public void enterFax()
	{
		BaseClass.settext(fax, BaseClass.enterRandomNumber(10)+Keys.TAB);
		BaseClass.logInfo( "Fax # is entered as", BaseClass.getattribute(fax, "value"));
	}
	public void enterEmail()
	{
		BaseClass.settext(email, "test@abc.com");
		BaseClass.logInfo( "Email is entered as", BaseClass.getattribute(email, "value"));
	}
	public void selectIRSType()
	{
		BaseClass.selectByIndex(irsType);
		BaseClass.logInfo( "IRS Type is selected as", BaseClass.getfirstselectedoption(irsType));
	}
	public void enterName()
	{
		BaseClass.settext(name, BaseClass.stringGeneratorl(5)+ " " + BaseClass.stringGeneratorl(5));
		BaseClass.logInfo( "Name is entered as", BaseClass.getattribute(name, "value"));
	}
	
	public void enterDBA()
	{
		BaseClass.settext(dba, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo( "DBA is entered as", BaseClass.getattribute(dba, "value"));
	}
	public void enterMailingAddress(boolean isForeignAddres, boolean isSameAsParent)
	{
		if(isForeignAddres==true)
		{
			BaseClass.click(foreignAddress);
			BaseClass.settext(mailingForeignAddress1, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Mailing Foreign Address1 is entered as", BaseClass.getattribute(mailingForeignAddress1, "value"));
			BaseClass.settext(mailingForeignAddress2, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Mailing Foreign Address2 is entered as", BaseClass.getattribute(mailingForeignAddress2, "value"));
			BaseClass.settext(mailingForeignCity, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Mailing Foreign City is entered as", BaseClass.getattribute(mailingForeignCity, "value"));
			BaseClass.settext(mailingForeignCounty, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Mailing Foreign County is entered as", BaseClass.getattribute(mailingForeignCounty, "value"));
			BaseClass.selectByIndex(mailingForeignState);
			BaseClass.logInfo( "Mailing Foreign State is selected as", BaseClass.getfirstselectedoption(mailingForeignState));
			BaseClass.settext(mailingForeignZip, BaseClass.enterRandomNumber(5));
			BaseClass.logInfo( "Mailing Foreign Zip is entered as", BaseClass.getattribute(mailingForeignZip, "value"));
			BaseClass.selectByIndex(mailingForeignCountry);
			BaseClass.logInfo( "Mailing Foreign Country is selected as", BaseClass.getfirstselectedoption(mailingForeignCountry));
		}
		else
		{
			BaseClass.settext(mailingAddress1, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Mailing  Address1 is entered as", BaseClass.getattribute(mailingAddress1, "value"));
			BaseClass.settext(mailingAddress2, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Mailing  Address2 is entered as", BaseClass.getattribute(mailingAddress2, "value"));
			BaseClass.settext(mailingPostal,"12345"+Keys.TAB);
			BaseClass.logInfo( "Mailing  Postal is entered as", BaseClass.getattribute(mailingPostal, "value"));
			BaseClass.selectByIndex(mailingCity);
			BaseClass.logInfo( "Mailing  City is selected as", BaseClass.getfirstselectedoption(mailingCity));
			BaseClass.logInfo( "Mailing  State is selected as", BaseClass.getfirstselectedoption(mailingState));
		}
	}
	
	public void enterPhysicalAddress(boolean isSameAsMailing)
	{
		
		if(foreignAddress.isSelected())
		{
			if(isSameAsMailing)
			{
				BaseClass.click(sameAsMailing);
				Wait.waitForValuePresent(BaseClass.getDriver(), physicalForeignAddress1);
				Assert.assertEquals(BaseClass.getattribute(physicalForeignAddress1, "value"), BaseClass.getattribute(mailingForeignAddress1, "value"));
				Assert.assertEquals(BaseClass.getattribute(physicalForeignAddress2, "value"), BaseClass.getattribute(mailingForeignAddress2, "value"));
				Assert.assertEquals(BaseClass.getattribute(physicalForeignCity, "value"), BaseClass.getattribute(mailingForeignCity, "value"));
				Assert.assertEquals(BaseClass.getattribute(physicalForeignCounty, "value"), BaseClass.getattribute(mailingForeignCounty, "value"));
				Assert.assertEquals(BaseClass.getattribute(physicalForeignZip, "value"), BaseClass.getattribute(mailingForeignZip, "value"));
				Assert.assertEquals(BaseClass.getfirstselectedoption(physicalForeignState), BaseClass.getfirstselectedoption(mailingForeignState));
				Assert.assertEquals(BaseClass.getfirstselectedoption(physicalForeignCountry), BaseClass.getfirstselectedoption(mailingForeignCountry));
				BaseClass.logpass( "Physical Address Values should be updated same as Mailing Address", "Physical Address Values are updated same as Mailing Address");

			}
			else
			{
			BaseClass.settext(physicalForeignAddress1, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Physical Foreign Address1 is entered as", BaseClass.getattribute(physicalForeignAddress1, "value"));
			BaseClass.settext(physicalForeignAddress2, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Physical Foreign Address2 is entered as", BaseClass.getattribute(physicalForeignAddress2, "value"));
			BaseClass.settext(physicalForeignCity, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Physical Foreign City is entered as", BaseClass.getattribute(physicalForeignCity, "value"));
			BaseClass.settext(physicalForeignCounty, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "Physical Foreign County is entered as", BaseClass.getattribute(physicalForeignCounty, "value"));
			BaseClass.selectByIndex(physicalForeignState);
			BaseClass.logInfo( "Physical Foreign State is selected as", BaseClass.getfirstselectedoption(physicalForeignState));
			BaseClass.settext(physicalForeignZip, BaseClass.enterRandomNumber(5));
			BaseClass.logInfo( "Physical Foreign Zip is entered as", BaseClass.getattribute(physicalForeignZip, "value"));
			BaseClass.selectByIndex(physicalForeignCountry);
			BaseClass.logInfo( "Physical Foreign Country is selected as", BaseClass.getfirstselectedoption(physicalForeignCountry));
			}
		}
		else
		{
			if(isSameAsMailing)
			{
				BaseClass.click(sameAsMailing);
				Wait.waitForValuePresent(BaseClass.getDriver(), physicalAddress1);
				Assert.assertEquals(BaseClass.getattribute(physicalAddress1, "value"), BaseClass.getattribute(mailingAddress1, "value"));
				Assert.assertEquals(BaseClass.getattribute(physicalAddress2, "value"), BaseClass.getattribute(mailingAddress2, "value"));
				Assert.assertEquals(BaseClass.getattribute(physicalPostal, "value"), BaseClass.getattribute(mailingPostal, "value"));
				Assert.assertEquals(BaseClass.getfirstselectedoption(physicalCity), BaseClass.getfirstselectedoption(mailingCity));
				Assert.assertEquals(BaseClass.getfirstselectedoption(physicalState), BaseClass.getfirstselectedoption(mailingState));
				BaseClass.logpass( "Physical Address Values should be updated same as Mailing Address", "Physical Address Values are updated same as Mailing Address");
			}
			else
			{
			BaseClass.settext(physicalAddress1, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "physical  Address1 is entered as", BaseClass.getattribute(physicalAddress1, "value"));
			BaseClass.settext(physicalAddress2, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo( "physical  Address2 is entered as", BaseClass.getattribute(physicalAddress2, "value"));
			BaseClass.settext(physicalPostal,"12345"+Keys.TAB);
			BaseClass.logInfo( "physical  Postal is entered as", BaseClass.getattribute(physicalPostal, "value"));
			BaseClass.selectByIndex(physicalCity);
			BaseClass.logInfo( "physical  City is selected as", BaseClass.getfirstselectedoption(physicalCity));
			BaseClass.logInfo( "physical  State is selected as", BaseClass.getfirstselectedoption(physicalState));
			}
		}
	}
	
	public void setUnauthorizedYes()
	{
		if(!unauthorized.isSelected())
		{
			BaseClass.click(unauthorized);
			Assert.assertTrue(unauthorized.isSelected());
			BaseClass.logInfo( "Unauthorized checkbox is selected", "");
		}
	}
	public void setUnauthorizedYNo()
	{
		if(unauthorized.isSelected())
		{
			BaseClass.click(unauthorized);
			Assert.assertFalse(unauthorized.isSelected());
			BaseClass.logInfo( "Unauthorized checkbox is unselected", "");
		}
	}
	public void setDeactivateYes()
	{
		if(!deactivate.isSelected())
		{
			BaseClass.click(deactivate);
			Assert.assertTrue(deactivate.isSelected());
			BaseClass.logInfo( "Deactivate checkbox is selected", "");
		}
	}
	public void setDeactivateYNo()
	{
		if(deactivate.isSelected())
		{
			BaseClass.click(deactivate);
			Assert.assertFalse(deactivate.isSelected());
			BaseClass.logInfo( "Deactivate checkbox is unselected", "");
		}
	}
	
	public void setvendorType()
	{
		BaseClass.selectByIndex(vendorType);
		BaseClass.logInfo( "Vendor Type is selected as", BaseClass.getfirstselectedoption(vendorType));
	}
	
	public void setvendorType(String vendorTypeValue)
	{
		BaseClass.selectByVisibleText(vendorType, vendorTypeValue);
		BaseClass.logInfo( "Vendor Type is selected as", BaseClass.getfirstselectedoption(vendorType));
	}

	public void enterTaxIDValue() {
		String parent=BaseClass.getDriver().getWindowHandle();
	if(BaseClass.getfirstselectedoption(taxIDType).equals("SSN"))
	{
		BaseClass.settext(VendorNumber, BaseClass.enterRandomNumber(9)+Keys.TAB);
	}
	if(BaseClass.getfirstselectedoption(taxIDType).equals("TaxID(FEIN)"))
	{
		BaseClass.settext(VendorNumber, BaseClass.enterRandomNumber(9)+Keys.TAB);
	}
	Wait.waitFor(7);
	BaseClass.switchToNextWindow(parent, "Vendor Search",2);
	System.out.println(BaseClass.getDriver().getWindowHandles().size());
	System.out.println(BaseClass.getDriver().getTitle());
	BaseClass.click(closeButton);
	Wait.waitFor(3);
	BaseClass.getDriver().switchTo().window(parent);
	}

	public void newVendor() {
		BaseClass.click(newVendor);
		BaseClass.logInfo( "New Vendor button is clicked", "");
		
	}
	
	public void addMedicalProvider()
	{
		BaseClass.click(medicalProvider);
		BaseClass.logInfo( "Medical Provider Checkbox is clicked", "");
		BaseClass.settext(nationalProviderIdentifier, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo( "National Provider Identifier is entered as",BaseClass.getattribute(nationalProviderIdentifier, "value"));
		BaseClass.click(addSpeciality);
		BaseClass.logInfo( "Add Speciality button is clicked", "");
		BaseClass.selectByIndex(specialty1);
		BaseClass.logInfo( "Speciality is selected as",BaseClass.getfirstselectedoption(specialty1));
		BaseClass.click(boardCertificate1);
		BaseClass.logInfo( "Board Certificate Checkbox is clicked for First Specialty", "");
		BaseClass.click(boardEligible1);
		BaseClass.logInfo( "Board Eligible Checkbox is clicked for First Specialty", "");
		BaseClass.selectByIndex(specialty2);
		BaseClass.logInfo( "Speciality is selected as",BaseClass.getfirstselectedoption(specialty2));
		BaseClass.settext(license, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "License# is entered as",BaseClass.getattribute(license, "value"));
		BaseClass.settext(medicalProviderText, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "Medical Provider# is entered as",BaseClass.getattribute(medicalProviderText, "value"));
		BaseClass.settext(DEA, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "DEA# is entered as",BaseClass.getattribute(DEA, "value"));
		BaseClass.settext(medicalSchool, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "Medical School Identifier is entered as",BaseClass.getattribute(medicalSchool, "value"));
		BaseClass.settext(expirationDate, BaseClass.futuredatefromdate("", 2));
		BaseClass.logInfo( "Expiration Date is entered as",BaseClass.getattribute(expirationDate, "value"));
		BaseClass.settext(DEAExpirationDate, BaseClass.futuredatefromdate("", 2));
		BaseClass.logInfo( "DEA Expiration Date is entered as",BaseClass.getattribute(DEAExpirationDate, "value"));
		BaseClass.click(medicalUnauthorized);
		BaseClass.logInfo( "Medical Unauthorized? Checkbox is clicked", "");
	}
	
	public void addAdditionalNames()
	{
		BaseClass.click(additionalNames);
		BaseClass.logInfo( "Additional Names Checkbox is clicked", "");
		BaseClass.click(addAditionalName);
		BaseClass.logInfo( "Add Aditional Name button is clicked", "");
		BaseClass.settext(firstName1, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "First Name is entered as",BaseClass.getattribute(firstName1, "value"));
		BaseClass.settext(middleName1, BaseClass.stringGeneratorl(2));
		BaseClass.logInfo( "Middle Name is entered as",BaseClass.getattribute(middleName1, "value"));
		BaseClass.settext(lastName1, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "Last Name is entered as",BaseClass.getattribute(lastName1, "value"));
		BaseClass.settext(phone1, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo( "Phone Number is entered as",BaseClass.getattribute(phone1, "value"));
		BaseClass.settext(fax1, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo( "Fax Number is entered as",BaseClass.getattribute(fax1, "value"));
		BaseClass.settext(email1, "test1@abc.com");
		BaseClass.logInfo( "Email Address is entered as",BaseClass.getattribute(email1, "value"));
		BaseClass.settext(firstName2, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "First Name is entered as",BaseClass.getattribute(firstName2, "value"));
		BaseClass.settext(middleName2, BaseClass.stringGeneratorl(2));
		BaseClass.logInfo( "Middle Name is entered as",BaseClass.getattribute(middleName2, "value"));
		BaseClass.settext(lastName2, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "Last Name is entered as",BaseClass.getattribute(lastName2, "value"));
		BaseClass.settext(phone2, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo( "Phone Number is entered as",BaseClass.getattribute(phone2, "value"));
		BaseClass.settext(fax2, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo( "Fax Number is entered as",BaseClass.getattribute(fax2, "value"));
		BaseClass.settext(email2, "test2@abc.com");
		BaseClass.logInfo( "Email Address is entered as",BaseClass.getattribute(email2, "value"));
	}
	
	public void addPaymentModes()
	{
		BaseClass.click(paymentModes);
		BaseClass.logInfo( "Payment Modes Checkbox is clicked", "");
		BaseClass.click(computerPrintedCheckBox);
		BaseClass.logInfo( "Computer Printed/Computer Printed Checkbox is clicked", "");
		BaseClass.click(journalVoucherCheckBox);
		BaseClass.logInfo( "Journal Voucher/Journal Voucher Checkbox is clicked", "");
		BaseClass.selectByIndex(paymentMode);
		BaseClass.logInfo( "Payment Mode is selected as",BaseClass.getfirstselectedoption(paymentMode));
		BaseClass.settext(accountNumber1, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo( "Account Number 1 is entered as",BaseClass.getattribute(accountNumber1, "value"));
		BaseClass.settext(accountNumber2, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo( "Account Number 2 is entered as",BaseClass.getattribute(accountNumber2, "value"));
		BaseClass.settext(accountInfo, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "Account Info is entered as",BaseClass.getattribute(accountInfo, "value"));
		BaseClass.settext(accountType, BaseClass.stringGeneratorl(6));
		BaseClass.logInfo( "Account Type is entered as",BaseClass.getattribute(accountType, "value"));	
	}
	
	
	  public void VendorSetUp_MandatoryCheck()
	  {
		  saveVendor(true);
		  Assert.assertTrue(BaseClass.gettext(getErrorMessages().get(0)).equals("SSN / Tax Id is required."));
		  Assert.assertTrue(BaseClass.gettext(getErrorMessages().get(1)).equals("Vendor Name is required."));
		  Assert.assertTrue(BaseClass.gettext(getErrorMessages().get(2)).equals("Mailing Address1 is required."));
		  Assert.assertTrue(BaseClass.gettext(getErrorMessages().get(3)).equals("City is required."));
		  Assert.assertTrue(BaseClass.gettext(getErrorMessages().get(4)).equals("State is required."));
		  Assert.assertTrue(BaseClass.gettext(getErrorMessages().get(5)).equals("Postal is required."));
		  BaseClass.logpass("Error messages should be displayed for Mandatory Fields", "Error messages are displayed for Mandatory Fields");
	  }
	  
	  public void VendorSetUp_CreateVendor(HashMap<String,String> hm)
	  {
		  Wait.waitFor(3);
		
		  if(hm.get("TaxIdType").equals("No ID"))
		  {
		  selectTaxIDType("No ID");
		  }
		  else
		  {
			  if(hm.get("TaxIdType").equals("SSN"))
			  {
			  selectTaxIDType("SSN");
			  enterTaxIDValue();
			  } 
			  else
			  {
				  if(hm.get("TaxIdType").equals("TaxID(FEIN)"))
				  {
				  selectTaxIDType("TaxID(FEIN)");
				  enterTaxIDValue();
				  } 
			  }
		  }
		  
		  enterDateW9Sent();
		  enterDateW9Received();
		  enterVendorName();
		  enterLastOFACDate();
		  enterCrossRef();
		  enterContactName();
		  enterPhone();
		  enterFax();
		  enterEmail();
		  selectIRSType();
		  enterName();
		  enterDBA();
		  enterMailingAddress(false, false);
		  enterPhysicalAddress(false);
		  enterNote();
		  enterAlternateVendorNumber();
		  if(hm.get("Is1099MiscReporting").equals("Yes"))
		  {
			  addMiscREporting();
		  }
		  if(hm.get("IsUnauthorized").equals("Yes"))
		  {
			  setUnauthorizedYes();
		  }
		  if(hm.get("IsDeactivated").equals("Yes"))
		  {
			  setDeactivateYes();
		  }
		  setvendorType();
		  if(hm.get("IsMedicalProvider").equals("Yes"))
		  {
			  addMedicalProvider();
		  }
		  if(hm.get("IsAdditionalNames").equals("Yes"))
		  {
			  addAdditionalNames();
		  }
		  if(hm.get("IsPaymentModes").equals("Yes"))
		  {
			addPaymentModes();  
		  }
		  saveVendor(false);
		  BaseClass.logpass("Vendor Record should be created", "Vendor Record is created");
		  newVendor();
	  }

	private void addMiscREporting() {
		// TODO Auto-generated method stub
		
	}

	private void enterAlternateVendorNumber() {
		// TODO Auto-generated method stub
		
	}

	private void enterNote() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	

}
