package ClaimVisionPages;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import CommonModules.BaseClass;
import CommonModules.Wait;


public class ClientStateSpeciicPage {
	
	private ClientStateSpeciicPage(){}
	private static final ClientStateSpeciicPage ClientStateSpecific=new ClientStateSpeciicPage();

	public static ClientStateSpeciicPage getinstance()
	{
		return ClientStateSpecific;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(css="div#SpecialAnalysis_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	WebElement claimStatus;
	
	@FindBy(id="SpecialAnalysis_SpecialAnalysisFieldsControl_specialAnalysisControl0")
	private WebElement deductible;
	@FindBy(id="SpecialAnalysis_SpecialAnalysisFieldsControl_specialAnalysisControl1")
	private WebElement sir;
	@FindBy(id="SpecialAnalysis_SpecialAnalysisFieldsControl_specialAnalysisControl2")
	private WebElement deductibleReceived;
	@FindBy(id="SpecialAnalysis_SpecialAnalysisFieldsControl_specialAnalysisControl3")
	private WebElement uncollectedReceivables;
	@FindBy(id="SpecialAnalysis_saveButton")
	private WebElement save;
	@FindBy(id="SpecialAnalysis_cancelButton")
	private WebElement cancel;
	@FindBy(id="closeClaimImageButton")
	private WebElement closeClaim;
	@FindBy(xpath="//div[@id='SpecialAnalysis_claimInfo_claimInfoPanel']//")
	public WebElement claimStatusValue;
	@FindBy(css = "input#btnClose")
	public WebElement cancelBtnDiary;
	
	public void saveFunctionality(HashMap<String, String> hm){
		if(hm.get("CustomerCode").toUpperCase().contains("LACO"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of laco
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("AMLJIA"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of AMLJIA
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("RYDER"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of RYDER
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("NYLAW"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of NYLAW
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("NMGSD"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of NMGSD
		}
		//save functionality
		
	}
	
	public void cancelButtonFunctionality(HashMap<String, String> hm){
		if(hm.get("CustomerCode").toUpperCase().contains("LACO"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of laco
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("AMLJIA"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of AMLJIA
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("RYDER"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of RYDER
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("NYLAW"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of NYLAW
		}
		else if(hm.get("CustomerCode").toUpperCase().contains("NMGSD"))
		{BaseClass.logInfo("Client state specific screen for: ", hm.get("CustomerCode"));
		//fields of NMGSD
		}
		//cancel button functionality
		
	}
	public void claimStatus(HashMap<String, String> hm){
		//same as employe/incident screen
	}
	
	
	
	
	public void ValidateClientStateSpecific(HashMap<String, String> hm){
		
			BaseClass.logInfo("Verify Client State Specific values", "");
			
			Assert.assertEquals(BaseClass.getattribute(deductible, "value"), hm.get("Deductible"));
			Assert.assertEquals(BaseClass.getattribute(sir, "value"), hm.get("SIR"));
			Assert.assertEquals(BaseClass.getattribute(deductibleReceived, "value"), hm.get("DeductibleReceived"));
			Assert.assertEquals(BaseClass.getattribute(uncollectedReceivables, "value"), hm.get("UncollectedReceivables"));
			BaseClass.logpass("All Fields are displayed correctly ", "");
			
		
	}
	
	
	public void closeClaim_clientStateSpecficScreen(){
		
		String mainWindowHandle = null;
			BaseClass.logInfo("Validate Closed Claim from Client State Specific", "");
			BaseClass.click(closeClaim);
			BaseClass.logInfo("User clicked on close claim icon","");
			Wait.waitFor(3);
			BaseClass.acceptalert();
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusValue);
			switchToDiariesPopUp(mainWindowHandle);
			Wait.waitFor(4);
			BaseClass.driver.switchTo().window(mainWindowHandle);
			Wait.waitFor(2);
			String statusValue = claimStatusValue.getText();
			System.out.println("Value of status is: " + statusValue);
			Assert.assertTrue(statusValue.contains("Closed"));
			BaseClass.logpass("Verified that Claim status of claim is Closed", "");
			
			
		}
		
	
	public void verifySpecialAnalysisValue(HashMap<String,String> hm)
	{
		Assert.assertEquals(BaseClass.getattribute(deductible, "value"), hm.get("Deductible"));
		Assert.assertEquals(BaseClass.getattribute(sir, "value"), hm.get("SIR"));
		Assert.assertEquals(BaseClass.getattribute(deductibleReceived, "value"), hm.get("DeductibleReceived"));
		Assert.assertEquals(BaseClass.getattribute(uncollectedReceivables, "value"), hm.get("UncollectedReceivables"));
	}
	
	public String switchToDiariesPopUp(String parent){
		
		((JavascriptExecutor) BaseClass.driver).executeScript("window.focus();");
		
		Set<String> windows = BaseClass.driver.getWindowHandles();
		String childWindow="";

		Iterator<String> iterate = windows.iterator();
		while (iterate.hasNext()) {
			childWindow = iterate.next();

			if (!childWindow.equalsIgnoreCase(parent)) {
				BaseClass.driver.switchTo().window(childWindow);
				{
					if (BaseClass.driver.getTitle().equalsIgnoreCase("Open Diaries"))
					break;
				}
			}
		}
		String diaryTitle = BaseClass.getTitle();
		cancelBtnDiary.click();
		return diaryTitle;
			
	}
	
	public String getClaimStatusValue(){
		String statusValue = claimStatusValue.getText();
		return statusValue;
	}

}
