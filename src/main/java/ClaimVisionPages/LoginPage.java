package ClaimVisionPages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;


public class LoginPage {

	
private static final LoginPage login=new LoginPage();

private LoginPage(){}

public void initElement(WebDriver driver)
{
	PageFactory.initElements(driver, this);
}


public static LoginPage getinstance()
{
	return login;
}

	/*
	 * WebElements of Fields to be entered while Logging in
	 */
	@FindBy(css="input#customerCodeTextBox")
	public WebElement customercode; 
	
	@FindBy(css="span#headerControl_lblCustomerCode")
	public WebElement CustomerCodeLogged;
	
	@FindBy(css="input#userNameTextBox")
	public WebElement username;

	@FindBy(css="input#passwordTextBox")
	public WebElement password;
	
	@FindBy(css="a#headerControl_loginOutLinkButton")
	public WebElement logout;

	/*
	 * WebElements of Login Button to be clicked
	 */
	@FindBy(css="input#loginImageButton")
	public WebElement loginbutton;
	
	@FindBy(css="input#btnAgree")
	public WebElement agreeButton;

	/*
	 * WebElements of ValidationMessages labels if error occurred
	 */
	@FindBy(css="span#messageLabel")
	public WebElement MessageText;

	@FindBy(css="div#validationSummary")
	public WebElement ValidationMessage;
	
	@FindBy(id="ClaimSearch_claimNumberTextBox")
     public WebElement claimsearch_claimtextbox;
	/*
	 * Methods that will be used while writing Test Scripts.
	 */
	
	// This method will be used to enter Customer Code
	public void login(String customerCode,String userName,String password,String invalidScenario,String errorMessage)
	{	
		
	}
	public void login(String customerCode,String userName,String password)
	{
		BaseClass.settext(customercode,customerCode);
		BaseClass.logInfo("Customer Code is set as: ", customerCode);
		BaseClass.settext(username, userName);
		BaseClass.logInfo("User Name is set as: ", userName);
		BaseClass.settext(this.password, password);
		BaseClass.logInfo("Password is set as: ", password);
		BaseClass.click(loginbutton);
		BaseClass.logInfo("Login button is Clicked","");
			if(customerCode.toUpperCase().contains("LACO") || customerCode.toUpperCase().contains("COLA"))
			{
				BaseClass.click(agreeButton);
				BaseClass.logInfo("Agree Button is Clicked","");
				BaseClass.verifyTitle("Claim Search");
				BaseClass.logInfo("Claim Search Page is displayed","");
				BaseClass.verifyElement(claimsearch_claimtextbox);
			}
			

			BaseClass.logpass("User should be login successfully", "User is logged in Successfully");
	}
}
