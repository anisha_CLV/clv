package ClaimVisionPages;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.log4j.pattern.FullLocationPatternConverter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.graphbuilder.curve.CatmullRomSpline;
import com.relevantcodes.extentreports.ExtentTest;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class IRFPage {

	public HashMap<String, String> data = new HashMap<String, String>();
	String irfWindow = "";
	String searchClientWindow = "";
	String selectLocationWindow = "";

	private IRFPage() {
	}

	private static final IRFPage irf = new IRFPage();

	public static IRFPage getinstance() {
		return irf;
	}

	public void initElement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	/*
	 * WebElements of fields on IRF - WC Screen
	 */
	// Incident Date
	@FindBy(css = "input#dateOfIncidentTextBox")
	private WebElement incidentDate;

	@FindBy(id = "ClientAssignToText")
	WebElement claimClient;

	@FindBy(id = "LevelAssignedToText")
	WebElement claimlevelAssignedTo;

	@FindBy(id="NWClaimantContactControl1_IsCompanyClaimantCheckBox")
	WebElement claimantCompanyCheckbox;
	
	@FindBy(id="NWClaimantContactControl1_ClaimantCompanyTextBox")
	WebElement claimantCompanyTextbox;
	
	@FindBy(id="NWClaimantContactControl1_AddressControl1_Address1TextBox")
	WebElement claimantNCAddress1;
	
	@FindBy(id="NWClaimantContactControl1_AddressControl1_Address2TextBox")
	WebElement claimantNCAddress2;
	
	@FindBy(id="NWClaimantContactControl1_AddressControl1_ZipCodeTextBox")
	WebElement claimantNCzip;
	
	@FindBy(id="NWClaimantContactControl1_AddressControl1_CityDropDownList")
	WebElement claimantNCCity;
	
	@FindBy(id="NWClaimantContactControl1_AddressControl1_StateDropDownList")
	WebElement claimantNCState;
	
	@FindBy(id="NWClaimantContactControl1_AddressControl1_countyTextBox")
	WebElement claimantNCCounty;
	@FindBy(id="NWClaimantContactControl1_WorkPhoneTextBox")
	WebElement claimantNCWorkPhone;
	@FindBy(id="NWClaimantContactControl1_HomePhoneTextBox")
	WebElement claimantNCHomePhone;
	@FindBy(id="NWClaimantContactControl1_CellPhoneTextBox")
	WebElement claimantNCCellPhone;
	@FindBy(id="NWClaimantContactControl1_EmailTextBox")
	WebElement claimantNCEmail;
	@FindBy(id="NWClaimantContactControl1_ClaimantSSNTextBox")
	WebElement claimantNCClaimantID;
	@FindBy(id="NWClaimantContactControl1_EmployeeIDTextBox")
	WebElement claimantNCEmpID;
	
	@FindBy(id="NWClaimantContactControl1_ClaimantFirstNameTextBox")
	WebElement claimantNCFirstName;
	@FindBy(id="NWClaimantContactControl1_ClaimantMiddleNameTextBox")
	WebElement claimantNCMiddleName;
	@FindBy(id="NWClaimantContactControl1_ClaimantLastNameTextBox")
	WebElement claimantNCLastName;
	
	
	
	@FindBy(id = "IncidentTimeControl_hourDropDown")
	WebElement claimHour;

	@FindBy(id = "IncidentTimeControl_minuteDropDown")
	WebElement claimMinute;

	@FindBy(id = "IncidentTimeControl_meridianDropDown")
	WebElement claimMeridian;

	@FindBy(id = "IncidentDateText1")
	WebElement claimIncidentDate;

	@FindBy(id = "NWAddClaimantButton")
	WebElement addClaimant;
	
	@FindBy(id = "NWSaveClaimantButton")
	WebElement saveClaimant;
	
	@FindBy(id = "NWCancelClaimantButton")
	WebElement cancelClaimant;
	
	@FindBy(css = "input#returnButton")
	public WebElement returnButton;

	@FindBy(id = "NWClaimLocationAddressControl_Address1TextBox")
	public WebElement claimLocationAddress1;

	@FindBy(id = "NWClaimLocationAddressControl_Address2TextBox")
	public WebElement claimLocationAddress2;
	
	@FindBy(id = "NWincidentDescriptionTextBox")
	public WebElement claimIncidentDescription;
	
	@FindBy(id = "NWSummary1TextBox")
	public WebElement claimSummary;
	
	@FindBy(id = "NWDamages1TextBox")
	public WebElement claimDamages;

	@FindBy(id = "NWClaimLocationAddressControl_ZipCodeTextBox")
	public WebElement claimLocationZip;
	@FindBy(id = "NWClaimLocationAddressControl_CityDropDownList")
	public WebElement claimLocationCity;
	@FindBy(id = "NWClaimLocationAddressControl_StateDropDownList")
	public WebElement claimLocationState;
	@FindBy(id = "NWClaimLocationAddressControl_countyTextBox")
	public WebElement claimLocationCounty;

	@FindBy(id = "NWClaimContactControl_ClaimantFirstNameTextBox")
	WebElement claimContactFirstName;
	@FindBy(id = "NWClaimContactControl_ClaimantMiddleNameTextBox")
	WebElement claimContactMiddleName;
	@FindBy(id = "NWClaimContactControl_ClaimantLastNameTextBox")
	WebElement claimContactLastName;
	@FindBy(id = "NWClaimContactControl_ClaimantSSNTextBox")
	WebElement claimContactClaimantID;

	@FindBy(id = "NWClaimContactControl_AddressControl1_Address1TextBox")
	public WebElement claimContactAddress1;

	@FindBy(id = "NWClaimContactControl_AddressControl1_Address2TextBox")
	public WebElement claimContactAddress2;

	@FindBy(id = "NWClaimContactControl_AddressControl1_ZipCodeTextBox")
	public WebElement claimContactZip;
	@FindBy(id = "NWClaimContactControl_AddressControl1_CityDropDownList")
	public WebElement claimContactCity;
	@FindBy(id = "NWClaimContactControl_AddressControl1_StateDropDownList")
	public WebElement claimContactState;
	@FindBy(id = "NWClaimContactControl_AddressControl1_countyTextBox")
	public WebElement claimContactCounty;

	@FindBy(id = "NWClaimContactControl_WorkPhoneTextBox")
	public WebElement claimContactWorkPhone;
	@FindBy(id = "NWClaimContactControl_HomePhoneTextBox")
	public WebElement claimContactHomePhone;
	@FindBy(id = "NWClaimContactControl_CellPhoneTextBox")
	public WebElement claimContactCellPhone;
	@FindBy(id = "NWClaimContactControl_EmailTextBox")
	public WebElement claimContactEmail;
	
	@FindBy(id = "NWClaimantContactControl2_ClaimantFirstNameTextBox")
	WebElement claimantContactFirstName;
	@FindBy(id = "NWClaimantContactControl2_ClaimantMiddleNameTextBox")
	WebElement claimantContactMiddleName;
	@FindBy(id = "NWClaimantContactControl2_ClaimantLastNameTextBox")
	WebElement claimantContactLastName;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignAddressCheckBox")
	WebElement claimantContactForeignAddressCheckbox;

	@FindBy(id = "NWClaimantContactControl2_AddressControl1_Address1TextBox")
	public WebElement claimantContactAddress1;

	@FindBy(id = "NWClaimantContactControl2_AddressControl1_Address2TextBox")
	public WebElement claimantContactAddress2;

	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ZipCodeTextBox")
	public WebElement claimantContactZip;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_CityDropDownList")
	public WebElement claimantContactCity;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_StateDropDownList")
	public WebElement claimantContactState;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_countyTextBox")
	public WebElement claimantContactCounty;
	
	@FindBy(id = "NWSummary2TextBox")
	public WebElement claimantSummary;
	@FindBy(id = "NWDamages2TextBox")
	public WebElement claimantDamages;

	@FindBy(id = "NWClaimantContactControl2_WorkPhoneTextBox")
	public WebElement claimantContactWorkPhone;
	@FindBy(id = "NWClaimantContactControl2_HomePhoneTextBox")
	public WebElement claimantContactHomePhone;
	@FindBy(id = "NWClaimantContactControl2_CellPhoneTextBox")
	public WebElement claimantContactCellPhone;
	@FindBy(id = "NWClaimantContactControl2_EmailTextBox")
	public WebElement claimantContactEmail;
	@FindBy(id = "NWisWitnessRadioList_0")
	public WebElement claimWitnessYes;

	@FindBy(id = "NWisWitnessRadioList_1")
	public WebElement claimWitnessNo;

	@FindBy(id = "NWadditionalWitnessButton")
	public WebElement claimAdditionalWitness;

	// TimeOfLoss-Hours
	@FindBy(css = "select#timeOfLossTimeControl_hourDropDown")
	public WebElement timeOfLoss_hour;

	// TimeOfLoss-Minutes
	@FindBy(css = "select#timeOfLossTimeControl_minuteDropDown")
	public WebElement timeOfLoss_min;

	// TimeOfLoss-Meridian
	@FindBy(css = "select#timeOfLossTimeControl_meridianDropDown")
	public WebElement timeOfLoss_meridian;

	// Client
	@FindBy(css = "input#clientTextBox")
	public WebElement client;

	// LevelAssign
	@FindBy(css = "input#locationTextBox")
	public WebElement location;

	// SearchClient
	@FindBy(css = "img#searchClientImageButton")
	public WebElement searchClient;

	// SearchButton
	@FindBy(css = "input#searchButton.defaultButton01")
	public WebElement searchButton;

	// SelectValue
	@FindBy(css = "#searchResultDataList_ctl01_TableCell1")
	public WebElement selectValue;

	// LocationAssignedTo
	@FindBy(css = "img#searchLocationImage")
	public WebElement searchLocation;

	// Location of Level assigned to
	@FindBy(id = "locationTreeViewt0")
	public WebElement levelAssignedTo;

	// image of PCIS Insurrance
	@FindBy(css = "a#locationTreeViewt0i")
	public WebElement levelAssignedTreeView;

	@FindBy(id = "locationTextBox")
	public WebElement levelAssignedToTextBox;

	@FindBy(id = "clientTextBox")
	public WebElement clientTextBox;

	// close button of level assigned to
	@FindBy(css = "class#loginHead")
	public WebElement closeButton;

	// PolicyType
	@FindBy(css = "select#policyTypeDropDown")
	public WebElement policyType;

	// ClaimantID
	@FindBy(css = "input#socialSecurityTextBox")
	public WebElement claimantID;

	// ID Type
	@FindBy(css = "select#IdTypeDropDown")
	public WebElement idType;

	// SearchButton
	@FindBy(css = "input#claimantFirstNameTextBox")
	public WebElement firstName;

	// SelectValue
	@FindBy(css = "input#claimantMiddleNameTextBox")
	public WebElement middleName;

	// LocationAssignedTo
	@FindBy(css = "input#claimantLastNameTextBox")
	public WebElement lastName;

	// PolicyType
	@FindBy(css = "input#claimantSuffixTextBox")
	public WebElement suffix;

	// SearchClaimant
	@FindBy(css = "img#searchClaimantImage")
	public WebElement searchClaimant;

	// ClaimantID
	@FindBy(css = "input#employeeIdTextBox")
	public WebElement employeeID;

	// ID Type
	@FindBy(css = "input#accidentDescriptionTextBox")
	public WebElement incidentDesc;

	// AcquiredClaim
	@FindBy(css = "input#isAcquiredClaimCheckBox")
	public WebElement isAcquiredClaim;

	/*
	 * WebElements of buttons on IRF - WC Screen
	 */
	// SearchClaimant
	@FindBy(css = "input#cancel1Button")
	public WebElement cancelButton;

	// ClaimantID
	@FindBy(css = "input#save1Button")
	public WebElement saveButton;

	// ID Type
	@FindBy(css = "input#nextButton")
	public WebElement nextButton;

	@FindBy(id = "BodyPartImgButton")
	public WebElement expandBodyPart;
	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Claimant Information Section:
	 */
	// Gender
	@FindBy(css = "input#genderRadioButtonList_0")
	public WebElement genderMale;

	@FindBy(css = "input#genderRadioButtonList_1")
	public WebElement genderFemale;
	
	@FindBy(id = "claimantGenderRadioButtonList_0")
	public WebElement claimantGenderMale;

	@FindBy(id = "claimantGenderRadioButtonList_1")
	public WebElement claimantGenderFemale;
	
	@FindBy(id="NWPrimaryLanguageDropDownList")
	WebElement claimantPrimaryLanguage;
	
	@FindBy(id="NWdateOfBirthTextBox")
	WebElement claimantDateOfBirth;
	
	@FindBy(id="NWAgeTextBox")
	WebElement claimantAgeOnIncident;
	
	@FindBy(id="NWNoOfDependantsTextBox")
	WebElement claimantNoOfDependentsNC;
	
	@FindBy(id="NWMaritalStatusDropDownList")
	WebElement claimantMaritalStatus;
	
	@FindBy(id="NWReportDateToInsuredTextBox")
	WebElement claimantReportDateToInsured;
	
	@FindBy(id="NWEntryDateTextBox")
	WebElement claimantEntryDate;
	
	@FindBy(id="IncidentDateText2")
	WebElement claimantIncidentDate;
	
	@FindBy(id="IncidentTimeControl2_hourDropDown")
	WebElement claimantIncidentHour;
	
	@FindBy(id="IncidentTimeControl2_minuteDropDown")
	WebElement claimantIncidentMinute;
	
	@FindBy(id="IncidentTimeControl2_meridianDropDown")
	WebElement claimantIncidentMeridian;
	
	@FindBy(id="NWNoOfDependantsTextBox")
	WebElement claimantNoOfDependents;

	// dateOfBirth
	@FindBy(css = "input#dateOfBirthTextBox")
	public WebElement dateOfBirth;

	// ageOnIncidentDate
	@FindBy(css = "input#ageOnInjuryDateTextBox")
	public WebElement ageOnIncidentDate;

	// isForeignAddress
	@FindBy(css = "input#foreignAddressCheckBox")
	public WebElement isForeignAddress;

	// claimantAddress1
	@FindBy(css = "input#claimantAddress1TextBox")
	public WebElement claimantAddress1;

	// claimantAddress2
	@FindBy(css = "input#claimantAddress2TextBox")
	public WebElement claimantAddress2;

	@FindBy(id = "additionalWitnessButton")
	public WebElement addwitness;
	// claimantCity
	@FindBy(css = "select#CityDropDownList")
	public WebElement claimantCity;

	// claimantState
	@FindBy(css = "select#stateDropDown")
	public WebElement claimantState;

	// claimantPostal
	@FindBy(css = "input#postalTextBox")
	public WebElement claimantPostal;

	// claimantCounty
	@FindBy(css = "input#countyTextBox")
	public WebElement claimantCounty;

	// homePhone
	@FindBy(css = "input#homePhoneTextBox")
	public WebElement homePhone;

	// cellPhone
	@FindBy(css = "input#cellPhoneTextBox")
	public WebElement cellPhone;

	// emailAddress
	@FindBy(css = "input#emailAddressTextBox")
	public WebElement emailAddress;

	// primaryLanguage
	@FindBy(css = "select#primaryLanguageDropDown")
	public WebElement primaryLanguage;

	@FindBy(id = "irfTitleTreatmentDetail")
	public WebElement treatmentDetail;

	@FindBy(id = "PolicyTypeText")
	WebElement claimPolicyType;
	// maritalStatus
	@FindBy(css = "select#maritalStatusDropDown")
	public WebElement maritalStatus;

	// noOfDependents
	@FindBy(css = "input#noOfDependentsTextBox")
	public WebElement noOfDependents;

	// employmentStatus
	@FindBy(css = "select#employmentStatusDropDown")
	public WebElement employmentStatus;

	// hireDate
	@FindBy(css = "input#hireDateTextBox")
	public WebElement hireDate;

	// hireState
	@FindBy(css = "select#hireStateDropDown")
	public WebElement hireState;

	@FindBy(css = "input[id*='FirstNameTextBox']")
	List<WebElement> firstNameFields;

	@FindBy(id = "companyTextBox")
	public WebElement companyName;
	@FindBy(id = "IsCompanyClaimantCheckBox")
	public WebElement companyCheckbox;

	@FindBy(id = "foreignAddress1TextBox")
	public WebElement foreignAddress1;

	@FindBy(id = "foreignAddress2TextBox")
	public WebElement foreignAddress2;

	@FindBy(id = "ForeignCityTextBox")
	public WebElement foreignCity;

	@FindBy(id = "ForeignCountyTextBox")
	public WebElement foreignCounty;

	@FindBy(id = "ForeignStateDDL")
	public WebElement foreignState;
	@FindBy(id = "ForeignZipTextBox")
	public WebElement foreignZip;
	@FindBy(id = "ForeignCountryDDL")
	public WebElement foreignCountry;
	@FindBy(id = "accidentForeignAddressCheckBox")
	public WebElement accidentForeignAddressCheckBox;
	@FindBy(id = "occurredForeignAddress1TextBox")
	public WebElement incidentForeignAddress1;

	@FindBy(id = "occurredForeignAddress2TextBox")
	public WebElement incidentForeignAddress2;

	public void enterClaimantForeignAddress() {
		BaseClass.click(isForeignAddress);
		BaseClass.logInfo("Foreign Address Checkbox is clicked", "");
		BaseClass.settext(foreignAddress1, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Foreign Address 1 is set as:", BaseClass.getattribute(foreignAddress1, "value"));
		BaseClass.settext(foreignAddress2, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Foreign Address 2 is set as:", BaseClass.getattribute(foreignAddress2, "value"));
		BaseClass.settext(foreignCity, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Foreign City is set as:", BaseClass.getattribute(foreignCity, "value"));
		BaseClass.settext(foreignCounty, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Foreign County is set as:", BaseClass.getattribute(foreignCounty, "value"));
		BaseClass.settext(foreignZip, BaseClass.enterRandomNumber(5));
		BaseClass.logInfo("Foreign Zip is set as:", BaseClass.getattribute(foreignZip, "value"));
		BaseClass.selectByIndex(foreignState);
		BaseClass.logInfo("Foreign State is selected as: ", BaseClass.getfirstselectedoption(foreignState));
		BaseClass.selectByIndex(foreignCountry);
		BaseClass.logInfo("Foreign Country is selected as: ", BaseClass.getfirstselectedoption(foreignCountry));
	}

	public void enterIncidentForeignAddress() {
		BaseClass.click(accidentForeignAddressCheckBox);
		BaseClass.logInfo("Incident Foreign Address Checkbox is clicked", "");
		BaseClass.settext(incidentForeignAddress1, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Incident Foreign Address 1 is set as:",
				BaseClass.getattribute(incidentForeignAddress1, "value"));
		BaseClass.settext(incidentForeignAddress2, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Incident Foreign Address 2 is set as:",
				BaseClass.getattribute(incidentForeignAddress2, "value"));
	}

	public void enterFirstName() {
		System.out.println(firstNameFields.size());

		int count = 0;

		for (WebElement f : firstNameFields) {
			try {
				f.sendKeys(BaseClass.stringGeneratorl(5));

				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
		data.put("FirstName", BaseClass.getattribute(firstName, "value"));
	}

	@FindBy(css = "input[id*='MiddleNameTextBox']")
	List<WebElement> middleNameFields;

	public void enterMiddleName() {
		System.out.println(middleNameFields.size());

		int count = 0;

		for (WebElement f : middleNameFields) {
			try {
				f.sendKeys(BaseClass.stringGeneratorl(5));

				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
		data.put("MiddleName", BaseClass.getattribute(middleName, "value"));
	}

	@FindBy(css = "input[id*='LastNameTextBox']")
	List<WebElement> lastNameFields;

	public void enterLastName() {
		System.out.println(lastNameFields.size());

		int count = 0;

		for (WebElement f : lastNameFields) {
			try {
				f.sendKeys(BaseClass.stringGeneratorl(5));

				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
		data.put("LastName", BaseClass.getattribute(lastName, "value"));
	}

	@FindBy(css = "input[id*='Address1TextBox']")
	List<WebElement> address1Fields;

	public void enterAddress1() {
		System.out.println(address1Fields.size());

		int count = 0;

		for (WebElement f : address1Fields) {
			try {
				f.sendKeys(BaseClass.stringGeneratorl(5));

				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}
		System.out.println("Loop out:" + count);

	}

	@FindBy(css = "input[id*='Address2TextBox']")
	List<WebElement> address2Fields;

	public void enterAddress2() {
		System.out.println(address2Fields.size());

		int count = 0;

		for (WebElement f : address2Fields) {
			try {
				f.sendKeys(BaseClass.stringGeneratorl(5));

				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
	}

	/*
	 * @FindBy(css = "input[id*='ZipCodeTextBox']") List<WebElement>
	 * zipCodeFields;
	 * 
	 * public void enterZipCode(){ System.out.println(zipCodeFields.size());
	 * 
	 * int count = 0;
	 * 
	 * for(WebElement f:zipCodeFields){ try{ Wait.modifyWait(driver, f);
	 * f.clear(); f.sendKeys("12345"); Wait.waitFor(2); //f.sendKeys(Keys.TAB);
	 * 
	 * System.out.println(f.getAttribute("name"));
	 * 
	 * System.out.println("Loop in:" + count); count++; } catch(Exception e){
	 * continue; }
	 * 
	 * }
	 * 
	 * 
	 * System.out.println("Loop out:" + count); }
	 */

	@FindBy(css = "input#NWClaimContactControl_AddressControl1_ZipCodeTextBox")
	public WebElement claimContactLocationZip;

	@FindBy(css = "input#NWClaimantContactControl1_AddressControl1_ZipCodeTextBox")
	public WebElement claimantLocationZip;

	@FindBy(css = "input#NWClaimantContactControl2_AddressControl1_ZipCodeTextBox")
	public WebElement claimantContactLocationZip;

	public void enterZipCodeClaimantContactLocation() throws InterruptedException {

		claimantContactLocationZip.clear();
		claimantContactLocationZip.sendKeys("88595");
	}

	@FindBy(css = "input[id*='PhoneTextBox']")
	List<WebElement> phoneFields;

	public void enterPhone() {
		System.out.println(phoneFields.size());

		int count = 0;

		for (WebElement f : phoneFields) {
			try {
				f.clear();
				f.sendKeys(BaseClass.enterRandomNumber(10));
				f.sendKeys(Keys.TAB);
				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
	}

	@FindBy(css = "input[id*='EmailTextBox']")
	List<WebElement> emailFields;

	public void enterEmailAddressValues() {
		System.out.println(emailFields.size());

		int count = 0;

		for (WebElement f : emailFields) {
			try {
				f.clear();
				f.sendKeys("test@test.com");
				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
	}

	@FindBy(css = "input[id*='Summary']")
	List<WebElement> summaryFields;

	public void enterSummaryValues() {
		System.out.println(summaryFields.size());

		int count = 0;

		for (WebElement f : summaryFields) {
			try {
				f.clear();
				f.sendKeys("Summary is" + BaseClass.stringGeneratorl(5) + " " + BaseClass.stringGeneratorl(3));
				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
	}

	@FindBy(css = "input[id*='Damages']")
	List<WebElement> damagesFields;

	public void enterDamageValues() {
		System.out.println(damagesFields.size());

		int count = 0;

		for (WebElement f : damagesFields) {
			try {
				f.clear();
				f.sendKeys("Damage is" + BaseClass.stringGeneratorl(5) + " " + BaseClass.stringGeneratorl(3));
				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
	}

	@FindBy(css = "input[id*='SuffixTextBox']")
	List<WebElement> suffixFields;

	public void enterSuffix() {
		System.out.println(suffixFields.size());

		int count = 0;

		for (WebElement f : suffixFields) {
			try {
				f.clear();
				f.sendKeys(BaseClass.stringGeneratorl(5));

				System.out.println(f.getAttribute("name"));
				count++;
				System.out.println("Loop in:" + count);
			} catch (Exception e) {
				continue;
			}

		}

		System.out.println("Loop out:" + count);
		data.put("Suffix", BaseClass.getattribute(suffix, "value"));
	}
	// *

	@FindBy(css = "input#NWclaimMadeDateTextBox")
	public WebElement claimMadeDate;

	public String enterClaimMadeDate() {
		final Random random = new Random();
		final String month = Integer.toString(random.nextInt(12 - 1) + 1);
		final String day = Integer.toString(random.nextInt(30 - 10) + 10);
		final String year = Integer.toString(random.nextInt(2016 - 1975) + 1975);
		final String date = month + "/" + day + "/" + year;
		return date;
	}

	public String getIncidentdate() {
		return BaseClass.getattribute(incidentDate, "value");
	}

	public String getClient() {
		return BaseClass.getattribute(clientTextBox, "value");
	}

	public String getLevelAssignedTo() {
		return BaseClass.getattribute(levelAssignedToTextBox, "value");
	}

	public String getClaimantID() {
		return BaseClass.getattribute(claimantID, "value");
	}

	public String getEmployeeID() {
		return BaseClass.getattribute(employeeID, "value");
	}

	public String getClaimantFirstName() {
		return BaseClass.getattribute(firstName, "value");
	}

	public String getClaimantMiddleName() {
		return BaseClass.getattribute(middleName, "value");
	}

	public String getClaimantLastName() {
		return BaseClass.getattribute(lastName, "value");
	}

	public String getClaimantSuffix() {
		return BaseClass.getattribute(suffix, "value");
	}

	public String getIncidentDescription() {
		return BaseClass.getattribute(incidentDesc, "value");
	}

	public String getPolicyType() {
		return BaseClass.getfirstselectedoption(policyType);
	}

	public String gettimeoflossHH() {
		return BaseClass.getfirstselectedoption(timeOfLoss_hour);
	}

	public String gettimeoflossMM() {
		return BaseClass.getfirstselectedoption(timeOfLoss_min);
	}

	public String gettimeoflossMeridian() {
		return BaseClass.getfirstselectedoption(timeOfLoss_meridian);
	}

	public String getIDType() {
		return BaseClass.getfirstselectedoption(idType);
	}

	@FindBy(id = "NWPoliceReportTextBox")
	public WebElement policeReport;

	@FindBy(id = "NWPoliceReportCityTextBox")
	public WebElement policeReportCity;

	@FindBy(id = "NWAgencyTextBox")
	public WebElement agency;

	@FindBy(id = "NWOfficerTextBox")
	public WebElement officer;

	@FindBy(id = "NWBadgeTextBox")
	public WebElement badge;

	@FindBy(id = "NWPrimaryLanguageDropDownList")
	public WebElement primaryLanguageNonComp;

	@FindBy(id = "NWMaritalStatusDropDownList")
	public WebElement maritalStatusNonComp;

	@FindBy(css = "select#NWNoOfDependantsTextBox")
	public WebElement noOfDependantsNonComp;

	public WebElement witnessFirstName;
	public WebElement witnessMiddleName;
	public WebElement witnessLastName;
	public WebElement witnessPhoneNumber;

	// jurisdictionState
	@FindBy(css = "select#jurisdictionStateDropDown")
	public WebElement jurisdictionState;

	// occupationCode
	@FindBy(css = "input#ncciCodeTextBox")
	public WebElement occupationCode;

	// occupationDesc
	@FindBy(css = "input#ncciReadonlyTextBox")
	public WebElement occupationDesc;

	// nAICSCode
	@FindBy(css = "input#naicsTextBox")
	public WebElement nAICSCode;

	// searchNAICS
	@FindBy(css = "img#searchNaicsImageButton")
	public WebElement searchNAICS;

	// nAICSDesc
	@FindBy(css = "input#naicsReadOnlyTextBox")
	public WebElement nAICSDesc;

	// wageRate
	@FindBy(css = "input#wageRateTextBox")
	public WebElement wageRate;

	// Per
	@FindBy(css = "select#perDropDown")
	public WebElement per;

	// Days WorkedPerWeek
	@FindBy(css = "select#daysWorkedPerWeekDropDown")
	public WebElement daysWorkedPerWeek;

	// Is Full Day pay
	@FindBy(css = "input#isFullPayRadioButton_0")
	public WebElement fullDayYes;

	@FindBy(css = "input#isFullPayRadioButton_1")
	public WebElement fullDayNo;

	// Did Salary Continue?
	@FindBy(css = "input#isSalaryContinueRadioButton_0")
	public WebElement didSalaryContinueYes;

	@FindBy(css = "input#isSalaryContinueRadioButton_1")
	public WebElement didSalaryContinueNo;

	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Employee/Incident Information Section:
	 */
	// dateOfKnowledge
	@FindBy(css = "input#employerDateOfKnowledgeTextBox")
	public WebElement dateOfKnowledge;

	// adminReportedDate
	@FindBy(css = "input#adminReportedDateTextBox")
	public WebElement adminReportedDate;

	// reportedToFirstName
	@FindBy(css = "input#reportedToFirstNameTextBox")
	public WebElement reportedToFirstName;

	@FindBy(css = "div#irfImageInformation")
	public WebElement claimantInformation;

	@FindBy(css = "div#irfImageClaimInformation")
	public WebElement claimDetail;

	@FindBy(css = "div#irfImageClaimantInformation")
	public WebElement claimantDetail;

	@FindBy(css = "div#irfImageStateSpecificDetail")
	public WebElement clientStateSpecific;

	@FindBy(css = "div#irfImageSpecialAnalysisFields")
	public WebElement specialAnalysisFields;

	@FindBy(id = "irfImageAccidentDetail")
	public WebElement incidentDetail;

	// reportedToMiddleName
	@FindBy(css = "input#reportedToMiddleNameTextBox")
	public WebElement reportedToMiddleName;

	// reportedToLastName
	@FindBy(css = "input#reportedToLastNameTextBox")
	public WebElement reportedToLastName;

	// reportedToPhoneNumber
	@FindBy(css = "input#reportedToPhoneNumberTextBox")
	public WebElement reportedToPhoneNumber;

	@FindBy(css = "input#lastWorkDateTextBox")
	public WebElement lastWorkDate;

	// reportedToJobTitle
	@FindBy(css = "input#reportedToJobTitleTextBox")
	public WebElement reportedToJobTitle;

	// reportedToEmailAddress
	@FindBy(css = "input#reportedToEmailAddressTextBox")
	public WebElement reportedToEmailAddress;

	// isfatality
	@FindBy(css = "select#fatalityDropDownList")
	public WebElement isfatality;

	// fatalityDate
	@FindBy(css = "input#fatalityDateTextBox")
	public WebElement fatalityDate;

	// isPremisesRadio
	@FindBy(css = "input#isPremisesRadio_1")
	public WebElement isPremisesRadioNo;

	@FindBy(css = "input#isPremisesRadio_0")
	public WebElement isPremisesRadioYes;

	// accidentSummary
	@FindBy(css = "textarea#accidentSummaryTextBox")
	public WebElement accidentSummary;

	// majorInjuryCause
	@FindBy(css = "select#majorInjuryCauseDropDown")
	public WebElement majorInjuryCause;

	// minorInjuryCause
	@FindBy(css = "select#minorInjuryCauseDropDown")
	public WebElement minorInjuryCause;

	@FindBy(css = "input#occurredAddress1TextBox")
	public WebElement occuredAddress1;

	@FindBy(css = "input#occurredAddress2TextBox")
	public WebElement occuredAddress2;

	@FindBy(id = "accidentCityDropDown")
	public WebElement occuredCity;

	@FindBy(id = "accidentStateDropDown")
	public WebElement occuredState;

	@FindBy(css = "input#accidentPostalTextBox")
	public WebElement occuredPostal;

	// majorInjuryNature
	@FindBy(css = "select#majorInjuryNatureDropDown")
	public WebElement majorInjuryNature;

	// minorInjuryNature
	@FindBy(css = "select#minorInjuryNatureDropDown")
	public WebElement minorInjuryNature;

	@FindBy(css = "select#treatmentTypeDropDown")
	public WebElement treatmentType;

	// injuryType
	@FindBy(css = "select#injuryTypeDropDownList")
	public WebElement injuryType;

	// isRelatedOccurrenceRadio
	@FindBy(css = "input#isRelatedOccurrenceRadio_0")
	public WebElement isRelatedOccurrenceRadioYes;

	@FindBy(css = "input#isRelatedOccurrenceRadio_1")
	public WebElement isRelatedOccurrenceRadioNo;

	// isLostTimeAnticipatedRadio
	@FindBy(css = "input#isLostTimeAnticipatedRadio_0")
	public WebElement isLostTimeAnticipatedRadioYes;

	@FindBy(css = "input#isLostTimeAnticipatedRadio_1")
	public WebElement isLostTimeAnticipatedRadioNo;

	// timeBeganWork
	@FindBy(css = "input#timeBeganWorkTextBox")
	public WebElement timeBeganWork;

	// isWitnessRadioList
	@FindBy(css = "input#isWitnessRadioList_0")
	public WebElement isWitnessRadioListYes;

	@FindBy(css = "input#isWitnessRadioList_1")
	public WebElement isWitnessRadioListNo;
	// compfirstExposure
	@FindBy(css = "input#CompfirstExposureTextBox")
	public WebElement compfirstExposure;

	@FindBy(css = "input#dateDisabilityBeganTextBox")
	public WebElement dateDisabilityBegin;

	@FindBy(css = "div#irfImageSpecialAnalysisFields")
	public WebElement specialAnalysis;

	@FindBy(css = "input#returnToWorkDateTextBox")
	public WebElement returnToWork;

	// compLastExposure
	@FindBy(css = "input#CompLastExposureTextBox")
	public WebElement compLastExposure;

	// stateClaimNumber
	@FindBy(css = "input#stateClaimNumberTextBox")
	public WebElement stateClaimNumber;

	@FindBy(css = "input#bodyPartPercentTextBox")
	public WebElement bodyPartPercent;

	@FindBy(css = "input#isContinuousTraumaCheckBox")
	public WebElement isContinuousTrauma;

	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Treatment Detail Section:
	 */
	@FindBy(css = "input#isTreatmentRequired_0")
	public WebElement isTreatmentRequiredYes;

	@FindBy(css = "input#isTreatmentRequired_1")
	public WebElement isTreatmentRequiredNo;

	/*
	 * WebElements of elements after click of Next button on IRF - WC Screen
	 * Special Analysis fields Section:
	 */
	@FindBy(css = "select#specialAnalysisControl0")
	public WebElement awardsPaidInFull;

	@FindBy(css = "input#specialAnalysisControl0")
	public WebElement deductible;

	@FindBy(css = "select#specialAnalysisControl1")
	public WebElement caseAccepted;

	@FindBy(css = "input#specialAnalysisControl1")
	public WebElement sir;

	@FindBy(css = "select#specialAnalysisControl2")
	public WebElement caseDismissed;

	@FindBy(css = "select#specialAnalysisControl3")
	public WebElement delayInDecision;

	@FindBy(css = "input#specialAnalysisControl2")
	public WebElement deductibleReceived;

	@FindBy(css = "select#specialAnalysisControl4")
	public WebElement destroyed;

	@FindBy(css = "input#specialAnalysisControl3")
	public WebElement uncollectedDeductible;

	@FindBy(css = "select#specialAnalysisControl5")
	public WebElement lifeMedical;

	@FindBy(css = "select#specialAnalysisControl6")
	public WebElement lifePension;

	@FindBy(css = "select#specialAnalysisControl7")
	public WebElement longTermDisability;

	@FindBy(css = "select#specialAnalysisControl8")
	public WebElement maintenanceCase;

	@FindBy(css = "select#specialAnalysisControl9")
	public WebElement megaFlexParticipant;

	@FindBy(css = "select#specialAnalysisControl10")
	public WebElement presumptiveStatus;

	@FindBy(css = "select#specialAnalysisControl11")
	public WebElement salaryContinuation;

	@FindBy(css = "select#specialAnalysisControl12")
	public WebElement scifContribution;

	@FindBy(css = "select#specialAnalysisControl13")
	public WebElement sensitiveClaim;

	@FindBy(css = "select#specialAnalysisControl14")
	public WebElement transferToIndemnity;

	@FindBy(css = "select#specialAnalysisControl15")
	public WebElement carveOut;

	@FindBy(css = "select#specialAnalysisControl16")
	public WebElement injuryCodeMisc1;

	@FindBy(css = "select#specialAnalysisControl17")
	public WebElement nCCIInjuryType;

	@FindBy(css = "select#specialAnalysisControl18")
	public WebElement maywoodFire;

	@FindBy(css = "input#conditionTextBox")
	public WebElement searchTextBoxClient;

	@FindBy(id = "searchResultDataList_ctl01_showImage")
	public WebElement firstCell;

	@FindBy(xpath = "//div[@id='searchNaicsResult']//tr[2]/td/img")
	public WebElement firstNAICS;

	@FindBy(css = "div#irfImageTreatmentDetail")
	public WebElement treatmentRequired;

	@FindBy(css = "select#specialAnalysisControl0")
	public WebElement civilDefense;

	@FindBy(css = "select#specialAnalysisControl1")
	public WebElement classified;

	@FindBy(css = "select#specialAnalysisControl2")
	public WebElement closedSymptomatic;

	@FindBy(css = "select#specialAnalysisControl3")
	public WebElement text15_8;

	@FindBy(css = "select#specialAnalysisControl4")
	public WebElement finalAdjustment;

	@FindBy(css = "select#specialAnalysisControl5")
	public WebElement lumpsum;

	@FindBy(css = "select#specialAnalysisControl6")
	public WebElement maritime;

	@FindBy(css = "select#specialAnalysisControl7")
	public WebElement rehab;

	@FindBy(css = "select#specialAnalysisControl8")
	public WebElement thridParty;

	@FindBy(css = "select#specialAnalysisControl9")
	public WebElement thirdPartyLienCollect;

	@FindBy(css = "select#specialAnalysisControl10")
	public WebElement thirdPartyLienEstab;

	@FindBy(css = "select#specialAnalysisControl11")
	public WebElement thirdPartyReviewed;

	@FindBy(css = "select#specialAnalysisControl12")
	public WebElement text5105Est;

	@FindBy(css = "select#specialAnalysisControl13")
	public WebElement text5105DoesNotApply;

	@FindBy(css = "select#specialAnalysisControl14")
	public WebElement text5105Reviewed;

	@FindBy(css = "select#specialAnalysisControl15")
	public WebElement text25A;

	@FindBy(css = "select#specialAnalysisControl16")
	public WebElement subrogation;

	@FindBy(css = "select#specialAnalysisControl17")
	public WebElement medicareMedication;

	@FindBy(css = "input#specialAnalysisControl18")
	public WebElement timeInAssignment;

	@FindBy(css = "select#specialAnalysisControl19")
	public WebElement timeInAssignmentUnknown;

	/*
	 * WebElements for Buttons on IRF Submit screen
	 */
	@FindBy(css = "input#osha301aButton")
	public WebElement osha301aButton;

	@FindBy(css = "input#cancelButton")
	public WebElement cancelButton1;

	@FindBy(css = "input#saveButton")
	public WebElement saveButton1;

	@FindBy(css = "input#saveAndNew")
	public WebElement saveAndNewButton;

	@FindBy(css = "input#submitButton")
	public WebElement submitButton;

	@FindBy(className = "submittedSuccessfully")
	public WebElement successfulMessage;

	@FindBy(css = "input#reportTypeRadioButton_0")
	public WebElement reportOnly;

	@FindBy(css = "input#reportTypeRadioButton_2")
	public WebElement delay;

	@FindBy(xpath = "//input[@value='Claim']")
	public WebElement claim;

	public void selectReportOnly() {
		BaseClass.click(reportOnly);
	}

	public void selectDelay() {
		BaseClass.click(delay);
	}

	/*
	 * public String getClient() throws IOException{ return
	 * ExcelReader.getCell(1, 0, 1); }
	 */
	public String enterIncidentDate() throws InterruptedException, IOException {

		final Random random = new Random();
		String month = Integer.toString(random.nextInt(12 - 1) + 1);
		final String day = Integer.toString(random.nextInt(30 - 10) + 10);
		final String year = Integer.toString(random.nextInt(2005 - 2000) + 2000);
		if (month.length() == 1) {
			month = "0" + month;
		}
		final String date = month + "/" + day + "/" + year;

		// Wait.modifyWait(driver, incidentDate);
		incidentDate.sendKeys(date);
		data.put("IncidentDate", date);
		return date;
	}

	public void enterIncidentDate(String date) {
		incidentDate.sendKeys(date);
	}

	public void enterTimeOfLoss() throws IOException {
		Select s = new Select(timeOfLoss_hour);
		s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
		data.put("TimeOfLossHour", BaseClass.getfirstselectedoption(timeOfLoss_hour));
		Select s1 = new Select(timeOfLoss_min);
		s1.selectByIndex(BaseClass.generateRandomNumberAsInteger(s1.getOptions().size()));
		data.put("TimeOfLossMin", BaseClass.getfirstselectedoption(timeOfLoss_min));
		Select s2 = new Select(timeOfLoss_meridian);
		s2.selectByIndex(BaseClass.generateRandomNumberAsInteger(s2.getOptions().size()));
		data.put("TimeOfLossMeridian", BaseClass.getfirstselectedoption(timeOfLoss_meridian));
		String time = s.getFirstSelectedOption().getText() + ":" + s1.getFirstSelectedOption().getText() + " "
				+ s2.getFirstSelectedOption().getText();

	}

	public void enterClient(String client) {
		irfWindow = BaseClass.getcurrentwindow();
		BaseClass.click(searchClient);
		BaseClass.switchToNextWindow(irfWindow, "Search Client", 3);
		BaseClass.settext(searchTextBoxClient, client);
		BaseClass.click(searchButton);
		BaseClass.click(firstCell);
		BaseClass.switchToWindow(irfWindow, 2);
	}

	public void selectLocation() {
		irfWindow = BaseClass.getcurrentwindow();
		BaseClass.click(searchLocation);
		BaseClass.switchToNextWindow(irfWindow, "Search Location", 3);
		BaseClass.gettext(levelAssignedTo);
		List<WebElement> location = BaseClass.getDriver()
				.findElements(By.xpath("//a[contains(@id,'locationTreeViewt')]"));
		BaseClass.click(location.get(BaseClass.generateRandomNumberAsInteger(location.size())));
		BaseClass.switchToWindow(irfWindow, 2);

	}

	public void enterLastName1() throws IOException {
		final String lastNameValue = BaseClass.stringGeneratorl(5);
		lastName.sendKeys(lastNameValue);

	}

	public void enterSuffix1() throws IOException {
		final String suffixValue = BaseClass.stringGeneratorl(2);
		suffix.sendKeys(suffixValue);

	}

	public void enterIncidentDescription() throws IOException {

		final String incidentValue = "The Incident is" + " " + BaseClass.stringGeneratorl(5);
		incidentDesc.sendKeys(incidentValue);
		data.put("IncidentDescription", BaseClass.getattribute(incidentDesc, "value"));

	}

	public void selectNext() {
		nextButton.click();
	}

	public void enterTreatmentType() throws IOException {
		if (isTreatmentRequiredYes.isSelected()) {
			final Select s = new Select(treatmentType);
			s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
			final String temp = s.getFirstSelectedOption().getText();

		}

	}

	public void selectMaywoodFire() throws InterruptedException, IOException {
		try {

			final Select s = new Select(maywoodFire);
			s.selectByIndex(BaseClass.generateRandomNumberAsInteger(s.getOptions().size()));
			final String temp = s.getFirstSelectedOption().getText();

		} catch (final NoSuchElementException e) {
			e.getMessage();
		}

	}

	public void enterDeductible() {
		deductible.sendKeys(BaseClass.enterRandomNumber(3));

	}

	public void enterSIR() {
		sir.sendKeys(BaseClass.enterRandomNumber(3));

	}

	public void enterDeductibleReceived() {
		deductibleReceived.sendKeys(BaseClass.enterRandomNumber(3));

	}

	public void enterUncollectedDeductible() {
		uncollectedDeductible.sendKeys(BaseClass.enterRandomNumber(3));

	}

	public String getClaimNumber() {
		String firstMessage = successfulMessage.getText();
		String test = firstMessage.substring(80);
		System.out.println("Claim number: " + test);
		System.out.println("Claim successful message" + successfulMessage.getText());
		return test;
	}

	public String getClaimNumberForNYLAW() {
		String firstMessage = successfulMessage.getText();
		String test = firstMessage.substring(80);
		System.out.println("Claim number: " + test);
		System.out.println("Claim successful message" + successfulMessage.getText());
		return test;
	}

	@FindBy(id = "firstNameTextBox")
	public WebElement irfSearchFirstName;

	@FindBy(id = "lastNameTextBox")
	public WebElement irfSearchLastName;

	@FindBy(id = "claimantNameTextBox")
	public WebElement irfSearchCompanyName;

	@FindBy(id = "SSNTextBox")
	public WebElement irfSearchSSNTextBox;

	@FindBy(id = "employeeIdTextBox")
	public WebElement irfSearchEmployeeID;

	@FindBy(id = "ClaimNumberTextBox")
	public WebElement irfSearchClaimNumber;

	@FindBy(id = "clientIdTextBox")
	public WebElement irfSearchClientID;

	@FindBy(id = "locationTextBox")
	public WebElement irfSearchLocation;

	@FindBy(id = "dateOfInjuryFromTextBox")
	public WebElement irfSearchIncidentFrom;

	@FindBy(id = "dateOfInjuryToTextBox")
	public WebElement irfSearchIncidentTo;

	@FindBy(id = "iRFStatusDropDown")
	public WebElement irfSearchIRFStatus;

	@FindBy(id = "policyTypeDropDownList")
	public WebElement irfSearchPolicyType;

	@FindBy(id = "searchButton")
	public WebElement irfSearch;

	@FindBy(id = "clearButton")
	public WebElement irfClear;
	@FindBy(id = "CancelButton")
	public WebElement irfCancel;

	@FindBy(id = "printdiv")
	public WebElement searchGrid;

	@FindBy(id = "searchResultDataList_ctl01_DeleteButton")
	public WebElement deleteButton;
	@FindBy(id = "searchResultDataList_ctl01_TableCell2")
	public WebElement irfID;

	@FindBy(xpath = "//table[@id='searchResultDataList']//img")
	private WebElement viewButton;
	@FindBy(id = "searchResultDataList_ctl01_TableCell5")
	private WebElement firstIRFIncidentDate;

	@FindBy(id = "newIrfImageButton")
	private WebElement newIrfImageButton;

	@FindBy(id = "deleteIrfImageButton")
	private WebElement deleteIrfImageButton;

	@FindBy(id = "NWClaimLocationAddressControl_ForeignAddressCheckBox")
	private WebElement claimLocationForeignAddressCheckbox;
	@FindBy(id = "NWClaimLocationAddressControl_ForeignAddress1TextBox")
	private WebElement claimLocationForeignAddress1;
	@FindBy(id = "NWClaimLocationAddressControl_ForeignAddress2TextBox")
	private WebElement claimLocationForeignAddress2;
	@FindBy(id = "NWClaimLocationAddressControl_ForeignCity")
	private WebElement claimLocationForeignCity;
	@FindBy(id = "NWClaimLocationAddressControl_ForeignCounty")
	private WebElement claimLocationForeignCounty;
	@FindBy(id = "NWClaimLocationAddressControl_ForeignStateDDL")
	private WebElement claimLocationForeignState;
	@FindBy(id = "NWClaimLocationAddressControl_ForeignZip")
	private WebElement claimLocationForeignZip;
	@FindBy(id = "NWClaimLocationAddressControl_ForeignCountyDDL")
	private WebElement claimLocationForeignCountry;

	public void enterClaimLocationForeignAddress() {
		BaseClass.click(claimLocationForeignAddressCheckbox);
		BaseClass.logInfo("Foreign Address Checkbox is selected", "");
		BaseClass.settext(claimLocationForeignAddress1, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Claimant Foreign Address 1 is set",
				BaseClass.getattribute(claimLocationForeignAddress1, "value"));
		BaseClass.settext(claimLocationForeignAddress2, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Claimant Foreign Address 2 is set",
				BaseClass.getattribute(claimLocationForeignAddress2, "value"));
		BaseClass.settext(claimLocationForeignCity, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Claimant Foreign Address is set",
				BaseClass.getattribute(claimLocationForeignAddress1, "value"));
		BaseClass.settext(claimLocationForeignCounty, BaseClass.stringGeneratorl(5));
		BaseClass.settext(claimLocationForeignZip, BaseClass.stringGeneratorl(5));
	}

	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignAddressCheckBox")
	private WebElement claimContactLocationForeignAddressCheckbox;
	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignAddress1TextBox")
	private WebElement claimContactLocationForeignAddress1;
	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignAddress2TextBox")
	private WebElement claimContactLocationForeignAddress2;
	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignCity")
	private WebElement claimContactLocationForeignCity;
	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignCounty")
	private WebElement claimContactLocationForeignCountry;
	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignStateDDL")
	private WebElement claimContactLocationForeignState;
	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignZip")
	private WebElement claimContactLocationForeignZip;
	@FindBy(id = "NWClaimContactControl_AddressControl1_ForeignCountyDDL")
	private WebElement claimContactLocationForeignCounty;

	public void enterClaimContactForeignAddress() {
		BaseClass.click(claimContactLocationForeignAddressCheckbox);
		BaseClass.settext(claimContactLocationForeignAddress1, BaseClass.stringGeneratorl(10));
		BaseClass.settext(claimContactLocationForeignAddress2, BaseClass.stringGeneratorl(10));
		BaseClass.settext(claimContactLocationForeignCity, BaseClass.stringGeneratorl(5));
		BaseClass.settext(claimContactLocationForeignCounty, BaseClass.stringGeneratorl(5));
		BaseClass.settext(claimContactLocationForeignZip, BaseClass.stringGeneratorl(5));
	}

	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignAddressCheckBox")
	private WebElement ClaimantContactLocationForeignAddressCheckbox;
	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignAddress1TextBox")
	private WebElement ClaimantContactLocationForeignAddress1;
	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignAddress2TextBox")
	private WebElement ClaimantContactLocationForeignAddress2;
	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignCity")
	private WebElement ClaimantContactLocationForeignCity;
	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignCounty")
	private WebElement ClaimantContactLocationForeignCountry;
	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignStateDDL")
	private WebElement ClaimantContactLocationForeignState;
	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignZip")
	private WebElement ClaimantContactLocationForeignZip;
	@FindBy(id = "NWClaimantContactControl1_AddressControl1_ForeignCountyDDL")
	private WebElement ClaimantContactLocationForeignCounty;

	public void enterClaimantForeignAddress_NC() {
		BaseClass.click(ClaimantContactLocationForeignAddressCheckbox);
		BaseClass.settext(ClaimantContactLocationForeignAddress1, BaseClass.stringGeneratorl(10));
		BaseClass.settext(ClaimantContactLocationForeignAddress2, BaseClass.stringGeneratorl(10));
		BaseClass.settext(ClaimantContactLocationForeignCity, BaseClass.stringGeneratorl(5));
		BaseClass.settext(ClaimantContactLocationForeignCounty, BaseClass.stringGeneratorl(5));
		BaseClass.settext(ClaimantContactLocationForeignZip, BaseClass.stringGeneratorl(5));
	}

	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignAddressCheckBox")
	private WebElement ClaimantContact2LocationForeignAddressCheckbox;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignAddress1TextBox")
	private WebElement ClaimantContact2LocationForeignAddress1;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignAddress2TextBox")
	private WebElement ClaimantContact2LocationForeignAddress2;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignCity")
	private WebElement ClaimantContact2LocationForeignCity;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignCounty")
	private WebElement ClaimantContact2LocationForeignCountry;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignStateDDL")
	private WebElement ClaimantContact2LocationForeignState;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignZip")
	private WebElement ClaimantContact2LocationForeignZip;
	@FindBy(id = "NWClaimantContactControl2_AddressControl1_ForeignCountyDDL")
	private WebElement ClaimantContact2LocationForeignCounty;

	public void enterClaimantContactForeignAddress_NC() {
		BaseClass.click(ClaimantContact2LocationForeignAddressCheckbox);
		BaseClass.settext(ClaimantContact2LocationForeignAddress1, BaseClass.stringGeneratorl(10));
		BaseClass.settext(ClaimantContact2LocationForeignAddress2, BaseClass.stringGeneratorl(10));
		BaseClass.settext(ClaimantContact2LocationForeignCity, BaseClass.stringGeneratorl(5));
		BaseClass.settext(ClaimantContact2LocationForeignCounty, BaseClass.stringGeneratorl(5));
		BaseClass.settext(ClaimantContact2LocationForeignZip, BaseClass.stringGeneratorl(5));
	}

	public HashMap<String, String> getSpecialAnalysisFieldsValue() {
		HashMap<String, String> hm = new HashMap<>();
		hm.put("Deductible", BaseClass.getattribute(deductible, "value"));
		hm.put("SIR", BaseClass.getattribute(sir, "value"));
		hm.put("DeductibleReceived", BaseClass.getattribute(deductibleReceived, "value"));
		hm.put("UncollectedReceivables", BaseClass.getattribute(uncollectedDeductible, "value"));
		return hm;
	}

	public void createIRF(HashMap<String, String> hm) {
		BaseClass.click(claim);
		BaseClass.logInfo("CLaim radio Button is selected", "");
		BaseClass.settext(incidentDate, this.enterClaimMadeDate());
		BaseClass.logInfo("Date is set as: ", BaseClass.getattribute(incidentDate, "value"));
		if (hm.get("SetTime").equals("Yes")) {
			BaseClass.selectByIndex(timeOfLoss_hour);
			BaseClass.logInfo("Time Of Loss Hour is selected as: ", BaseClass.getfirstselectedoption(timeOfLoss_hour));
			BaseClass.selectByIndex(timeOfLoss_min);
			BaseClass.logInfo("Time Of Loss Minutes is selected as: ",
					BaseClass.getfirstselectedoption(timeOfLoss_min));
			BaseClass.selectByIndex(timeOfLoss_meridian);
			BaseClass.logInfo("Time Of Loss Meridian is selected as: ",
					BaseClass.getfirstselectedoption(timeOfLoss_meridian));
		}
		enterClient(hm.get("Client"));
		BaseClass.logInfo("Client is set as: ", BaseClass.getattribute(client, "value"));
		Wait.waitFor(3);
		selectLocation();
		BaseClass.logInfo("Location is set as: ", BaseClass.getattribute(location, "value"));
		BaseClass.selectByVisibleText(policyType, hm.get("PolicyType"));
		BaseClass.logInfo("Policy Type is selected as: ", BaseClass.getfirstselectedoption(policyType));
		BaseClass.settext(claimantID, BaseClass.enterRandomNumber(9));
		BaseClass.logInfo("Claimant ID is set as: ", BaseClass.getattribute(claimantID, "value"));
		BaseClass.selectByVisibleText(idType, "Social Security Number");
		BaseClass.logInfo("ID Type Hour is selected as: ", BaseClass.getfirstselectedoption(idType));
		if (hm.get("ClaimantIsCompany").equalsIgnoreCase("Yes")) {
			BaseClass.click(companyCheckbox);
			BaseClass.logInfo("Claimant is Company Checkbox is selected ", "");
			BaseClass.settext(companyName, BaseClass.stringGeneratorl(4) + " " + BaseClass.stringGeneratorl(6));
			BaseClass.logInfo("Company Name is set as: ", BaseClass.getattribute(companyName, "value"));
		} else {
			BaseClass.settext(firstName, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo("First Name is set as: ", BaseClass.getattribute(firstName, "value"));
			BaseClass.settext(middleName, BaseClass.stringGeneratorl(2));
			BaseClass.logInfo("Middle Name is set as: ", BaseClass.getattribute(middleName, "value"));
			BaseClass.settext(lastName, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo("Last Name is set as: ", BaseClass.getattribute(lastName, "value"));
		}
		BaseClass.settext(incidentDesc, BaseClass.stringGeneratorl(5) + " " + BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Incident Description is set as: ", BaseClass.getattribute(incidentDesc, "value"));
		BaseClass.click(nextButton);
		BaseClass.logInfo("NEXT button is clicked", "");
		if (hm.get("PolicyType").equals("Workers Compensation")
				|| hm.get("PolicyType").equals("Workers' Compensation")) {
			enterClaimantdetail(hm);
			enterIncidentDetail(hm);
			enterTreatmentDetail(hm);
			

		} else {
			enterClaimDetail(hm);
			enterclaimantDetail(hm);
		}
		enterSpecialAnalysisFields(hm);
		BaseClass.click(submitButton);
		BaseClass.logInfo("SUBMIT button is clicked", "");
		BaseClass.verifyTitle("Submitted");
		Assert.assertTrue(BaseClass.gettext(successfulMessage)
				.contains("You have successfully submitted a report to the Claim system. Your claim id is"));
		BaseClass.logpass("Claim should be submitted",
				"Claim is submitted with Claim Number: " + BaseClass.gettext(successfulMessage).split("is")[1].trim());

	}

	private void enterclaimantDetail(HashMap<String, String> hm) {
		
		BaseClass.click(claimantDetail);
		if(companyCheckbox.isSelected())
		{
			Assert.assertTrue(claimantCompanyCheckbox.isSelected());
			Assert.assertEquals(BaseClass.getattribute(claimantCompanyTextbox, "value"), BaseClass.getattribute(companyName, "value"));
		}
		else
		{
			Wait.waitFor(3);
			System.out.println(claimantNCFirstName.getAttribute("value"));
			System.out.println(firstName.getAttribute("value"));
			Assert.assertEquals(BaseClass.getattribute(claimantNCFirstName, "value"), BaseClass.getattribute(firstName, "value"));
			Assert.assertEquals(BaseClass.getattribute(claimantNCMiddleName, "value"), BaseClass.getattribute(middleName, "value"));
			Assert.assertEquals(BaseClass.getattribute(claimantNCLastName, "value"), BaseClass.getattribute(lastName, "value"));
		}
		BaseClass.settext(claimantNCClaimantID, BaseClass.enterRandomNumber(10));
		BaseClass.settext(claimantNCEmpID, BaseClass.enterRandomNumber(10));
		if (hm.get("IsAddressForeign").equalsIgnoreCase("Yes")) {
			enterClaimantForeignAddress_NC();
		} else {
			BaseClass.settext(claimantNCAddress1, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claimant Address1 is set as: ",
					BaseClass.getattribute(claimantNCAddress1, "value"));
			BaseClass.settext(claimantNCAddress2, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claimant Address2 is set as: ",
					BaseClass.getattribute(claimantNCAddress2, "value"));
			BaseClass.settext(claimantNCzip, hm.get("Postal") + Keys.TAB);
			BaseClass.logInfo("Claimant Zip Code is set as: ", BaseClass.getattribute(claimantNCzip, "value"));
			BaseClass.selectByIndex(claimantNCCity);
			BaseClass.logInfo("Claimant City is selected as: ",
					BaseClass.getfirstselectedoption(claimantNCCity));
			BaseClass.logInfo("Claimant State is selected as: ",
					BaseClass.getfirstselectedoption(claimantNCState));
			BaseClass.logInfo("Claimant County is set as: ", BaseClass.getattribute(claimantNCCounty, "value"));
		}
		BaseClass.settext(claimantNCWorkPhone, BaseClass.enterRandomNumber(10));
		BaseClass.settext(claimantNCHomePhone, BaseClass.enterRandomNumber(10));
		BaseClass.settext(claimantNCCellPhone, BaseClass.enterRandomNumber(10));
		BaseClass.settext(claimantNCEmail,"test@pcisvision.com");
		BaseClass.clickRandomWebElement(claimantGenderFemale, claimantGenderMale);
		if (claimantGenderFemale.isSelected()) {
			BaseClass.logInfo("Gender is selected as Female", "");
		} else {
			BaseClass.logInfo("Gender is selected as Male", "");
		}
		BaseClass.selectByIndex(claimantPrimaryLanguage);
		BaseClass.logInfo("Primary Language is selected as: ", BaseClass.getfirstselectedoption(claimantPrimaryLanguage));
		BaseClass.settext(claimantDateOfBirth,
				BaseClass.pastdatefromdate(BaseClass.getattribute(incidentDate, "value"), 20, 50)+Keys.TAB);
		BaseClass.logInfo("Claimant Date Of Birth is set as: ", BaseClass.getattribute(claimantDateOfBirth, "value"));
		Assert.assertTrue(BaseClass.getattribute(claimantAgeOnIncident, "value").equals("20"));
		BaseClass.logpass("Age On Claim Date should be 20", "Age On CLiam Date is 20");
		BaseClass.settext(claimantNoOfDependentsNC,
				BaseClass.enterRandomNumber(2));
		BaseClass.logInfo("Claimant No Of Dependents is set as: ", BaseClass.getattribute(claimantNoOfDependentsNC, "value"));
		BaseClass.selectByIndex(claimantMaritalStatus);
		BaseClass.logInfo("Marital Status is selected as: ", BaseClass.getfirstselectedoption(claimantMaritalStatus));
		BaseClass.settext(claimantReportDateToInsured,
				BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 5));
		BaseClass.logInfo("Claimant Report Date TO Insured is set as: ", BaseClass.getattribute(claimantReportDateToInsured, "value"));
		Assert.assertEquals(BaseClass.getattribute(claimantEntryDate, "value"), BaseClass.getCurrentDatePST());
		Assert.assertEquals(BaseClass.getattribute(claimantIncidentDate, "value"), BaseClass.getattribute(incidentDate, "value"));
		BaseClass.logpass("Incident Date Under Claimant Detail tab should be displayed as:"+BaseClass.getattribute(incidentDate, "value"), "Incident Date Under Claimant Detail tab is displayed as:"+BaseClass.getattribute(claimantIncidentDate, "value"));

		Assert.assertEquals(BaseClass.getfirstselectedoption(claimantIncidentHour),
				BaseClass.getfirstselectedoption(timeOfLoss_hour));
		BaseClass.logpass("Incident Hour Under Claimant Detail tab should be displayed as:"+BaseClass.getfirstselectedoption(timeOfLoss_hour), "Incident Hour Under Claimant Detail tab is displayed as:"+BaseClass.getfirstselectedoption(claimantIncidentHour));

		Assert.assertEquals(BaseClass.getfirstselectedoption(claimantIncidentMinute),
				BaseClass.getfirstselectedoption(timeOfLoss_min));
		BaseClass.logpass("Incident Minutes Under Claimant Detail tab should be displayed as:"+BaseClass.getfirstselectedoption(timeOfLoss_min), "Incident Minutes Under Claimant Detail tab is displayed as:"+BaseClass.getfirstselectedoption(claimantIncidentMinute));

		Assert.assertEquals(BaseClass.getfirstselectedoption(claimantIncidentMeridian),
				BaseClass.getfirstselectedoption(timeOfLoss_meridian));
		BaseClass.logpass("Incident Meridian Under Claimant Detail tab should be displayed as:"+BaseClass.getfirstselectedoption(timeOfLoss_meridian), "Incident Meridian Under Claimant Detail tab is displayed as:"+BaseClass.getfirstselectedoption(claimantIncidentMeridian));
		enterClaimantContact(hm.get("IsAddressForeign"), hm.get("Postal"));
		BaseClass.settext(claimantSummary, BaseClass.stringGeneratorl(20));
		BaseClass.logInfo("Claimant Summary is set as: ",
				BaseClass.getattribute(claimantSummary, "value"));
		BaseClass.settext(claimantDamages, BaseClass.stringGeneratorl(20));
		BaseClass.logInfo("Claimant Damages is set as: ",
				BaseClass.getattribute(claimantDamages, "value"));
		
	
	}
	private void enterClaimantContact(String isForeign, String zip) {
		BaseClass.settext(claimantContactFirstName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Claimant Contact First Name is set as: ",
				BaseClass.getattribute(claimantContactFirstName, "value"));
		BaseClass.settext(claimantContactMiddleName, BaseClass.stringGeneratorl(2));
		BaseClass.logInfo("Claimant Contact Middle Name is set as: ",
				BaseClass.getattribute(claimantContactMiddleName, "value"));
		BaseClass.settext(claimantContactLastName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Claimant Contact Last Name is set as: ", BaseClass.getattribute(claimantContactLastName, "value"));
		if (isForeign.equalsIgnoreCase("Yes")) {
			enterClaimantContactForeignAddress_NC();
		} else {
			BaseClass.settext(claimantContactAddress1, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claimant Contact Address1 is set as: ",
					BaseClass.getattribute(claimantContactAddress1, "value"));
			BaseClass.settext(claimantContactAddress2, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claimant Contact Address2 is set as: ",
					BaseClass.getattribute(claimantContactAddress2, "value"));
			BaseClass.settext(claimantContactZip, zip + Keys.TAB);
			BaseClass.logInfo("Claimant Contact Zip Code is set as: ", BaseClass.getattribute(claimantContactZip, "value"));
			BaseClass.selectByIndex(claimantContactCity);
			BaseClass.logInfo("Claimant Contact City is selected as: ",
					BaseClass.getfirstselectedoption(claimantContactCity));
			BaseClass.logInfo("Claimant Contact State is selected as: ",
					BaseClass.getfirstselectedoption(claimantContactState));
			BaseClass.logInfo("Claimant Contact County is set as: ", BaseClass.getattribute(claimantContactCounty, "value"));
		}
		BaseClass.settext(claimantContactWorkPhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claimant Contact Work Phone is set as: ", BaseClass.getattribute(claimantContactWorkPhone, "value"));
		BaseClass.settext(claimantContactHomePhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claimant Contact Home Phone is set as: ", BaseClass.getattribute(claimantContactHomePhone, "value"));
		BaseClass.settext(claimantContactCellPhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claimant Contact Cell Phone is set as: ", BaseClass.getattribute(claimantContactCellPhone, "value"));
		BaseClass.settext(claimantContactEmail, "test@pcisvision.com");
		BaseClass.logInfo("Claimant Contact Email is set as: ", BaseClass.getattribute(claimantContactEmail, "value"));
	}

	private void enterClaimDetail(HashMap<String, String> hm) {
		BaseClass.click(claimDetail);
		Assert.assertEquals(BaseClass.getattribute(claimPolicyType, "value"),
				BaseClass.getfirstselectedoption(policyType));
		BaseClass.logpass("Policy Type Under Claim Detail tab should be displayed as:"+BaseClass.getfirstselectedoption(policyType), "Policy Type Under Claim Detail tab is displayed as:"+BaseClass.getattribute(claimPolicyType, "value"));
		Assert.assertEquals(BaseClass.getattribute(claimIncidentDate, "value"),
				BaseClass.getattribute(incidentDate, "value"));
		BaseClass.logpass("Incident Date Under Claim Detail tab should be displayed as:"+BaseClass.getattribute(incidentDate, "value"), "Incident Date Under Claim Detail tab is displayed as:"+BaseClass.getattribute(claimIncidentDate, "value"));

		Assert.assertEquals(BaseClass.getfirstselectedoption(claimHour),
				BaseClass.getfirstselectedoption(timeOfLoss_hour));
		BaseClass.logpass("Incident Hour Under Claim Detail tab should be displayed as:"+BaseClass.getfirstselectedoption(timeOfLoss_hour), "Incident Hour Under Claim Detail tab is displayed as:"+BaseClass.getfirstselectedoption(claimHour));

		Assert.assertEquals(BaseClass.getfirstselectedoption(claimMinute),
				BaseClass.getfirstselectedoption(timeOfLoss_min));
		BaseClass.logpass("Incident Minutes Under Claim Detail tab should be displayed as:"+BaseClass.getfirstselectedoption(timeOfLoss_min), "Incident Minutes Under Claim Detail tab is displayed as:"+BaseClass.getfirstselectedoption(claimMinute));

		Assert.assertEquals(BaseClass.getfirstselectedoption(claimMeridian),
				BaseClass.getfirstselectedoption(timeOfLoss_meridian));
		BaseClass.logpass("Incident Meridian Under Claim Detail tab should be displayed as:"+BaseClass.getfirstselectedoption(timeOfLoss_meridian), "Incident Meridian Under Claim Detail tab is displayed as:"+BaseClass.getfirstselectedoption(claimMeridian));

		Assert.assertEquals(BaseClass.getattribute(claimClient, "value"), BaseClass.getattribute(client, "value"));
		BaseClass.logpass("Clieny Under Claim Detail tab should be displayed as:"+BaseClass.getattribute(client, "value"), "Incident Date Under Claim Detail tab is displayed as:"+BaseClass.getattribute(claimClient, "value"));
		Assert.assertEquals(BaseClass.getattribute(claimlevelAssignedTo, "value"),
				BaseClass.getattribute(location, "value"));
		BaseClass.logpass("Level Assigned To Under Claim Detail tab should be displayed as:"+BaseClass.getattribute(location, "value"), "Level Assigned To Under Claim Detail tab is displayed as:"+BaseClass.getattribute(claimlevelAssignedTo, "value"));
		enterClaimAddress(hm.get("IsAddressForeign"), hm.get("Postal"));
		BaseClass.settext(policeReport, BaseClass.enterRandomNumber(5));
		BaseClass.logInfo("Police Report# is set as: ", BaseClass.getattribute(policeReport, "value"));
		BaseClass.settext(policeReportCity, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Police Report City is set as: ", BaseClass.getattribute(policeReportCity, "value"));
		BaseClass.settext(agency, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Agency is set as: ", BaseClass.getattribute(agency, "value"));
		BaseClass.settext(officer, BaseClass.stringGeneratorl(10));
		BaseClass.logInfo("Officer is set as: ", BaseClass.getattribute(officer, "value"));
		BaseClass.settext(badge, BaseClass.enterRandomNumber(5));
		BaseClass.logInfo("Badge# is set as: ", BaseClass.getattribute(badge, "value"));
		enterClaimContact(hm.get("IsAddressForeign"), hm.get("Postal"));
		BaseClass.clickRandomWebElement(claimWitnessYes, claimWitnessNo);
		if (claimWitnessYes.isSelected()) {
			BaseClass.logInfo("Was There a Witness? is selected as Yes", "");
			BaseClass.click(claimAdditionalWitness);
			BaseClass.logInfo("Additional Witness Button is clicked", "");
			for (int i = 0; i < 2; i++) {
				witnessFirstName = BaseClass.getDriver().findElement(By.id("NWwitness" + i + "FirstNameTextBox"));
				witnessMiddleName = BaseClass.getDriver().findElement(By.id("NWwitness" + i + "MiddleNameTextBox"));
				witnessLastName = BaseClass.getDriver().findElement(By.id("NWwitness" + i + "LastNameTextBox"));
				witnessPhoneNumber = BaseClass.getDriver().findElement(By.id("NWwitness" + i + "PhoneTextBox"));
				BaseClass.settext(witnessFirstName, BaseClass.stringGeneratorl(5));
				BaseClass.logInfo("Witness " + (i + 1) + " First Name is set as ",
						BaseClass.getattribute(witnessFirstName, "value"));
				BaseClass.settext(witnessMiddleName, BaseClass.stringGeneratorl(2));
				BaseClass.logInfo("Witness " + (i + 1) + " Middle Name is set as ",
						BaseClass.getattribute(witnessMiddleName, "value"));
				BaseClass.settext(witnessLastName, BaseClass.stringGeneratorl(5));
				BaseClass.logInfo("Witness " + (i + 1) + " Last Name is set as ",
						BaseClass.getattribute(witnessLastName, "value"));
				BaseClass.settext(witnessPhoneNumber, BaseClass.enterRandomNumber(10));
				BaseClass.logInfo("Witness " + (i + 1) + " Phone Number is set as ",
						BaseClass.getattribute(witnessPhoneNumber, "value"));
			}

		} else {
			BaseClass.logInfo("Was There a Witness? is selected as No", "");
		}
		Assert.assertEquals(BaseClass.getattribute(claimIncidentDescription, "value"), BaseClass.getattribute(incidentDesc, "value"));
		BaseClass.logpass("Incident Description Under Claim Detail should be: "+BaseClass.getattribute(claimIncidentDescription, "value"), "Incident Description Under Claim Detail are: "+BaseClass.getattribute(claimIncidentDescription, "value"));
		BaseClass.settext(claimSummary, BaseClass.stringGeneratorl(20));
		BaseClass.logInfo("Claim Summary is set as: ",
				BaseClass.getattribute(claimantSummary, "value"));
		BaseClass.settext(claimDamages, BaseClass.stringGeneratorl(20));
		BaseClass.logInfo("Claim Damages is set as: ",
				BaseClass.getattribute(claimDamages, "value"));
	}

	private void enterClaimContact(String isForeign, String zip) {
		BaseClass.settext(claimContactFirstName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Claim Contact First Name is set as: ",
				BaseClass.getattribute(claimContactFirstName, "value"));
		BaseClass.settext(claimContactMiddleName, BaseClass.stringGeneratorl(2));
		BaseClass.logInfo("Claim Contact Middle Name is set as: ",
				BaseClass.getattribute(claimContactMiddleName, "value"));
		BaseClass.settext(claimContactLastName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Claim Contact Last Name is set as: ", BaseClass.getattribute(claimContactLastName, "value"));
		BaseClass.settext(claimContactClaimantID, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claim Contact Claimant ID is set as: ", BaseClass.getattribute(claimContactClaimantID, "value"));
		if (isForeign.equalsIgnoreCase("Yes")) {
			enterClaimContactForeignAddress();
		} else {
			BaseClass.settext(claimContactAddress1, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claim Contact Address1 is set as: ",
					BaseClass.getattribute(claimContactAddress1, "value"));
			BaseClass.settext(claimContactAddress2, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claim Contact Address2 is set as: ",
					BaseClass.getattribute(claimContactAddress2, "value"));
			BaseClass.settext(claimContactZip, zip + Keys.TAB);
			BaseClass.logInfo("Claim Contact Zip Code is set as: ", BaseClass.getattribute(claimContactZip, "value"));
			BaseClass.selectByIndex(claimContactCity);
			BaseClass.logInfo("Claim Contact City is selected as: ",
					BaseClass.getfirstselectedoption(claimContactCity));
			BaseClass.logInfo("Claim Contact State is selected as: ",
					BaseClass.getfirstselectedoption(claimContactState));
			BaseClass.logInfo("Claim Contact County is set as: ", BaseClass.getattribute(claimContactCounty, "value"));
		}
		BaseClass.settext(claimContactWorkPhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claim Contact Work Phone is set as: ", BaseClass.getattribute(claimContactWorkPhone, "value"));
		BaseClass.settext(claimContactHomePhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claim Contact Home Phone is set as: ", BaseClass.getattribute(claimContactHomePhone, "value"));
		BaseClass.settext(claimContactCellPhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claim Contact Cell Phone is set as: ", BaseClass.getattribute(claimContactCellPhone, "value"));
		BaseClass.settext(claimContactEmail, "test@pcisvision.com");
		BaseClass.logInfo("Claim Contact Email is set as: ", BaseClass.getattribute(claimContactEmail, "value"));
	}

	private void enterClaimAddress(String isForeign, String zip) {

		if (isForeign.equalsIgnoreCase("Yes")) {
			enterClaimantForeignAddress();
		} else {
			BaseClass.settext(claimLocationAddress1, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claim Location Address1 is set as: ",
					BaseClass.getattribute(claimLocationAddress1, "value"));
			BaseClass.settext(claimLocationAddress2, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claim Location  Address2 is set as: ",
					BaseClass.getattribute(claimLocationAddress2, "value"));
			BaseClass.settext(claimLocationZip, zip + Keys.TAB);
			BaseClass.logInfo("Claim Location Zip Code is set as: ", BaseClass.getattribute(claimLocationZip, "value"));
			BaseClass.selectByIndex(claimLocationCity);
			BaseClass.logInfo("Claim Location City is selected as: ",
					BaseClass.getfirstselectedoption(claimLocationCity));
			BaseClass.logInfo("Claim Location State is selected as: ",
					BaseClass.getfirstselectedoption(claimLocationState));
			BaseClass.logInfo("Claim Location County is set as: ",
					BaseClass.getattribute(claimLocationCounty, "value"));
		}
	}


	private void enterSpecialAnalysisFields(HashMap<String, String> hm) {
		BaseClass.click(specialAnalysisFields);
		if (hm.get("CustomerCode").toUpperCase().contains("RYDER")) {
			
			BaseClass.selectByIndex(BaseClass.getDriver().findElement(By.id("specialAnalysisControl9")));
			BaseClass.logInfo("Special Anylysis Field Blind Application is set as: ",
					BaseClass.getattribute(BaseClass.getDriver().findElement(By.id("specialAnalysisControl9")), "value"));
		}
		if (hm.get("CustomerCode").toUpperCase().contains("MDMTA")) {
			BaseClass.selectByIndex(BaseClass.getDriver().findElement(By.id("specialAnalysisControl5")));
			BaseClass.logInfo("Special Anylysis Field Claim Status is set as: ",
					BaseClass.getattribute(BaseClass.getDriver().findElement(By.id("specialAnalysisControl5")), "value"));
		}

	}

	private void enterTreatmentDetail(HashMap<String, String> hm) {

		BaseClass.click(treatmentDetail);
		BaseClass.clickRandomWebElement(isTreatmentRequiredYes, isTreatmentRequiredNo);
		if (isTreatmentRequiredYes.isSelected()) {
			BaseClass.logInfo("Treatment Required is selected as Yes", "");
			BaseClass.selectByIndex(treatmentType);
			BaseClass.logInfo("Treatment Type is selected as", BaseClass.getfirstselectedoption(treatmentType));

		} else {
			BaseClass.logInfo("Treatment Required is selected as No", "");
		}

	}

	private void enterIncidentDetail(HashMap<String, String> hm) {
		BaseClass.click(incidentDetail);
		BaseClass.settext(dateOfKnowledge,
				BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 2));
		BaseClass.logInfo("Date of Knowledge is set as: ", BaseClass.getattribute(dateOfKnowledge, "value"));
		BaseClass.settext(adminReportedDate,
				BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 3));
		BaseClass.logInfo("Admin Report Date is set as: ", BaseClass.getattribute(adminReportedDate, "value"));
		BaseClass.settext(reportedToFirstName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Incident Reported to First Name is set as: ",
				BaseClass.getattribute(reportedToFirstName, "value"));
		BaseClass.settext(reportedToMiddleName, BaseClass.stringGeneratorl(2));
		BaseClass.logInfo("Incident Reported to Middle Name is set as: ",
				BaseClass.getattribute(reportedToMiddleName, "value"));
		BaseClass.settext(reportedToLastName, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Incident Reported to Last Name is set as: ",
				BaseClass.getattribute(reportedToLastName, "value"));
		BaseClass.settext(reportedToPhoneNumber, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Incident Reported to Phone Number is set as: ",
				BaseClass.getattribute(reportedToPhoneNumber, "value"));
		BaseClass.settext(reportedToEmailAddress, "incident@test.com");
		BaseClass.logInfo("Incident Reported to Email Address is set as: ",
				BaseClass.getattribute(reportedToEmailAddress, "value"));
		BaseClass.settext(reportedToJobTitle, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Incident Reported to Job Title is set as: ",
				BaseClass.getattribute(reportedToJobTitle, "value"));
		BaseClass.selectByIndex(isfatality);
		BaseClass.logInfo("Is Fatality Related to the Incident is selected as:",
				BaseClass.getfirstselectedoption(isfatality));
		if (BaseClass.getfirstselectedoption(isfatality).equals("Yes")) {
			BaseClass.settext(fatalityDate,
					BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 4));
			BaseClass.logInfo("Fatality Date is set as: ", BaseClass.getattribute(fatalityDate, "value"));
		}
		BaseClass.clickRandomWebElement(isPremisesRadioYes, isPremisesRadioNo);
		if (isPremisesRadioYes.isSelected()) {
			BaseClass.logInfo("Incident Occurred On Employer's Premises? is selected as Yes", "");
		} else {
			BaseClass.logInfo("Incident Occurred On Employer's Premises? is selected as No", "");
			if (hm.get("IsAddressForeign").equalsIgnoreCase("Yes")) {
				enterIncidentForeignAddress();
			} else {
				BaseClass.settext(occuredAddress1, BaseClass.stringGeneratorl(5));
				BaseClass.logInfo("Occurance Address 1 is set as: ", BaseClass.getattribute(occuredAddress1, "value"));
				BaseClass.settext(occuredAddress2, BaseClass.stringGeneratorl(5));
				BaseClass.logInfo("Occurance Address 2 is set as: ", BaseClass.getattribute(occuredAddress2, "value"));
				BaseClass.settext(occuredPostal, hm.get("Postal"));
				BaseClass.logInfo("Occurance Postal is set as: ", BaseClass.getattribute(occuredPostal, "value"));
				BaseClass.settext(accidentSummary, "");
				BaseClass.selectByIndex(occuredCity);
				BaseClass.logInfo("Occurance City is selected as:", BaseClass.getfirstselectedoption(occuredCity));
				BaseClass.logInfo("Occurance State is selected as:", BaseClass.getfirstselectedoption(occuredState));
			}
		}
		BaseClass.settext(accidentSummary, BaseClass.stringGeneratorl(300));
		BaseClass.logInfo("Claim Summary is set as: ", BaseClass.getattribute(accidentSummary, "value"));
		BaseClass.selectByIndex(majorInjuryCause);
		BaseClass.logInfo("Cause Of Incident (Major) is selected as:",
				BaseClass.getfirstselectedoption(majorInjuryCause));
		BaseClass.selectByIndex(minorInjuryCause);
		BaseClass.logInfo("Cause Of Incident (Minor) is selected as:",
				BaseClass.getfirstselectedoption(minorInjuryCause));
		BaseClass.selectByIndex(majorInjuryNature);
		BaseClass.logInfo("Nature Of Incident (Major) is selected as:",
				BaseClass.getfirstselectedoption(majorInjuryNature));
		BaseClass.selectByIndex(minorInjuryNature);
		BaseClass.logInfo("Nature Of Incident (Minor) is selected as:",
				BaseClass.getfirstselectedoption(minorInjuryNature));
		BaseClass.click(expandBodyPart);
		for (int i = 1; i <= 6; i++) {
			WebElement majorBodyPart, minorBodyPart;
			if (i == 1) {
				majorBodyPart = BaseClass.getDriver().findElement(By.id("majorBodyPartDropDown"));
				minorBodyPart = BaseClass.getDriver().findElement(By.id("minorBodyPartDropDown"));
			} else {
				majorBodyPart = BaseClass.getDriver().findElement(By.id("majorPartDropDown" + i));
				minorBodyPart = BaseClass.getDriver().findElement(By.id("minorPartDropDown" + i));
			}
			while (BaseClass.getfirstselectedoption(majorBodyPart).equals("Select one")
					|| BaseClass.getfirstselectedoption(majorBodyPart).equals("Body System")
					|| BaseClass.getfirstselectedoption(majorBodyPart).equals("Pending Classification")
					|| BaseClass.getfirstselectedoption(majorBodyPart).equals("Conversion")) {
				BaseClass.selectByIndex(majorBodyPart);
			}

			BaseClass.logInfo("Part Of Body (Major) #" + i + " is selected as:",
					BaseClass.getfirstselectedoption(majorBodyPart));
			BaseClass.selectByIndex(minorBodyPart);
			BaseClass.logInfo("Part Of Body (Minor) #" + i + " is selected as:",
					BaseClass.getfirstselectedoption(minorBodyPart));
		}

		BaseClass.selectByIndex(injuryType);
		BaseClass.logInfo("Type Of Incident Code is selected as:", BaseClass.getfirstselectedoption(injuryType));
		BaseClass.clickRandomWebElement(isRelatedOccurrenceRadioYes, isRelatedOccurrenceRadioNo);
		if (isRelatedOccurrenceRadioYes.isSelected()) {
			BaseClass.logInfo("Is This Incident Related To An Occurrence? is selected as Yes", "");
		} else {
			BaseClass.logInfo("Is This Incident Related To An Occurrence? is selected as No", "");
		}
		BaseClass.clickRandomWebElement(isLostTimeAnticipatedRadioYes, isLostTimeAnticipatedRadioNo);
		if (isLostTimeAnticipatedRadioYes.isSelected()) {
			BaseClass.logInfo("Lost Time Anticipated is selected as Yes", "");
			BaseClass.settext(dateDisabilityBegin,
					BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 1));
			BaseClass.logInfo("Disability Begin Date Date is set as: ",
					BaseClass.getattribute(dateDisabilityBegin, "value"));
			BaseClass.settext(returnToWork,
					BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 20));
			BaseClass.logInfo("Return to Work Date is set as: ", BaseClass.getattribute(returnToWork, "value"));
			BaseClass.settext(lastWorkDate, BaseClass.getattribute(incidentDate, "value"));
			BaseClass.logInfo("Last Work Date is set as: ", BaseClass.getattribute(lastWorkDate, "value"));
		} else {
			BaseClass.logInfo("Lost Time Anticipated is selected as No", "");
		}
		BaseClass.settext(timeBeganWork, "12:45");
		BaseClass.logInfo("Time Employee Began Work (Military) is set as: ",
				BaseClass.getattribute(timeBeganWork, "value"));
		BaseClass.clickRandomWebElement(isWitnessRadioListYes, isWitnessRadioListNo);
		if (isWitnessRadioListYes.isSelected()) {
			BaseClass.logInfo("Was There a Witness? is selected as Yes", "");
			BaseClass.click(addwitness);
			BaseClass.logInfo("Additional Witness Button is clicked", "");
			for (int i = 0; i < 2; i++) {
				witnessFirstName = BaseClass.getDriver().findElement(By.id("witness" + i + "FirstNameTextBox"));
				witnessMiddleName = BaseClass.getDriver().findElement(By.id("witness" + i + "MiddleNameTextBox"));
				witnessLastName = BaseClass.getDriver().findElement(By.id("witness" + i + "LastNameTextBox"));
				witnessPhoneNumber = BaseClass.getDriver().findElement(By.id("witness" + i + "PhoneTextBox"));
				BaseClass.settext(witnessFirstName, BaseClass.stringGeneratorl(5));
				BaseClass.logInfo("Witness " + (i + 1) + " First Name is set as ",
						BaseClass.getattribute(witnessFirstName, "value"));
				BaseClass.settext(witnessMiddleName, BaseClass.stringGeneratorl(2));
				BaseClass.logInfo("Witness " + (i + 1) + " Middle Name is set as ",
						BaseClass.getattribute(witnessMiddleName, "value"));
				BaseClass.settext(witnessLastName, BaseClass.stringGeneratorl(5));
				BaseClass.logInfo("Witness " + (i + 1) + " Last Name is set as ",
						BaseClass.getattribute(witnessLastName, "value"));
				BaseClass.settext(witnessPhoneNumber, BaseClass.enterRandomNumber(10));
				BaseClass.logInfo("Witness " + (i + 1) + " Phone Number is set as ",
						BaseClass.getattribute(witnessPhoneNumber, "value"));
			}

		} else {
			BaseClass.logInfo("Was There a Witness? is selected as No", "");
		}
		BaseClass.settext(compfirstExposure,
				BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 2));
		BaseClass.logInfo("First Exposure Date is set as: ", BaseClass.getattribute(compfirstExposure, "value"));
		BaseClass.settext(compLastExposure,
				BaseClass.futuredatefromdate(BaseClass.getattribute(incidentDate, "value"), 20));
		BaseClass.logInfo("Last Exposure Date is set as: ", BaseClass.getattribute(compLastExposure, "value"));
		BaseClass.settext(stateClaimNumber, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("State Claim Number is set as: ", BaseClass.getattribute(stateClaimNumber, "value"));
		BaseClass.settext(bodyPartPercent, BaseClass.randInt(1, 100));
		BaseClass.logInfo("Body Part Percent is set as: ", BaseClass.getattribute(bodyPartPercent, "value"));
	}

	private void enterClaimantdetail(HashMap<String, String> hm) {
		BaseClass.click(claimantInformation);
		BaseClass.clickRandomWebElement(genderFemale, genderMale);
		if (genderFemale.isSelected()) {
			BaseClass.logInfo("Gender is selected as Female", "");
		} else {
			BaseClass.logInfo("Gender is selected as Male", "");
		}
		BaseClass.settext(dateOfBirth,
				BaseClass.pastdatefromdate(BaseClass.getattribute(incidentDate, "value"), 20, 50));
		BaseClass.logInfo("Claimant Date Of Birth is set as: ", BaseClass.getattribute(dateOfBirth, "value"));
		setClaimantaddress(hm.get("IsAddressForeign"), hm.get("Postal"));
		BaseClass.settext(homePhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claimant Home Phone is set as: ", BaseClass.getattribute(homePhone, "value"));
		BaseClass.settext(cellPhone, BaseClass.enterRandomNumber(10));
		BaseClass.logInfo("Claimant Cell Phone is set as: ", BaseClass.getattribute(cellPhone, "value"));

		BaseClass.settext(emailAddress, "pcis@test.com");
		BaseClass.logInfo("Claimant Email Address is set as: ", BaseClass.getattribute(emailAddress, "value"));
		BaseClass.selectByIndex(primaryLanguage);
		BaseClass.logInfo("Claimant Primary Language is selected as: ",
				BaseClass.getfirstselectedoption(primaryLanguage));
		BaseClass.selectByIndex(maritalStatus);
		BaseClass.logInfo("Claimant Marital Status is selected as: ", BaseClass.getfirstselectedoption(maritalStatus));
		BaseClass.settext(noOfDependents, BaseClass.generateRandomNumberAsInteger(2).toString());
		BaseClass.logInfo("Claimant No Of Dependent is set as: ", BaseClass.getattribute(noOfDependents, "value"));
		BaseClass.selectByIndex(employmentStatus);
		BaseClass.logInfo("Claimant Employment Status is selected as: ",
				BaseClass.getfirstselectedoption(employmentStatus));
		BaseClass.settext(hireDate, BaseClass.pastdatefromdate(BaseClass.getattribute(incidentDate, "value"), 50));
		BaseClass.logInfo("Claimant Hire Date is set as: ", BaseClass.getattribute(hireDate, "value"));
		BaseClass.selectByIndex(hireState);
		BaseClass.logInfo("Claimant Hire State is selected as: ", BaseClass.getfirstselectedoption(hireState));
		BaseClass.selectByIndex(jurisdictionState);
		BaseClass.logInfo("Claimant Jurisdiction State is selected as: ",
				BaseClass.getfirstselectedoption(jurisdictionState));
		selectNAICS();
		BaseClass.settext(wageRate, BaseClass.enterRandomNumber(4));
		BaseClass.logInfo("Claimant Wage Rate is set as: ", BaseClass.getattribute(wageRate, "value"));
		BaseClass.selectByIndex(per);
		BaseClass.logInfo("Per is selected as: ", BaseClass.getfirstselectedoption(per));
		BaseClass.selectByIndex(daysWorkedPerWeek);
		BaseClass.logInfo("Days Worked Per Week is selected as: ", BaseClass.getfirstselectedoption(daysWorkedPerWeek));
		BaseClass.clickRandomWebElement(fullDayYes, fullDayNo);
		if (fullDayYes.isSelected()) {
			BaseClass.logInfo("Full Pay For Day Of Incident is selected as Yes", "");
		} else {
			BaseClass.logInfo("Full Pay For Day Of Incident is selected as No", "");
		}
		BaseClass.clickRandomWebElement(didSalaryContinueYes, didSalaryContinueNo);
		if (didSalaryContinueYes.isSelected()) {
			BaseClass.logInfo("Did Salary Continue is selected as Yes", "");
		} else {
			BaseClass.logInfo("Did Salary Continue is selected as No", "");
		}
	}

	private void setClaimantaddress(String isForeign, String postal) {
		if (isForeign.equalsIgnoreCase("Yes")) {
			enterClaimantForeignAddress();
		} else {
			BaseClass.settext(claimantAddress1, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claimant Address1 is set as: ", BaseClass.getattribute(claimantAddress1, "value"));
			BaseClass.settext(claimantAddress2, BaseClass.stringGeneratorl(10));
			BaseClass.logInfo("Claimant Address2 is set as: ", BaseClass.getattribute(claimantAddress2, "value"));
			BaseClass.settext(claimantPostal, postal + Keys.TAB);
			BaseClass.logInfo("Postal Code is set as: ", BaseClass.getattribute(claimantPostal, "value"));
			BaseClass.selectByIndex(claimantCity);
			BaseClass.logInfo("City is selected as: ", BaseClass.getfirstselectedoption(claimantCity));
			BaseClass.logInfo("State is selected as: ", BaseClass.getfirstselectedoption(claimantState));
			BaseClass.logInfo("County is set as: ", BaseClass.getattribute(claimantCounty, "value"));
		}

	}

	private void selectNAICS() {
		irfWindow = BaseClass.getcurrentwindow();
		BaseClass.click(searchNAICS);
		BaseClass.switchToNextWindow(irfWindow, "Search NAICS Code", 3);
		BaseClass.click(searchButton);
		BaseClass.click(firstNAICS);
		BaseClass.switchToWindow(irfWindow, 2);
		BaseClass.logInfo("NAICS Code is selected as: ", BaseClass.getattribute(nAICSCode, "value"));
		BaseClass.logInfo("NAICS Description is selected as: ", BaseClass.getattribute(nAICSDesc, "value"));
	}

}