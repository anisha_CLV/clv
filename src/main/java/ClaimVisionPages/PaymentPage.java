package ClaimVisionPages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class PaymentPage {
	String paymentPage,vendorPage,robPage;
	WebElement element;
	String paymentFromDate="";
	String paymentThroughDate="";
	String CycleFromDate="";
	String CycleThroughDate="";
	String Recurringtotal="";
	int totalrecurringPaymentDays;
	String OneTimetotal;

	private PaymentPage(){}
	private static final PaymentPage payment=new PaymentPage();

	public static PaymentPage getinstance()
	{
		return payment;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="Payments_ClaimPaymentOneTime_saveNewButton1")
	WebElement saveButton;
	@FindBy(id="Payments_ClaimPaymentOneTime_saveMessageLabel")
	WebElement paymentonetimemessagelabel;
	@FindBy(id="Payments_subClaimDropDown")
	WebElement paymentNCSubClaimDropDown;
	@FindBy(id="Payments_ClaimPaymentRecurring_saveDraftButton")
	WebElement draftButtonRecurring;
	
	public void createPayment(HashMap<String, String> hm) {
		String companyid=BaseClass.getqueryresult("Select locationID from Claim where ClaimNumber='"+hm.get("ClaimNumber")+"'").get(0);
		while(!hm.containsKey("IsPendingPay") && !companyid.equals(""))
		{
			if(BaseClass.getqueryresult("Select ispendingpayment from company where companyid="+companyid).get(0).equals("Y"))
			{
				hm.put("IsPendingPay", "Yes");
			}
			else
			{
				companyid=BaseClass.getqueryresult("Select parentid from Company where CompanyId="+companyid).get(0);
			}			
		}
		if(!hm.get("PolicyType").contains("Compensation"))
		{
			BaseClass.selectByIndex(paymentNCSubClaimDropDown,1);
		}
		selectPaymentType(hm);
	   if(hm.get("PaymentType").equals("One Time"))
	   {
		   createPaymentOneTime(hm);
	   }
	   if(hm.get("PaymentType").equals("Recurring"))
	   {
		   createPaymentRecurring(hm);
	   }
	   
	   
	  
	}
	@FindBy(xpath="//input[contains(@id,'Continue')]")
	WebElement continueButton;
	@FindBy(xpath="//input[contains(@id,'SendDiary')]")
	WebElement sendDiary;
	@FindBy(id="Payments_ClaimPaymentOneTime_saveDraftButton")
	WebElement draftButtonOneTimePayment;
	public void createPaymentOneTime(HashMap<String, String> hm) {
		enterPayeeInformation(hm);
		Wait.waitFor(1);
		enterbilldetail();
		Wait.waitFor(1);
		entercheckinformation(hm);
		Wait.waitFor(1);
		if(hm.get("PolicyType").contains("Compensation"))
		{
			enterPaymentDetail(hm);
		}
		else
		{
			enterPaymentDetailNC(hm);
		}
		enterlinesubnotes();
		Wait.waitFor(5);
		if(hm.get("1stPaymentStatus").equalsIgnoreCase("Draft"))
		{
			BaseClass.click(draftButtonOneTimePayment);
	        BaseClass.logInfo("Draft Button is Clicked Successfully", "");
			Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
			BaseClass.Scrolltoelement(paymentonetimemessagelabel);
			Assert.assertTrue(BaseClass.gettext(paymentonetimemessagelabel).contains("is Saved as Draft"));
			BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
			Wait.waitFor(5);
			verifyGridd(hm, "Draft");
		}
		else
		{
		BaseClass.click(saveButton);
        BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
		BaseClass.Scrolltoelement(paymentonetimemessagelabel);
		if(hm.get("Scenario").equals("Unauthorized"))
		{
			if(hm.get("Permission").equals("Yes"))
			{
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),"Payee is flagged as an Unauthorized Vendor");
				BaseClass.logpass("Validation Message should be displayed as Payee is flagged as an Unauthorized Vendor", "Success Message is displayed as Payee is flagged as an Unauthorized Vendor");
				BaseClass.click(continueButton);
				BaseClass.logInfo("Continue Button is clicked", "");
				Wait.waitForTextChanged(paymentonetimemessagelabel,"Payee is flagged as an Unauthorized Vendor");
				Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),"Saved successfully.");
				BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
			}
			else
			{
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),"Payee is flagged as an Unauthorized Vendor");
				BaseClass.logpass("Validation Message should be displayed as Payee is flagged as an Unauthorized Vendor", "Success Message is displayed as Payee is flagged as an Unauthorized Vendor");
				BaseClass.click(sendDiary);
				BaseClass.logInfo("Send Diary Button is clicked", "");
				Wait.waitForTextChanged(paymentonetimemessagelabel,"Payee is flagged as an Unauthorized Vendor");
				Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),"Payee is flagged as an Unauthorized Vendor. Diary was generated!");
				BaseClass.logpass("Validation Message should be displayed as Payee is flagged as an Unauthorized Vendor. Diary was generated!", "Success Message is displayed as Payee is flagged as an Unauthorized Vendor. Diary was generated!");
				Wait.waitFor(5);
				verifyGridd(hm, "Draft");
			}
		}
		if(hm.get("Scenario").equals("Denied"))
		{
			if(hm.get("Permission").equals("Yes"))
			{
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),"The claim for this payment is denied! Please click continue if you want to save this payment or cancel to revise.");
				BaseClass.logpass("Validation Message should be displayed as The claim for this payment is denied! Please click continue if you want to save this payment or cancel to revise.", "Success Message is displayed as The claim for this payment is denied! Please click continue if you want to save this payment or cancel to revise.");
				BaseClass.click(continueButton);
				BaseClass.logInfo("Continue Button is clicked", "");
				Wait.waitForTextChanged(paymentonetimemessagelabel,"The claim for this payment is denied! Please click continue if you want to save this payment or cancel to revise.");
				Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
			}
			else
			{
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),hm.get("ClaimNumber")+" is a Denied Claim.You do not have permission to make Payments on Denied Claims.");
				BaseClass.logpass("Validation Message should be displayed as "+hm.get("ClaimNumber")+" is a Denied Claim.You do not have permission to make Payments on Denied Claims.", "Validation Message is displayed as Payee is flagged as "+hm.get("ClaimNumber")+" is a Denied Claim.You do not have permission to make Payments on Denied Claims.");
				BaseClass.click(sendDiary);
				BaseClass.logInfo("Send Diary Button is clicked", "");
				Wait.waitForTextChanged(paymentonetimemessagelabel,hm.get("ClaimNumber")+" is a Denied Claim.You do not have permission to make Payments on Denied Claims.");
				Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),"Denied Claim. Diary was generated!");
				BaseClass.logpass("Validation Message should be displayed as Denied Claim. Diary was generated!", "Validation Message is displayed as Denied Claim. Diary was generated!");
				Wait.waitFor(5);
				verifyGridd(hm, "Draft");
			}
		}
		if(hm.get("Scenario").equals("Questionable"))
		{
			if(hm.get("Permission").equals("Yes"))
			{
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),hm.get("ClaimNumber")+" is a Questionable Claim.\nPlease click continue if you want to save this payment or cancel to revise.");
				BaseClass.logpass("Validation Message should be displayed as "+hm.get("ClaimNumber")+" is a Questionable Claim.\nPlease click continue if you want to save this payment or cancel to revise.", "Success Message is displayed as "+hm.get("ClaimNumber")+" is a Questionable Claim.Please click continue if you want to save this payment or cancel to revise.");
				BaseClass.click(continueButton);
				BaseClass.logInfo("Continue Button is clicked", "");
				Wait.waitForTextChanged(paymentonetimemessagelabel,hm.get("ClaimNumber")+" is a Questionable Claim.\nPlease click continue if you want to save this payment or cancel to revise.");
				Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
			}
			else
			{
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),hm.get("ClaimNumber")+" is a Questionable Claim.You do not have permission to make Payments on Questionable Claims.");
				BaseClass.logpass("Validation Message should be displayed as "+hm.get("ClaimNumber")+" is a Questionable Claim.You do not have permission to make Payments on Questionable Claims.", "Validation Message is displayed as Payee is flagged as "+hm.get("ClaimNumber")+" is a Questionable Claim.You do not have permission to make Payments on Questionable Claims.");
				BaseClass.click(sendDiary);
				BaseClass.logInfo("Send Diary Button is clicked", "");
				Wait.waitForTextChanged(paymentonetimemessagelabel,hm.get("ClaimNumber")+" is a Questionable Claim.You do not have permission to make Payments on Questionable Claims.");
				Wait.waitFortextPresent(BaseClass.getDriver(), paymentonetimemessagelabel);
				Assert.assertEquals(BaseClass.gettext(paymentonetimemessagelabel),"Questionable Claim. Diary was generated!");
				BaseClass.logpass("Validation Message should be displayed as Questionable Claim. Diary was generated!", "Validation Message is displayed as Questionable Claim. Diary was generated!");
				Wait.waitFor(5);
				verifyGridd(hm, "Draft");
			}
		}
		if(hm.get("Scenario").equals("Select One") || hm.get("Permission").equals("Yes"))
		{
        Assert.assertTrue(BaseClass.gettext(paymentonetimemessagelabel).contains("Saved successfully."));
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		Wait.waitforTextDisappear(paymentonetimemessagelabel);
		Wait.waitFor(2);
		if(hm.containsKey("IsPendingPay"))
		{
		verifyGridd(hm,"Pending");
		hm.put("PaymentStatus", "Pending");
		}
		else
		{
			hm.put("PaymentStatus", "Released");
		}
		}
		}
	}
	@FindBy(id="Payments_ClaimPaymentRecurring_saveNewButton1")
	WebElement saveButtonRecurring;
	@FindBy(id="Payments_ClaimPaymentRecurring_saveMessageLabel")
	WebElement paymentRecurringMessageLabel;
	@FindBy(id="Payments_ClaimPaymentRecurring_FLWButtonNo")
	WebElement addFLWNo;
	@FindBy(id="Payments_ClaimPaymentRecurring_FLWButtonYes")
	WebElement addFLWYes;
	@FindBy(id="Payments_ClaimPaymentRecurring_firstButton")
	WebElement first;
	@FindBy(id="Payments_ClaimPaymentRecurring_waitingButton")
	WebElement waiting;
	@FindBy(id="Payments_ClaimPaymentRecurring_lastButton")
	WebElement last;
	public void createPaymentRecurring(HashMap<String, String> hm) {
		enterPayeeInformationRecurring(hm);
		Wait.waitFor(1);
		entercheckinformationRecurring(hm);
		Wait.waitFor(1);
			enterPaymentDetailRecurring(hm);
		enterlinesubnotes();
		Wait.waitFor(5);
		if(hm.get("1stPaymentStatus").equalsIgnoreCase("Draft"))
		{
			BaseClass.click(draftButtonRecurring);
	        BaseClass.logInfo("Draft Button is Clicked Successfully", "");
			Wait.waitFortextPresent(BaseClass.getDriver(), paymentRecurringMessageLabel);
			BaseClass.Scrolltoelement(paymentRecurringMessageLabel);
			System.out.println(BaseClass.gettext(paymentRecurringMessageLabel));
			Assert.assertTrue(BaseClass.gettext(paymentRecurringMessageLabel).contains("Saved as Draft\n\nDo you wish to add a First/Last/Waiting Payment?"));
			BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		}
		else
		{
		BaseClass.click(saveButtonRecurring);
        BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Wait.waitFortextPresent(BaseClass.getDriver(), paymentRecurringMessageLabel);
		BaseClass.Scrolltoelement(paymentRecurringMessageLabel);
		System.out.println(BaseClass.gettext(paymentRecurringMessageLabel));
		Assert.assertTrue(BaseClass.gettext(paymentRecurringMessageLabel).contains("Saved Successfully\n\nDo you wish to add a First/Last/Waiting Payment?"));
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		}
		if(hm.get("AddFLW").equals("Yes"))
		{
			BaseClass.click(addFLWYes);
			BaseClass.logInfo("Add First/Last/Waiting is clicked as Yes", "");
			Wait.waitFor(2);
			BaseClass.click(first);
			Wait.waitFor(4);
			BaseClass.getDriver().switchTo().alert().accept();
			enterPayeeInformationFLW(hm);
			enterbilldetailFLW();
			entercheckinformationFLW(hm);
			enterPaymentDetailFLW(hm);
			BaseClass.click(saveButtonRecurring);
			BaseClass.logInfo("Save button is clicked", "");
			Wait.waitFor(1);
			Wait.waitFortextPresent(BaseClass.getDriver(), paymentRecurringMessageLabel);
			Assert.assertTrue(BaseClass.gettext(paymentRecurringMessageLabel).contains(" is Saved as Draft"));
			BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
			
			BaseClass.click(waiting);
			Wait.waitFor(2);
			BaseClass.getDriver().switchTo().alert().accept();
			enterPayeeInformationFLW(hm);
			enterbilldetailFLW();
			entercheckinformationFLW(hm);
			enterPaymentDetailFLW(hm);
			BaseClass.click(saveButtonRecurring);
			BaseClass.logInfo("Save button is clicked", "");
			Wait.waitFor(1);
			Wait.waitFortextPresent(BaseClass.getDriver(), paymentRecurringMessageLabel);
			Assert.assertTrue(BaseClass.gettext(paymentRecurringMessageLabel).contains(" is Saved as Draft"));
			BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
			BaseClass.click(last);
			Wait.waitFor(2);
			BaseClass.getDriver().switchTo().alert().accept();
			enterPayeeInformationFLW(hm);
			enterbilldetailFLW();
			entercheckinformationFLW(hm);
			enterPaymentDetailFLW(hm);
			BaseClass.click(saveButtonRecurring);
			BaseClass.logInfo("Save button is clicked", "");
			Wait.waitFor(1);
			Wait.waitFortextPresent(BaseClass.getDriver(), paymentRecurringMessageLabel);
			Assert.assertTrue(BaseClass.gettext(paymentRecurringMessageLabel).contains(" is Saved as Draft"));
			BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
			BaseClass.click(recurring);
			Wait.waitFor(2);
			BaseClass.getDriver().switchTo().alert().accept();
			Wait.waitFor(2);
		}
		else
		{
			BaseClass.click(addFLWNo);
			BaseClass.logInfo("Add First/Last/Waiting is clicked as No", "");
			Wait.waitFor(5);

		}
		if(hm.get("1stPaymentStatus").equalsIgnoreCase("Draft"))
		{
			verifyGridd(hm,"Draft");
		}
		else
		{
		verifyGridd(hm,"Pending");
		approvePayment(hm);
		}
		
	}
	
	@FindBy(id="Payments_ClaimPaymentRecurring_approveButton")
	WebElement approve;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_recurringButton")
	WebElement recurring;
	
	
	private void approvePayment(HashMap<String, String> hm) {
		BaseClass.Scrolltoelement(approve);
		BaseClass.click(approve);
		BaseClass.logInfo("Approve Button is Clicked", "");
		Wait.waitFortextPresent(BaseClass.getDriver(), paymentRecurringMessageLabel);
		BaseClass.Scrolltoelement(paymentRecurringMessageLabel);
		System.out.println(BaseClass.gettext(paymentRecurringMessageLabel));
		Assert.assertTrue(BaseClass.gettext(paymentRecurringMessageLabel).contains("Saved Successfully"));
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		Wait.waitFor(5);
		verifyGridd(hm,"Released");
		hm.put("PaymentStatus", "Pending");
	}
	public void verifyGrid(HashMap<String, String> hm,String status)
	{
		int totalrecord=BaseClass.getDriver().findElements(By.xpath("//table[@id='Payments_PaymentDetailList']//tr[contains(@id,'Payments_PaymentDetailLis')]")).size();
		hm.put("CycleID", BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//table[@id='Payments_PaymentDetailList']//tr[contains(@id,'Payments_PaymentDetailLis')]["+totalrecord+"]/td[2]"))));
		BaseClass.logInfo("Recurring Payment is created with PaymentCycleID: ", hm.get("CycleID"));
		element=BaseClass.getDriver().findElement(By.xpath("//table[@id='Payments_PaymentDetailList']//tr[contains(@id,'Payments_PaymentDetailLis')]["+totalrecord+"]/td[3]"));
		BaseClass.Scrolltoelement(element);
		Wait.waitFor(1);
		Assert.assertTrue(BaseClass.gettext(element).equals(hm.get("PaymentType")));
		BaseClass.logpass("Payment Type in grid should be "+hm.get("PaymentType"), "Payment Type in grid is "+hm.get("PaymentType"));
		String payAmount=BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//table[@id='Payments_PaymentDetailList']//tr[contains(@id,'Payments_PaymentDetailLis')]["+totalrecord+"]/td[7]")));
		if(hm.get("PaymentType").equals("Recurring"))
		{
		Assert.assertEquals(payAmount,getPaymentAmount(hm.get("PayInterval")));
		}
		else
		{
			Assert.assertEquals(payAmount,OneTimetotal);
		}
		BaseClass.logpass("Payment Amount in grid should be:"+payAmount,"Payment Amount in grid is:"+payAmount);
		Assert.assertTrue(BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//table[@id='Payments_PaymentDetailList']//tr[contains(@id,'Payments_PaymentDetailLis')]["+totalrecord+"]/td[8]"))).equals(status));
		BaseClass.logpass("Payment Status in grid should be "+status, "Payment Status in grid is "+status);
		if(status.equals("Pending"))
		{
		BaseClass.click(BaseClass.getDriver().findElement(By.xpath("//table[@id='Payments_PaymentDetailList']//tr[contains(@id,'Payments_PaymentDetailLis')]["+totalrecord+"]/td[1]//input[contains(@id,'selectImageButton')]")));
		Wait.waitFor(5);
		}
	}
	public void verifyGridd(HashMap<String, String> hm,String status)
	{
		if(hm.get("PaymentType").equals("One Time"))
		{
			String claimPaymentID=BaseClass.getqueryresult("Select top 1 claimpaymentid from claimpayment where ClaimNumber='"+hm.get("ClaimNumber")+"' order by EntryDate desc").get(0);
			hm.put("ClaimPaymentID", claimPaymentID);
		BaseClass.Scrolltoelement(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+claimPaymentID+"')]")));
		BaseClass.logpass("Paymnet should be created with status: "+status, "Paymnet "+claimPaymentID+" created with status: "+status);
		Assert.assertTrue(BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+claimPaymentID+"')]/following-sibling::td[1]"))).equals(hm.get("PaymentType")));
		BaseClass.logpass("Payment Type in grid should be "+hm.get("PaymentType"), "Payment Type in grid is "+hm.get("PaymentType"));
		String payAmount=BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+claimPaymentID+"')]/following-sibling::td[5]")));
		Assert.assertEquals(payAmount,OneTimetotal);
		BaseClass.logpass("Payment Amount in grid should be:"+payAmount,"Payment Amount in grid is:"+payAmount);
		Assert.assertTrue(BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+claimPaymentID+"')]/following-sibling::td[6]"))).equals(status));
		BaseClass.logpass("Payment Status in grid should be "+status, "Payment Status in grid is "+status);
		}
		if(hm.get("PaymentType").equals("Recurring"))
		{
			String cycleID=BaseClass.getqueryresult("Select top 1 ClaimPaymentCycleId from ClaimPaymentCycle where ClaimNumber='"+hm.get("ClaimNumber")+"' order by EntryDate desc").get(0);
			hm.put("ClaimPaymentID", cycleID);
			BaseClass.Scrolltoelement(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+cycleID+"')]")));
		BaseClass.logpass("Paymnet should be created with status: "+status, "Paymnet "+cycleID+" created with status: "+status);
		Assert.assertTrue(BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+cycleID+"')]/following-sibling::td[1]"))).equals(hm.get("PaymentType")));
		BaseClass.logpass("Payment Type in grid should be "+hm.get("PaymentType"), "Payment Type in grid is "+hm.get("PaymentType"));
		//String payAmount=BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+cycleID+"')]/following-sibling::td[5]")));
		//Assert.assertEquals(payAmount,getPaymentAmount(hm.get("PayInterval")));
		//BaseClass.logpass("Payment Amount in grid should be:"+payAmount,"Payment Amount in grid is:"+payAmount);
		Assert.assertTrue(BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+cycleID+"')]/following-sibling::td[6]"))).equals(status));
		BaseClass.logpass("Payment Status in grid should be "+status, "Payment Status in grid is "+status);
		if(status.equals("Pending"))
		{
		BaseClass.click(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+cycleID+"')]/preceding-sibling::td//input[contains(@id,'selectImageButton')]")));
		Wait.waitFor(5);
		}
		}
		
	}
private String getPaymentAmount(String payInterval)
{
	double totalPaymentAmount=0;
	System.out.println("total="+Recurringtotal);
	double paymentAmount=BaseClass.getamount(Recurringtotal);
	System.out.println("totalrecurringPaymentDays="+totalrecurringPaymentDays);
     if(payInterval.equals("10 Weeks"))
     {
    	 totalPaymentAmount=paymentAmount*totalrecurringPaymentDays/70;
     }
     if(payInterval.equals("Bi-Weekly"))
     {
    	 totalPaymentAmount=paymentAmount*totalrecurringPaymentDays/14;
     }
     if(payInterval.equals("Daily"))
     {
    	 totalPaymentAmount=paymentAmount*totalrecurringPaymentDays;
     }
     if(payInterval.equals("Monthly"))
     {
    	 totalPaymentAmount=paymentAmount*totalrecurringPaymentDays/30;
     }
     if(payInterval.equals("Semi-Monthly"))
     {
    	 totalPaymentAmount=paymentAmount*totalrecurringPaymentDays/15;
     }
     if(payInterval.equals("Weekly"))
     {
    	 totalPaymentAmount=paymentAmount*totalrecurringPaymentDays/7;
     }
     totalPaymentAmount=Math.round(totalPaymentAmount * 100D) / 100D;
     System.out.println("Total Payment Amount"+totalPaymentAmount);
     return BaseClass.getcurrencyvalue(totalPaymentAmount);
}
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_addPayeeButton")
	WebElement addPayeeRecurringButton;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl00_payeeTypeDropdownList")
	WebElement payeeRecurringType1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl01_payeeTypeDropdownList")
	WebElement payeeRecurringType2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl02_payeeTypeDropdownList")
	WebElement payeeRecurringType3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl00_payeeNameImageButton")
	WebElement payeeRecurringVendorLookup1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl01_payeeNameImageButton")
	WebElement payeeRecurringVendorLookup2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl02_payeeNameImageButton")
	WebElement payeeRecurringVendorLookup3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl00_robSearchImage")
	WebElement payeeRecurringROBLookup1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl01_robSearchImage")
	WebElement payeeRecurringROBLookup2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl02_robSearchImage")
	WebElement payeeRecurringROBLookup3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl00_payeeNameTextBox")
	WebElement payeeRecurringname1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl01_payeeNameTextBox")
	WebElement payeeRecurringname2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PayeeInfoControl_payeeRepeater_ctl02_payeeNameTextBox")
	WebElement payeeRecurringname3;
	private void enterPayeeInformationRecurring(HashMap<String, String> hm) {
		
		Wait.waitforelement(BaseClass.getDriver(), payeeRecurringType1);
		for(int i=0;i< hm.get("PayeeType").split("\\|").length;i++)
		{
			paymentPage=BaseClass.getcurrentwindow();
			if(i>0)
			{
				BaseClass.click(addPayeeRecurringButton);
				BaseClass.logInfo("Add Payee Button is clicked", "");
			}
			System.out.println("Selecting Payee Type as: "+hm.get("PayeeType").split("\\|")[i]);
			if(i==0)
			{
				BaseClass.selectByVisibleText(payeeRecurringType1, hm.get("PayeeType").split("\\|")[0]);
				BaseClass.logInfo("Payee Type 1 is selected as", BaseClass.getfirstselectedoption(payeeRecurringType1));
				if(hm.get("PayeeType").split("\\|")[0].equals("Vendor"))
				{
					
					BaseClass.click(payeeRecurringVendorLookup1);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[0].equals("ROB"))
				{
					
					BaseClass.click(payeeRecurringROBLookup1);
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeeRecurringname1);
			}
			if(i==1)
			{
				BaseClass.selectByVisibleText(payeeRecurringType2, hm.get("PayeeType").split("\\|")[1]);
				BaseClass.logInfo("Payee Type 2 is selected as", BaseClass.getfirstselectedoption(payeeRecurringType2));
				if(hm.get("PayeeType").split("\\|")[1].equals("Vendor"))
				{
					
					BaseClass.click(payeeRecurringVendorLookup2);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[1].equals("ROB"))
				{
					
					BaseClass.click(payeeRecurringROBLookup2);	
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeeRecurringname2);
			}
			if(i==2)
			{
				BaseClass.selectByVisibleText(payeeRecurringType3, hm.get("PayeeType").split("\\|")[2]);
				BaseClass.logInfo("Payee Type 3 is selected as", BaseClass.getfirstselectedoption(payeeRecurringType3));
				if(hm.get("PayeeType").split("\\|")[2].equals("Vendor"))
				{
					
					BaseClass.click(payeeRecurringVendorLookup3);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[2].equals("ROB"))
				{
					
					BaseClass.click(payeeRecurringROBLookup3);	
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeeRecurringname3);
			}
		}
			
	}
	private void enterlinesubnotes() {
		// TODO Auto-generated method stub
		
	}
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_moreButton1")
	WebElement addlineitems;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_fromDateTextBox")
	WebElement paymentFromDateTextBox;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_thruDateTextBox")
	WebElement paymentThroughDateTextBox;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_totalTextBox")
	WebElement paymentTotal;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_payCategoryDown")
	WebElement paymentCategory1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl02_payCategoryDown")
	WebElement paymentCategory2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl03_payCategoryDown")
	WebElement paymentCategory3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_paySubCategoryDown")
	WebElement paymentSubCategory1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl02_paySubCategoryDown")
	WebElement paymentSubCategory2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl03_paySubCategoryDown")
	WebElement paymentSubCategory3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_paymentFromTextBox")
	WebElement paymentFromDate1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl02_paymentFromTextBox")
	WebElement paymentFromDate2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl03_paymentFromTextBox")
	WebElement paymentFromDate3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_paymentThruTextBox")
	WebElement paymentThroughDate1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl02_paymentThruTextBox")
	WebElement paymentThroughDate2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl03_paymentThruTextBox")
	WebElement paymentThroughDate3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl01_amountTextBox")
	WebElement paymentAmount1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl02_amountTextBox")
	WebElement paymentAmount2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForCompControl_lineItemRepeater_ctl03_amountTextBox")
	WebElement paymentAmount3;
	private void enterPaymentDetail(HashMap<String, String> hm) {
		String[] PaymentCategory=hm.get("PaymentCategory").split("\\|");
		String[] PaymentSubCategory=hm.get("PaymentSubCategory").split("\\|");
		String claimPaymentDate=BaseClass.getqueryresult("select convert(varchar, max(ThroughDate), 101) as FromDate"
				+ " from claimpayment where ClaimId=(select claimid from claim where ClaimNumber='"+hm.get("ClaimNumber")+"')").get(0);
		paymentFromDate="";
		paymentThroughDate="";
		for(int i=1;i<=Integer.parseInt(hm.get("LineItems"));i++)
		{
			if(paymentFromDate.equals("") && i==1){
				if(claimPaymentDate.equals("")){
					paymentFromDate=BaseClass.futuredatefromdate(BaseClass.getCurrentDate(), BaseClass.generateRandomNumberAsInteger(5));
				}
				else{
					paymentFromDate=BaseClass.futuredatefromdate(claimPaymentDate, 3);
				}}
				else
				{
					paymentFromDate=BaseClass.pastdatefromdate(paymentFromDate,1);
				}
				if(paymentThroughDate.equals("") &&i==1)
				{
				paymentThroughDate=BaseClass.futuredatefromdate(paymentFromDate, BaseClass.generateRandomNumberAsInteger(5));
				}
				else
				{
					paymentThroughDate=BaseClass.futuredatefromdate(paymentThroughDate,1);
				}
			String total=BaseClass.getattribute(paymentTotal, "value");
			if(i>1)
			{ 
			BaseClass.click(addlineitems);
			}
			if(i==1)
			{
				BaseClass.selectByVisibleText(paymentCategory1,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentCategory1));
				Wait.waitFor(10);
				BaseClass.selectByVisibleText(paymentSubCategory1,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentSubCategory1));
				BaseClass.settext(paymentFromDate1, paymentFromDate + Keys.TAB);
				BaseClass.logInfo("From Date for Line Item 1 is set as:", BaseClass.getattribute(paymentFromDate1, "value"));
				Wait.waitFor(3);
				//Wait.waitForContainValue(paymentFromDateTextBox,BaseClass.modifyDate(paymentFromDate));
				BaseClass.settext(paymentThroughDate1, paymentThroughDate + Keys.TAB);
				BaseClass.logInfo("From Through for Line Item 1 is set as:", BaseClass.getattribute(paymentThroughDate1, "value"));
				//Wait.waitForContainValue(paymentThroughDateTextBox,BaseClass.modifyDate(paymentThroughDate));
				Wait.waitFor(3);
				BaseClass.settext(paymentAmount1, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 1 is set as:", BaseClass.getattribute(paymentAmount1, "value"));
				Wait.waitForNotContainValue(paymentTotal,total);
			}
			if(i==2)
			{				
				Wait.waitforelement(BaseClass.getDriver(), paymentCategory2);
				BaseClass.selectByVisibleText(paymentCategory2,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentCategory2));
				Wait.waitFor(10);
				BaseClass.selectByVisibleText(paymentSubCategory2,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentSubCategory2));
				BaseClass.settext(paymentFromDate2, paymentFromDate + Keys.TAB);
				BaseClass.logInfo("From Date for Line Item 2 is set as:", BaseClass.getattribute(paymentFromDate2, "value"));
				Wait.waitFor(3);
				//Wait.waitForContainValue(paymentFromDateTextBox,BaseClass.modifyDate(paymentFromDate));
				BaseClass.settext(paymentThroughDate2, paymentThroughDate + Keys.TAB);
				BaseClass.logInfo("From Through for Line Item 2 is set as:", BaseClass.getattribute(paymentThroughDate2, "value"));
				//Wait.waitForContainValue(paymentThroughDateTextBox,BaseClass.modifyDate(paymentThroughDate));
				Wait.waitFor(3);
				BaseClass.settext(paymentAmount2, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 2 is set as:", BaseClass.getattribute(paymentAmount2, "value"));
				Wait.waitForNotContainValue(paymentTotal,total);
			}
			if(i==3)
			{
				Wait.waitforelement(BaseClass.getDriver(), paymentCategory3);
				BaseClass.selectByVisibleText(paymentCategory3,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentCategory3));
				Wait.waitFor(10);
				BaseClass.selectByVisibleText(paymentSubCategory3,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentSubCategory3));
				Wait.waitFor(3);
				BaseClass.settext(paymentFromDate3, paymentFromDate + Keys.TAB);
				BaseClass.logInfo("From Date for Line Item 3 is set as:", BaseClass.getattribute(paymentFromDate3, "value"));
				Wait.waitFor(3);
				//Wait.waitForContainValue(paymentFromDateTextBox,BaseClass.modifyDate(paymentFromDate));
				BaseClass.settext(paymentThroughDate3, paymentThroughDate + Keys.TAB);
				BaseClass.logInfo("From Through for Line Item 3 is set as:", BaseClass.getattribute(paymentThroughDate3, "value"));
				//Wait.waitForContainValue(paymentThroughDateTextBox,BaseClass.modifyDate(paymentThroughDate));
				Wait.waitFor(3);
				BaseClass.settext(paymentAmount3, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 3 is set as:", BaseClass.getattribute(paymentAmount3, "value"));
				Wait.waitForNotContainValue(paymentTotal,total);
			}
			OneTimetotal=BaseClass.getattribute(paymentTotal, "value");
			System.out.println(BaseClass.getattribute(paymentTotal, "value"));
		}
	}
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_moreButton1")
	WebElement addlineitemsRecurring;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_totalTextBox")
	WebElement paymentRecurringTotal;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl01_payCategoryDown")
	WebElement paymentRecurringCategory1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl02_payCategoryDown")
	WebElement paymentRecurringCategory2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl03_payCategoryDown")
	WebElement paymentRecurringCategory3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl01_paySubCategoryDown")
	WebElement paymentRecurringSubCategory1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl02_paySubCategoryDown")
	WebElement paymentRecurringSubCategory2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl03_paySubCategoryDown")
	WebElement paymentRecurringSubCategory3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl01_amountTextBox")
	WebElement paymentRecurringAmount1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl02_amountTextBox")
	WebElement paymentRecurringAmount2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl03_amountTextBox")
	WebElement paymentRecurringAmount3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl01_weeklyRateTextBox")
	WebElement weeklyAmount1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl02_weeklyRateTextBox")
	WebElement weeklyAmount2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl03_weeklyRateTextBox")
	WebElement weeklyAmount3;
	private void enterPaymentDetailRecurring(HashMap<String, String> hm) {
		String[] PaymentCategory=hm.get("PaymentCategory").split("\\|");
		String[] PaymentSubCategory=hm.get("PaymentSubCategory").split("\\|");
		for(int i=1;i<=Integer.parseInt(hm.get("LineItems"));i++)
		{
			Recurringtotal=BaseClass.getattribute(paymentRecurringTotal, "value");
			if(i>1)
			{ 
			BaseClass.click(addlineitemsRecurring);
			}
			if(i==1)
			{
				BaseClass.selectByVisibleText(paymentRecurringCategory1,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentRecurringCategory1));
				Wait.waitFor(3);
				BaseClass.selectByVisibleText(paymentRecurringSubCategory1,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentRecurringSubCategory1));
				Wait.waitFor(3);
				BaseClass.settext(weeklyAmount1, BaseClass.randDouble(10,99)+Keys.TAB);
				Wait.waitFor(3);
				//BaseClass.settext(paymentRecurringAmount1, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 1 is set as:", BaseClass.getattribute(paymentRecurringAmount1, "value"));
				Wait.waitForNotContainValue(paymentRecurringTotal,Recurringtotal);
			}
			if(i==2)
			{
				
				Wait.waitforelement(BaseClass.getDriver(), paymentRecurringCategory2);
				BaseClass.selectByVisibleText(paymentRecurringCategory2,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentRecurringCategory2));
				Wait.waitFor(3);
				BaseClass.selectByVisibleText(paymentRecurringSubCategory2,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentRecurringSubCategory2));
				Wait.waitFor(3);
				BaseClass.settext(weeklyAmount2, BaseClass.randDouble(10,99)+Keys.TAB);
				Wait.waitFor(3);
				//BaseClass.settext(paymentRecurringAmount2, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 2 is set as:", BaseClass.getattribute(paymentRecurringAmount2, "value"));
				Wait.waitForNotContainValue(paymentRecurringTotal,Recurringtotal);
			}
			if(i==3)
			{
				Wait.waitforelement(BaseClass.getDriver(), paymentRecurringCategory3);
				BaseClass.selectByVisibleText(paymentRecurringCategory3,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentRecurringCategory3));
				Wait.waitFor(3);
				BaseClass.selectByVisibleText(paymentRecurringSubCategory3,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentRecurringSubCategory3));
				Wait.waitFor(3);
				BaseClass.settext(weeklyAmount3, BaseClass.randDouble(10,99)+Keys.TAB);
				Wait.waitFor(3);
				//BaseClass.settext(paymentRecurringAmount3, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 3 is set as:", BaseClass.getattribute(paymentRecurringAmount3, "value"));
				Wait.waitForNotContainValue(paymentRecurringTotal,Recurringtotal);
			}
			System.out.println(BaseClass.getattribute(paymentRecurringTotal, "value"));
		}
		Recurringtotal=BaseClass.getattribute(paymentRecurringTotal, "value");
		System.out.println(Recurringtotal);
	}
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_modeTypeDropDown")
	WebElement modeTypeOneTime;
	
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_bankModeDropDown")
	WebElement paymentModeOneTime;
	
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_checkDateTextBox")
	WebElement paymentOneTimeCheckDate;
	
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_checkNumberTextBox")
	WebElement paymentOneTimeCheckNumber;
	
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_bankAccountTextBox")
	WebElement paymentOneTimeBankAccount;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_releaseReasonTextBox")
	WebElement paymentOneTimeReason;
	
	private void entercheckinformation(HashMap<String, String> hm) {
		BaseClass.selectByVisibleText(modeTypeOneTime, hm.get("ModeType"));
		BaseClass.logInfo("Mode Type is selected as:", BaseClass.getfirstselectedoption(modeTypeOneTime));
		BaseClass.selectByIndex(paymentModeOneTime);
		BaseClass.logInfo("Payment Mode is selected as:", BaseClass.getfirstselectedoption(paymentModeOneTime));
		Wait.waitForValuePresent(BaseClass.getDriver(), paymentOneTimeBankAccount);
	    if(hm.get("ModeType").equals("Journal Voucher"))
	    {
	    	Wait.waitforelement(BaseClass.getDriver(),paymentOneTimeCheckDate);
	    	BaseClass.settext(paymentOneTimeCheckDate,BaseClass.getCurrentDate());
			BaseClass.logInfo("Check Date is set as:", BaseClass.getattribute(paymentOneTimeCheckDate, "value"));
	    	BaseClass.settext(paymentOneTimeCheckNumber,BaseClass.enterRandomNumber(6));
			BaseClass.logInfo("Check Number is set as:", BaseClass.getattribute(paymentOneTimeCheckNumber, "value"));

	    }
    	BaseClass.settext(paymentOneTimeReason,BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Payment Reason is set as:", BaseClass.getattribute(paymentOneTimeReason, "value"));
        

	       
	}
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_modeTypeDropDown")
	WebElement modeTypeRecurring;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_bankModeDropDown")
	WebElement paymentModeRecurring;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_bankAccountTextBox")
	WebElement paymentRecurringBankAccount;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_firstFromDateTextBox")
	WebElement paymentFromDateRecurring;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_firstThruDateTextBox")
	WebElement paymentThroughDateRecurring;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_payIntervalDropDown")
	WebElement payInterval;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_weeksTextBox")
	WebElement weeks;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_daysTextBox")
	WebElement days;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentInfoItem_PaymentRecurringControl_ForeignCurrencyDropDown")
	WebElement currencyCode;
	
	
	private void entercheckinformationRecurring(HashMap<String, String> hm) {
		Wait.waitFor(2);
		BaseClass.selectByIndex(modeTypeRecurring,0);
		BaseClass.selectByVisibleText(modeTypeRecurring, hm.get("ModeType"));
		BaseClass.logInfo("Mode Type is selected as:", BaseClass.getfirstselectedoption(modeTypeRecurring));
		BaseClass.selectByIndex(paymentModeRecurring);
		BaseClass.logInfo("Payment Mode is selected as:", BaseClass.getfirstselectedoption(paymentModeRecurring));
		Wait.waitForValuePresent(BaseClass.getDriver(), paymentRecurringBankAccount);
		String claimPaymentDate=BaseClass.getqueryresult("select convert(varchar, max(ThroughDate), 101) as FromDate"
				+ " from claimpayment where ClaimId=(select claimid from claim where ClaimNumber='"+hm.get("ClaimNumber")+"')").get(0);
		CycleFromDate=BaseClass.getqueryresult("select convert(varchar, max(StopDate), 101) as FromDate"
				+ " from ClaimPaymentCycle where ClaimNumber='"+hm.get("ClaimNumber")+"' and stopDate>'"+claimPaymentDate+"'").get(0);
		
		if(CycleFromDate.equals(""))
		{
			if(claimPaymentDate.equals(""))
			{
			CycleFromDate=BaseClass.pastdatefromdate("",30);
			}
			else
			{
				CycleFromDate=claimPaymentDate;
			}
		}
		if(BaseClass.daysBetweenDates(CycleFromDate,BaseClass.getCurrentDate())<15)
		{
			claimPaymentDate=BaseClass.getqueryresult("select convert(varchar, min(FromDate), 101) as FromDate"
					+ " from claimpayment where ClaimId=(select claimid from claim where ClaimNumber='"+hm.get("ClaimNumber")+"')").get(0);
			CycleThroughDate=BaseClass.getqueryresult("select convert(varchar, min(StartDate), 101) as FromDate"
					+ " from ClaimPaymentCycle where ClaimNumber='"+hm.get("ClaimNumber")+"' and StartDate<'"+claimPaymentDate+"'").get(0);
			if(CycleThroughDate.equals("") && !claimPaymentDate.equals(""))
			{
				CycleThroughDate=claimPaymentDate;
			}
		}
		if(CycleThroughDate.equals(""))
		{
			BaseClass.settext(paymentFromDateRecurring, BaseClass.futuredatefromdate(CycleFromDate, 1));
			BaseClass.settext(paymentThroughDateRecurring, BaseClass.futuredatefromdate(CycleFromDate, BaseClass.generateRandomNumberAsInteger(30, 40))+Keys.TAB);
		}
		else
		{
			BaseClass.settext(paymentFromDateRecurring, BaseClass.pastdatefromdate(CycleThroughDate, BaseClass.generateRandomNumberAsInteger(30, 40)));
			BaseClass.settext(paymentThroughDateRecurring, BaseClass.pastdatefromdate(CycleThroughDate, 1)+Keys.TAB);
		}
		CycleFromDate=BaseClass.getattribute(paymentFromDateRecurring, "value");
		CycleThroughDate=BaseClass.getattribute(paymentThroughDateRecurring, "value");
		Wait.waitForValuePresent(BaseClass.getDriver(), weeks);
		totalrecurringPaymentDays=BaseClass.daysBetweenDatesCycle(BaseClass.getattribute(paymentFromDateRecurring, "value"),BaseClass.getattribute(paymentThroughDateRecurring, "value"),hm.get("CustomerCode"));
		System.out.println(totalrecurringPaymentDays);
		if(hm.get("CustomerCode").toUpperCase().contains("NYLAW"))
		{
		Assert.assertEquals(BaseClass.getattribute(weeks, "value"), totalrecurringPaymentDays/5+"");
		Assert.assertEquals(BaseClass.getattribute(days, "value"), totalrecurringPaymentDays%5+"");
		
		}
		else
			{
			Assert.assertEquals(BaseClass.getattribute(weeks, "value"), totalrecurringPaymentDays/7+"");
			Assert.assertEquals(BaseClass.getattribute(days, "value"), totalrecurringPaymentDays%7+"");
			}
		BaseClass.logpass("Weeks value should be:"+BaseClass.getattribute(weeks, "value"), "Weeks value is:"+BaseClass.getattribute(weeks, "value"));
		BaseClass.logpass("Days value should be:"+BaseClass.getattribute(days, "value"), "Days value is:"+BaseClass.getattribute(days, "value"));

	BaseClass.selectByVisibleText(payInterval, hm.get("PayInterval"));
BaseClass.selectByIndex(currencyCode);
	    	       
	}
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_invoiceDateTextBox")
	WebElement billSubmittedDate;
	
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_invoiceRcvdDateTextBox")
	WebElement billReceivedDate;
	
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_invoiceNumberTextBox")
	WebElement invoiceNumber;
	
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentOneTimeControl_invoiceAmtTextbox")
	WebElement invoiceAmount;
	private void enterbilldetail() {
		BaseClass.settext(billSubmittedDate, BaseClass.pastdatefromTodayDate(10));
		BaseClass.logInfo("Bill Submitted Date is set as:", BaseClass.getattribute(billSubmittedDate, "value"));
		BaseClass.settext(billReceivedDate, BaseClass.pastdatefromTodayDate(8));
		BaseClass.logInfo("Bill Received Date is set as:", BaseClass.getattribute(billReceivedDate, "value"));
		BaseClass.settext(invoiceNumber, BaseClass.enterRandomNumber(5));
		BaseClass.logInfo("Invoice Number is set as:", BaseClass.getattribute(invoiceNumber, "value"));
		BaseClass.settext(invoiceAmount, BaseClass.enterRandomNumber(4));
		BaseClass.logInfo("Invoice Amount is set as:", BaseClass.getattribute(invoiceAmount, "value"));
	}
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_addPayeeButton")
	WebElement addPayeeButton;
	@FindBy(id="vendorNumberTextBox1")
	WebElement vendorID;
	@FindBy(id="searchButton1")
	WebElement paymnetVendorSearchButton;
	@FindBy(xpath="//table[@id='CustomizationGrid1_CustomGridView']//td/input")
	WebElement paymentVendorSelectButton;
	@FindBy(id="ROBSearch_okButton")
	WebElement robSearchOK;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl00_payeeTypeDropdownList")
	WebElement payeeType1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl01_payeeTypeDropdownList")
	WebElement payeeType2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl02_payeeTypeDropdownList")
	WebElement payeeType3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl00_payeeNameImageButton")
	WebElement payeeVendorLookup1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl01_payeeNameImageButton")
	WebElement payeeVendorLookup2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl02_payeeNameImageButton")
	WebElement payeeVendorLookup3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl00_robSearchImage")
	WebElement payeeROBLookup1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl01_robSearchImage")
	WebElement payeeROBLookup2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl02_robSearchImage")
	WebElement payeeROBLookup3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl00_payeeNameTextBox")
	WebElement payeename1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl01_payeeNameTextBox")
	WebElement payeename2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PayeeInfoControl_payeeRepeater_ctl02_payeeNameTextBox")
	WebElement payeename3;
	private void enterPayeeInformation(HashMap<String, String> hm) {
		
		Wait.waitforelement(BaseClass.getDriver(), billSubmittedDate);
		for(int i=0;i< hm.get("PayeeType").split("\\|").length;i++)
		{
			paymentPage=BaseClass.getcurrentwindow();
			if(i>0)
			{
				BaseClass.click(addPayeeButton);
				BaseClass.logInfo("Add Payee Button is clicked", "");
			}
			System.out.println("Selecting Payee Type as: "+hm.get("PayeeType").split("\\|")[i]);
			if(i==0)
			{
				BaseClass.selectByVisibleText(payeeType1, hm.get("PayeeType").split("\\|")[0]);
				BaseClass.logInfo("Payee Type 1 is selected as", BaseClass.getfirstselectedoption(payeeType1));
				if(hm.get("PayeeType").split("\\|")[0].equals("Vendor"))
				{
					
					BaseClass.click(payeeVendorLookup1);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[0].equals("ROB"))
				{
					
					BaseClass.click(payeeROBLookup1);
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeename1);
			}
			if(i==1)
			{
				BaseClass.selectByVisibleText(payeeType2, hm.get("PayeeType").split("\\|")[1]);
				BaseClass.logInfo("Payee Type 2 is selected as", BaseClass.getfirstselectedoption(payeeType2));
				if(hm.get("PayeeType").split("\\|")[1].equals("Vendor"))
				{
					
					BaseClass.click(payeeVendorLookup2);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[1].equals("ROB"))
				{
					
					BaseClass.click(payeeROBLookup2);	
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeename2);
			}
			if(i==2)
			{
				BaseClass.selectByVisibleText(payeeType3, hm.get("PayeeType").split("\\|")[2]);
				BaseClass.logInfo("Payee Type 3 is selected as", BaseClass.getfirstselectedoption(payeeType3));
				if(hm.get("PayeeType").split("\\|")[2].equals("Vendor"))
				{
					
					BaseClass.click(payeeVendorLookup3);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[2].equals("ROB"))
				{
					
					BaseClass.click(payeeROBLookup3);	
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeename3);
			}
		}
			
	}
	
	private void selectROB(HashMap<String, String> hm)
	{
		robPage=BaseClass.switchToNextWindow(paymentPage,"Search Payee", 2);
		BaseClass.logInfo("ROB Search Page is displayed", "");
		element=BaseClass.getDriver().findElement(By.id("ROBSearch_ROBDataGrid_ctl0"+BaseClass.selectRandomNumber(2, 3)+"_ROBCheckBox"));
		BaseClass.click(element);
		BaseClass.logInfo("ROB Record is selected successfully", "");
		BaseClass.click(robSearchOK);
		BaseClass.switchToWindow(paymentPage,1);
		BaseClass.logInfo("ROB is selected successfully", "");
	}
	private void selectVendor(HashMap<String, String> hm)
	{
		List<String> list=null;
		vendorPage=BaseClass.switchToNextWindow(paymentPage,"Vendor Search", 2);
		BaseClass.logInfo("Vendor Search Page is displayed", "");
		if(hm.get("IsVendorUnauthorized").equalsIgnoreCase("Yes"))
		{
			list = BaseClass.getqueryresult("select VendorId,VendorName,Address1,Address2,City,StateCode,ZipCode from Vendor where (IsDeactivate='N' or IsDeactivate is null) and VendorName!='' and IsUnauthorized='Y'");
		}
		else
		{
		list = BaseClass.getqueryresult("select VendorId,VendorName,Address1,Address2,City,StateCode,ZipCode from Vendor where (IsDeactivate='N' or IsDeactivate is null) and VendorName!='' and (IsUnauthorized='N' or IsUnauthorized is null)");
		}
			BaseClass.settext(vendorID, list.get(0));
			BaseClass.logInfo("Vendor# is set as:", BaseClass.getattribute(vendorID, "value"));
			BaseClass.click(paymnetVendorSearchButton);
			BaseClass.logInfo("Vendor Search Button is clicked", "");
			BaseClass.click(paymentVendorSelectButton);
			BaseClass.logInfo("Vendor select Button is clicked", "");
		    BaseClass.switchToWindow(paymentPage,1);
			BaseClass.logInfo("Vendor is selected", "");
	}
	@FindBy(id="Payments_paymentTypeDropdown")
	WebElement paymentTypeDropDown;
	private void selectPaymentType(HashMap<String, String> hm) {
		if(hm.get("CustomerCode").contains("MDMTA"))
		{
			Assert.assertTrue(BaseClass.getfirstselectedoption(paymentTypeDropDown).equals("One Time"));
			BaseClass.logpass("Payment Type should be set to One Time by default", "Payment Type is set to One Time by default");
		}
		else
		{
		BaseClass.selectByVisibleText(paymentTypeDropDown, hm.get("PaymentType"));
		BaseClass.logInfo("Payment Type is selected as", BaseClass.getfirstselectedoption(paymentTypeDropDown));
		}
	}
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_moreButton1")
	WebElement addlineitemsNC;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_totalTextBox")
	WebElement paymentNCTotal;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_payCategoryDown")
	WebElement paymentNCCategory1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl02_payCategoryDown")
	WebElement paymentNCCategory2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl03_payCategoryDown")
	WebElement paymentNCCategory3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_paySubCategoryDown")
	WebElement paymentNCSubCategory1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl02_paySubCategoryDown")
	WebElement paymentNCSubCategory2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl03_paySubCategoryDown")
	WebElement paymentNCSubCategory3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_paymentFromTextBox")
	WebElement paymentNCFromDate1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl02_paymentFromTextBox")
	WebElement paymentNCFromDate2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl03_paymentFromTextBox")
	WebElement paymentNCFromDate3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_paymentThruTextBox")
	WebElement paymentNCThroughDate1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl02_paymentThruTextBox")
	WebElement paymentNCThroughDate2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl03_paymentThruTextBox")
	WebElement paymentNCThroughDate3;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl01_amountTextBox")
	WebElement paymentNCAmount1;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl02_amountTextBox")
	WebElement paymentNCAmount2;
	@FindBy(id="Payments_ClaimPaymentOneTime_ClaimPaymentOneTimeControl_PaymentDetailForNonCompControl_lineItemRepeater_ctl03_amountTextBox")
	WebElement paymentNCAmount3;
	private void enterPaymentDetailNC(HashMap<String, String> hm) {
		String claimPaymentDate=BaseClass.getqueryresult("select convert(varchar, max(ThroughDate), 101) as FromDate"
				+ " from claimpayment where ClaimId=(select claimid from claim where ClaimNumber='"+hm.get("ClaimNumber")+"')").get(0);
		for(int i=1;i<=Integer.parseInt(hm.get("LineItems"));i++)
		{
			if(paymentFromDate.equals("") && i==1){
				if(claimPaymentDate.equals("")){
					paymentFromDate=BaseClass.futuredatefromdate(BaseClass.getCurrentDate(), BaseClass.generateRandomNumberAsInteger(5));
				}
				else{
					paymentFromDate=BaseClass.futuredatefromdate(claimPaymentDate, 3);
				}}
				else
				{
					paymentFromDate=BaseClass.pastdatefromdate(paymentFromDate,1);
				}
				if(paymentThroughDate.equals("") &&i==1)
				{
				paymentThroughDate=BaseClass.futuredatefromdate(paymentFromDate, BaseClass.generateRandomNumberAsInteger(5));
				}
				else
				{
					paymentThroughDate=BaseClass.futuredatefromdate(paymentThroughDate,1);
				}
			String total=BaseClass.getattribute(paymentNCTotal, "value");
			if(i>1)
			{ 
				Wait.waitFor(5);
			BaseClass.click(addlineitemsNC);
			}
			if(i==1)
			{
					if(hm.containsKey("PaymentCategoryNC"))
					{
					BaseClass.selectByVisibleText(paymentNCCategory1,hm.get("PaymentCategoryNC"));
					}
					else
					{
						BaseClass.selectByIndex(paymentNCCategory1);
					}
					BaseClass.logInfo("Payment Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentNCCategory1));
					Wait.waitFor(3);
					BaseClass.selectByIndex(paymentNCSubCategory1);
					BaseClass.logInfo("Payment Sub Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentNCSubCategory1));
	
				BaseClass.settext(paymentNCFromDate1, paymentFromDate + Keys.TAB);
				BaseClass.logInfo("From Date for Line Item 1 is set as:", BaseClass.getattribute(paymentNCFromDate1, "value"));
				Wait.waitFor(3);
				//Wait.waitForContainValue(paymentNCFromDateTextBox,BaseClass.modifyDate(paymentNCFromDate));
				BaseClass.settext(paymentNCThroughDate1, paymentThroughDate + Keys.TAB);
				BaseClass.logInfo("From Through for Line Item 1 is set as:", BaseClass.getattribute(paymentNCThroughDate1, "value"));
				//Wait.waitForContainValue(paymentNCThroughDateTextBox,BaseClass.modifyDate(paymentNCThroughDate));
				Wait.waitFor(3);
				BaseClass.settext(paymentNCAmount1, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 1 is set as:", BaseClass.getattribute(paymentNCAmount1, "value"));
				Wait.waitForNotContainValue(paymentNCTotal,total);
			}
			if(i==2)
			{
				
				Wait.waitforelement(BaseClass.getDriver(), paymentNCCategory2);
				
				if(hm.containsKey("PaymentCategoryNC"))
				{
				BaseClass.selectByVisibleText(paymentNCCategory2,hm.get("PaymentCategoryNC"));
				}
				else
				{
					BaseClass.selectByIndex(paymentNCCategory2);
				}				
				BaseClass.logInfo("Payment Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentNCCategory2));
				Wait.waitFor(3);
				BaseClass.selectByIndex(paymentNCSubCategory2);
				BaseClass.logInfo("Payment Sub Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentNCSubCategory2));

				BaseClass.settext(paymentNCFromDate2, paymentFromDate + Keys.TAB);
				BaseClass.logInfo("From Date for Line Item 2 is set as:", BaseClass.getattribute(paymentNCFromDate2, "value"));
				Wait.waitFor(3);
				//Wait.waitForContainValue(paymentNCFromDateTextBox,BaseClass.modifyDate(paymentNCFromDate));
				BaseClass.settext(paymentNCThroughDate2, paymentThroughDate + Keys.TAB);
				BaseClass.logInfo("From Through for Line Item 2 is set as:", BaseClass.getattribute(paymentNCThroughDate2, "value"));
				//Wait.waitForContainValue(paymentNCThroughDateTextBox,BaseClass.modifyDate(paymentNCThroughDate));
				Wait.waitFor(3);
				BaseClass.settext(paymentNCAmount2, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 2 is set as:", BaseClass.getattribute(paymentNCAmount2, "value"));
				Wait.waitForNotContainValue(paymentNCTotal,total);
			}
			if(i==3)
			{
				Wait.waitforelement(BaseClass.getDriver(), paymentNCCategory3);
				if(hm.containsKey("PaymentCategoryNC"))
					{
					BaseClass.selectByVisibleText(paymentNCCategory3,hm.get("PaymentCategoryNC"));
					}
					else
					{
						BaseClass.selectByIndex(paymentNCCategory3);
					}
					BaseClass.logInfo("Payment Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentNCCategory3));
					Wait.waitFor(3);
					BaseClass.selectByIndex(paymentNCSubCategory3);
					BaseClass.logInfo("Payment Sub Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentNCSubCategory3));
				Wait.waitFor(3);
				BaseClass.settext(paymentNCFromDate3, paymentFromDate + Keys.TAB);
				BaseClass.logInfo("From Date for Line Item 3 is set as:", BaseClass.getattribute(paymentNCFromDate3, "value"));
				Wait.waitFor(3);
				//Wait.waitForContainValue(paymentNCFromDateTextBox,BaseClass.modifyDate(paymentNCFromDate));
				BaseClass.settext(paymentNCThroughDate3, paymentThroughDate + Keys.TAB);
				BaseClass.logInfo("From Through for Line Item 3 is set as:", BaseClass.getattribute(paymentNCThroughDate3, "value"));
				//Wait.waitForContainValue(paymentNCThroughDateTextBox,BaseClass.modifyDate(paymentNCThroughDate));
				Wait.waitFor(3);
				BaseClass.settext(paymentNCAmount3, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 3 is set as:", BaseClass.getattribute(paymentNCAmount3, "value"));
				Wait.waitForNotContainValue(paymentNCTotal,total);
			}
			OneTimetotal=BaseClass.getattribute(paymentNCTotal, "value");
			System.out.println(BaseClass.getattribute(paymentNCTotal, "value"));
		}
	}
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_addPayeeButton")
	WebElement addPayeeButtonFLW;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl00_payeeTypeDropdownList")
	WebElement payeeTypeFLW1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl01_payeeTypeDropdownList")
	WebElement payeeTypeFLW2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl02_payeeTypeDropdownList")
	WebElement payeeTypeFLW3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl00_payeeNameImageButton")
	WebElement payeeVendorLookupFLW1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl01_payeeNameImageButton")
	WebElement payeeVendorLookupFLW2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl02_payeeNameImageButton")
	WebElement payeeVendorLookupFLW3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl00_robSearchImage")
	WebElement payeeROBLookupFLW1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl01_robSearchImage")
	WebElement payeeROBLookupFLW2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl02_robSearchImage")
	WebElement payeeROBLookupFLW3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl00_payeeNameTextBox")
	WebElement payeenameFLW1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl01_payeeNameTextBox")
	WebElement payeenameFLW2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PayeeInfoControl_payeeRepeater_ctl02_payeeNameTextBox")
	WebElement payeenameFLW3;
	private void enterPayeeInformationFLW(HashMap<String, String> hm) {
		
		Wait.waitforelement(BaseClass.getDriver(), payeeTypeFLW1);
		Wait.waitFor(2);
		for(int i=0;i< hm.get("PayeeType").split("\\|").length;i++)
		{
			paymentPage=BaseClass.getcurrentwindow();
			if(i>0)
			{
				BaseClass.click(addPayeeButtonFLW);
				BaseClass.logInfo("Add Payee Button is clicked", "");
			}
			System.out.println("Selecting Payee Type as: "+hm.get("PayeeType").split("\\|")[i]);
			if(i==0)
			{
				BaseClass.selectByVisibleText(payeeTypeFLW1, hm.get("PayeeType").split("\\|")[0]);
				BaseClass.logInfo("Payee Type 1 is selected as", BaseClass.getfirstselectedoption(payeeTypeFLW1));
				if(hm.get("PayeeType").split("\\|")[0].equals("Vendor"))
				{
					
					BaseClass.click(payeeVendorLookupFLW1);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[0].equals("ROB"))
				{
					
					BaseClass.click(payeeROBLookupFLW1);
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeenameFLW1);
			}
			if(i==1)
			{
				BaseClass.selectByVisibleText(payeeTypeFLW2, hm.get("PayeeType").split("\\|")[1]);
				BaseClass.logInfo("Payee Type 2 is selected as", BaseClass.getfirstselectedoption(payeeTypeFLW2));
				if(hm.get("PayeeType").split("\\|")[1].equals("Vendor"))
				{
					
					BaseClass.click(payeeVendorLookupFLW2);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[1].equals("ROB"))
				{
					
					BaseClass.click(payeeROBLookupFLW2);	
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeenameFLW2);
			}
			if(i==2)
			{
				BaseClass.selectByVisibleText(payeeTypeFLW3, hm.get("PayeeType").split("\\|")[2]);
				BaseClass.logInfo("Payee Type 3 is selected as", BaseClass.getfirstselectedoption(payeeTypeFLW3));
				if(hm.get("PayeeType").split("\\|")[2].equals("Vendor"))
				{
					
					BaseClass.click(payeeVendorLookupFLW3);
					BaseClass.logInfo("Vendor look up is clicked", "");
					selectVendor(hm);
				}
				if(hm.get("PayeeType").split("\\|")[2].equals("ROB"))
				{
					
					BaseClass.click(payeeROBLookupFLW3);	
					BaseClass.logInfo("ROB look up is clicked", "");
					selectROB(hm);
				}
				Wait.waitForValuePresent(BaseClass.getDriver(), payeenameFLW3);
			}
		}
		
			
	}
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_invoiceDateTextBox")
	WebElement billSubmittedDateFLW;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_invoiceRcvdDateTextBox")
	WebElement billReceivedDateFLW;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_invoiceNumberTextBox")
	WebElement invoiceNumberFLW;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_invoiceAmtTextbox")
	WebElement invoiceAmountFLW;
	private void enterbilldetailFLW() {
		BaseClass.settext(billSubmittedDateFLW, BaseClass.pastdatefromTodayDate(10));
		BaseClass.logInfo("Bill Submitted Date is set as:", BaseClass.getattribute(billSubmittedDateFLW, "value"));
		BaseClass.settext(billReceivedDateFLW, BaseClass.pastdatefromTodayDate(8));
		BaseClass.logInfo("Bill Received Date is set as:", BaseClass.getattribute(billReceivedDateFLW, "value"));
		BaseClass.settext(invoiceNumberFLW, BaseClass.enterRandomNumber(5));
		BaseClass.logInfo("Invoice Number is set as:", BaseClass.getattribute(invoiceNumberFLW, "value"));
		BaseClass.clear(invoiceAmountFLW);
		BaseClass.settext(invoiceAmountFLW, BaseClass.randInt(1000, 9999));
		BaseClass.logInfo("Invoice Amount is set as:", BaseClass.getattribute(invoiceAmountFLW, "value"));
	}
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_modeTypeDropDown")
	WebElement modeTypeFLW;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_bankModeDropDown")
	WebElement paymentModeFLW;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_checkDateTextBox")
	WebElement paymentFLWCheckDate;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_checkNumberTextBox")
	WebElement paymentFLWCheckNumber;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_bankAccountTextBox")
	WebElement paymentFLWBankAccount;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_releaseReasonTextBox")
	WebElement paymentFLWReason;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_fromDateTextBox")
	WebElement paymentFLWFromDate;
	
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentOneTimeControl_thruDateTextBox")
	WebElement paymentFLWThroughDate;
	
	private void entercheckinformationFLW(HashMap<String, String> hm) {
		BaseClass.selectByVisibleText(modeTypeFLW, hm.get("ModeType"));
		BaseClass.logInfo("Mode Type is selected as:", BaseClass.getfirstselectedoption(modeTypeFLW));
		BaseClass.selectByIndex(paymentModeFLW);
		BaseClass.logInfo("Payment Mode is selected as:", BaseClass.getfirstselectedoption(paymentModeFLW));
		Wait.waitForValuePresent(BaseClass.getDriver(), paymentFLWBankAccount);
		BaseClass.settext(paymentFLWFromDate,CycleFromDate);
		BaseClass.logInfo("Payment From Date is set as:", BaseClass.getattribute(paymentFLWFromDate, "value"));
		BaseClass.settext(paymentFLWThroughDate,CycleThroughDate);
		BaseClass.logInfo("Payment Through Date is set as:", BaseClass.getattribute(paymentFLWThroughDate, "value"));
	    if(hm.get("ModeType").equals("Journal Voucher"))
	    {
	    	Wait.waitforelement(BaseClass.getDriver(),paymentFLWCheckDate);
	    	BaseClass.settext(paymentFLWCheckDate,BaseClass.getCurrentDate());
			BaseClass.logInfo("Check Date is set as:", BaseClass.getattribute(paymentFLWCheckDate, "value"));
	    	BaseClass.settext(paymentFLWCheckNumber,BaseClass.enterRandomNumber(6));
			BaseClass.logInfo("Check Number is set as:", BaseClass.getattribute(paymentFLWCheckNumber, "value"));
	    }
    	BaseClass.settext(paymentFLWReason,BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Payment Reason is set as:", BaseClass.getattribute(paymentFLWReason, "value"));	       
	}
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_moreButton1")
	WebElement addlineitemsFLW;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_totalTextBox")
	WebElement paymentTotalFLW;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl01_payCategoryDown")
	WebElement paymentCategoryFLW1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl02_payCategoryDown")
	WebElement paymentCategoryFLW2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl03_payCategoryDown")
	WebElement paymentCategoryFLW3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl01_paySubCategoryDown")
	WebElement paymentSubCategoryFLW1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl02_paySubCategoryDown")
	WebElement paymentSubCategoryFLW2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl03_paySubCategoryDown")
	WebElement paymentSubCategoryFLW3;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl01_amountTextBox")
	WebElement paymentAmountFLW1;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl02_amountTextBox")
	WebElement paymentAmountFLW2;
	@FindBy(id="Payments_ClaimPaymentRecurring_RecurringPaymentFLWInfoItem_PaymentDetailForCompControl_lineItemRepeater_ctl03_amountTextBox")
	WebElement paymentAmountFLW3;
	
	private void enterPaymentDetailFLW(HashMap<String, String> hm) {
		String[] PaymentCategory=hm.get("PaymentCategory").split("\\|");
		String[] PaymentSubCategory=hm.get("PaymentSubCategory").split("\\|");
		String total=BaseClass.getattribute(paymentTotalFLW, "value");
			for(int i=1;i<=Integer.parseInt(hm.get("LineItems"));i++)
			{
			if(i>1)
			{ 
				Wait.waitFor(5);
			BaseClass.click(addlineitemsFLW);
			}
			if(i==1)
			{
				BaseClass.selectByVisibleText(paymentCategoryFLW1,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentCategoryFLW1));
				Wait.waitFor(3);
				BaseClass.selectByVisibleText(paymentSubCategoryFLW1,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 1 is selected as:", BaseClass.getfirstselectedoption(paymentSubCategoryFLW1));
				Wait.waitFor(3);
				BaseClass.settext(paymentAmountFLW1, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 1 is set as:", BaseClass.getattribute(paymentAmountFLW1, "value"));
				Wait.waitForNotContainValue(paymentTotalFLW,total);
			}
			if(i==2)
			{
				
				Wait.waitforelement(BaseClass.getDriver(), paymentCategoryFLW2);
				BaseClass.selectByVisibleText(paymentCategoryFLW2,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentCategoryFLW2));
				Wait.waitFor(3);
				BaseClass.selectByVisibleText(paymentSubCategoryFLW2,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 2 is selected as:", BaseClass.getfirstselectedoption(paymentSubCategoryFLW2));
				Wait.waitFor(3);
				BaseClass.settext(paymentAmountFLW2, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 2 is set as:", BaseClass.getattribute(paymentAmountFLW2, "value"));
				Wait.waitForNotContainValue(paymentTotalFLW,total);
			}
			if(i==3)
			{
				Wait.waitforelement(BaseClass.getDriver(), paymentCategoryFLW3);
				BaseClass.selectByVisibleText(paymentCategoryFLW3,PaymentCategory[i-1]);
				BaseClass.logInfo("Payment Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentCategoryFLW3));
				Wait.waitFor(3);
				BaseClass.selectByVisibleText(paymentSubCategoryFLW3,PaymentSubCategory[i-1]);
				BaseClass.logInfo("Payment Sub Category for Line Item 3 is selected as:", BaseClass.getfirstselectedoption(paymentSubCategoryFLW3));
				Wait.waitFor(3);
				BaseClass.settext(paymentAmountFLW3, BaseClass.randDouble(10,99)+Keys.TAB);
				BaseClass.logInfo("Payment Amount for Line Item 3 is set as:", BaseClass.getattribute(paymentAmountFLW3, "value"));
				Wait.waitForNotContainValue(paymentTotalFLW,total);
			}
			System.out.println(BaseClass.getattribute(paymentTotalFLW, "value"));
			total=BaseClass.getattribute(paymentTotalFLW, "value");
		}
			Wait.waitFor(5);
	}
}
