package ClaimVisionPages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.internal.BaseClassFinder;

import com.aventstack.extentreports.Status;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class ClaimantIncidentPage {
	
	private ClaimantIncidentPage(){}
	private static final ClaimantIncidentPage claimantIncident=new ClaimantIncidentPage();

	public static ClaimantIncidentPage getinstance()
	{
		return claimantIncident;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="ClaimantAccident_claimDeniedRadioList_1")
	WebElement claimDeniedNo;
	
	@FindBy(id="ClaimantAccident_closedReasonDropDown")
	WebElement claimClosedReason;
	
	@FindBy(id="ClaimantAccident_claimDeniedRadioList_0")
	WebElement claimDeniedYes; 
	
	@FindBy(id="ClaimantAccident_isClaimQuestionableCheckBox")
	WebElement claimQuestionableCheckbox; 
	
	@FindBy(id="ClaimantAccident_saveButton")
	WebElement claimantSave; 
	
	@FindBy(id="btnClose")
	WebElement closedBtnForOpenDiaries;
	
	@FindBy(id="ClaimantAccident_saveMessageLabel")
	WebElement messageLabel; 
	
	@FindBy(id="ClaimantAccident_majorPartDropDown")
	WebElement claimantIncidentPartofBodyMajor; 
	
	@FindBy(id="ClaimantAccident_minorPartDropDown")
	WebElement claimantIncidentPartofBodyMinor; 
	
	@FindBy(id="ClaimantAccident_OccurrenceRadioList_1")
	WebElement claimantIncidentOccuranceNo;
	
	
	@FindBy(css = "input#ClaimantAccident_dateDeniedTextBox")
	WebElement deniedDateComp;
	
	@FindBy(css = "select#ClaimantAccident_DenialTypeDropDownList")
	WebElement denialTypeComp;
	
	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList")
	WebElement deniedReasonComp1;
	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList2")
	WebElement deniedReasonComp2;
	
	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList3")
	WebElement deniedReasonComp3;
	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList4")
	WebElement deniedReasonComp4;
	@FindBy(css = "select#ClaimantAccident_denialReasonDropDownList5")
	WebElement deniedReasonComp5;
	
	@FindBy(css = "input#ClaimantAccident_denialRescindedCheckBox")
	WebElement denialRescindedComp;
	
	@FindBy(css = "input#ClaimantAccident_denialRescissionDateTextBox")
	WebElement denialRescissionDateComp ;
	
	@FindBy(css = "select#ClaimantAccident_PartialDenialDropDownList")
	WebElement partialDenial;

	
	@FindBy(css = "input#ClaimantAccident_claimSuspendedCheckBox")
	WebElement claimSuspended;

	@FindBy(css = "select#ClaimantAccident_suspensionLevelDropDown")
	WebElement suspensionLevel;

	@FindBy(css = "select#ClaimantAccident_suspensionTypeDropDown")
	WebElement suspensionType;

	@FindBy(css = "input#ClaimantAccident_suspensionEffectiveDateTextBox")
	WebElement suspensionEffDate;

	@FindBy(css = "input#ClaimantAccident_suspensionNarrativeTextBox")
	WebElement suspensionNarrative;

	@FindBy(css = "input#ClaimantAccident_suspensionRescindedCheckBox")
	WebElement suspensionRescinded;

	@FindBy(css = "input#ClaimantAccident_RescissionDateTextBox")
	WebElement rescissionDate;
	
	@FindBy(css = "input#ClaimantAccident_accidentDescriptionTextBox")
	WebElement incidentDescription;
	
	@FindBy(css="input#Claim_saveButton")
	WebElement saveBtn;
	
	@FindBy(css="input#saveClaimImageButton")
	WebElement saveIcon;
	
	@FindBy(css="span#ClaimantAccident_incidentDescDisplayCharCount_countLabel > span")
	WebElement incidentDescCountLabel;
	
	@FindBy(css = "textarea#ClaimantAccident_claimSummaryTextBox")
	WebElement claimSummary;
	
	String claimantAccidentPage,DiaryPage;
	
	@FindBy(css="span#ClaimantAccident_summaryDisplayCharCount_countLabel > span")
	WebElement summaryDisplayCountLabel;
	
	@FindBy(css = "input#ClaimantAccident_emailTextBox")
	WebElement emailAddress;
	
	@FindBy(css = "input#ClaimantAccident_claimantHomePhoneTextBox")
	WebElement homePhone;
	@FindBy(css = "input#ClaimantAccident_cellPhoneTextBox")
	WebElement cellPhone;
	@FindBy(css = "input#ClaimantAccident_birthTextBox")
	WebElement dob;
	@FindBy(css = "input#ClaimantAccident_injuryDateTextBox")
	WebElement dateOfIncident;
	@FindBy(css = "input#ClaimantAccident_reportDateToERTextBox")
	WebElement dateOfKnowledge;
	@FindBy(css = "input#ClaimantAccident_fatalityDateTextBox")
	WebElement deathDate;
	@FindBy(css = "input#ClaimantAccident_initiallyDateTextBox")
	WebElement dateInitiallyReportedToXS;
	@FindBy(css = "input#ClaimantAccident_lastDateTextBox")
	WebElement dateLastReportedToXS;
	@FindBy(css = "input#ClaimantAccident_firstExposureTextBox")
	WebElement firstExposureDate;
	@FindBy(css = "input#ClaimantAccident_CompLastExposureTextBox")
	WebElement lastExposureDate;
	
	@FindBy(css="div#ClaimantAccident_SaveValidationSummary > ul > li")
	List<WebElement> listOfErrorMessages;
	
	@FindBy(css = "input#ClaimantAccident_cancelButton")
	WebElement cancel;
	
	@FindBy(css = "input#openClaimImageButton")
	WebElement openClaimIcon;
	
	@FindBy(css = "input#closeClaimImageButton")
	WebElement closeClaimIcon;
	
	@FindBy(css = "div#ClaimantAccident_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	WebElement claimStatusLabel;
	
	@FindBy(css = "select#ClaimantAccident_statusDropDown")
	WebElement statusDropDown;

	
	String suspendedRescissionDate, denialRescindedDate;
	
	
	
	
	public void updateClaimWithPermission(String Scenario)
	{
		System.out.println("test");
		settempvalue();
		if(Scenario.equals("Questionable"))
		{
			BaseClass.click(claimDeniedNo);
			if(!claimQuestionableCheckbox.isSelected())
			{
				BaseClass.click(claimQuestionableCheckbox);
			}
		}
		else
		{
		if(Scenario.equals("Denied"))
		{
			BaseClass.click(claimDeniedYes);
			if(claimQuestionableCheckbox.isSelected())
			{
				BaseClass.click(claimQuestionableCheckbox);
			}
		}
		else
		{
			BaseClass.click(claimDeniedNo);
			if(claimQuestionableCheckbox.isSelected())
			{
				BaseClass.click(claimQuestionableCheckbox);
			}
		}
		}
		BaseClass.click(claimantSave);
		Wait.waitFor(5);
		BaseClass.Scrolltoelement(messageLabel);
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
	}
	
	private void settempvalue() {
		BaseClass.selectByVisibleText(claimantIncidentPartofBodyMajor,"Head");
		BaseClass.selectByIndex(claimantIncidentPartofBodyMinor);
		BaseClass.click(claimantIncidentOccuranceNo);
	}
	
	public void claimQuestionable(String Scenario){
		//GroupSetupPage.getinstance().setGroupPermissionForQuestionable(Scenario);
		if(Scenario.equals("QuestionableWithPermission") ){
			BaseClass.Scrolltoelement(claimQuestionableCheckbox);
			Assert.assertEquals(claimQuestionableCheckbox.isDisplayed(), true);
			BaseClass.logInfo("Questionable checkbox is displayed when user have permission", "");
			if(claimQuestionableCheckbox.isSelected()){
				BaseClass.click(claimQuestionableCheckbox);
				BaseClass.logInfo("Questionable checkbox is de-selected", "");
				Assert.assertFalse(claimQuestionableCheckbox.isSelected());
			}
			else{
				BaseClass.click(claimQuestionableCheckbox);
				BaseClass.logInfo("Questionable checkbox is selected", "");
				Assert.assertTrue(claimQuestionableCheckbox.isSelected());
			}
			BaseClass.click(claimantSave);
			BaseClass.logInfo("Save Button is Clicked Successfully", "");
			Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
			BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		}
		else if(Scenario.equals("QuestionableWithoutPermission") ){
			try{
				boolean questionableCheckboxStatus = claimQuestionableCheckbox.isDisplayed();
				if(questionableCheckboxStatus)
					BaseClass.logInfo("Questionable checkbox is displaying when user doesn't have permission", "");
					BaseClass.logfail("Expected is Questionable checkbox should not display", "Actual is Questionable checkbox is displaying");
				}
			catch(NoSuchElementException e){
				BaseClass.logInfo("Questionable checkbox is not displaying when user doesn't have permission", "");
				BaseClass.logpass("Expected is Questionable checkbox should not display", "Actual is Questionable checkbox is not displaying");
			}
		}
	}
	
	
	public void claimDenied(HashMap<String, String> hm){
		Wait.waitforelement(BaseClass.driver, claimDeniedYes);
			if(!hm.get("Scenario").equals("NoDenied"))
			{
				
				if(!claimDeniedYes.isSelected())
					{
					BaseClass.click(claimDeniedYes);
					BaseClass.logInfo("Claim denied radio button 'Yes' is selected", "");
					Wait.waitTillPresent(BaseClass.getDriver(), deniedDateComp);
					}
				Assert.assertEquals(true, claimDeniedYes.isSelected());
				Assert.assertTrue(deniedDateComp.isDisplayed());
				Assert.assertTrue(denialRescindedComp.isDisplayed());
				Assert.assertTrue(denialRescissionDateComp.isDisplayed());
				Assert.assertTrue(denialTypeComp.isDisplayed());
				BaseClass.settext(deniedDateComp, BaseClass.getCurrentDate());
				BaseClass.logInfo("Denied date entered as ", BaseClass.getattribute(deniedDateComp, "value"));
				if(hm.get("Scenario").equals("FullDenied")){
					BaseClass.selectByVisibleText(denialTypeComp, "Full");
					BaseClass.logInfo("Denial type 'Full' is selected", "");
					Wait.waitTillPresent(BaseClass.getDriver(), deniedReasonComp1);
					Assert.assertEquals(BaseClass.getfirstselectedoption(denialTypeComp),"Full");
					Assert.assertTrue(deniedReasonComp1.isDisplayed());
					Assert.assertTrue(deniedReasonComp2.isDisplayed());
					Assert.assertTrue(deniedReasonComp3.isDisplayed());
					Assert.assertTrue(deniedReasonComp4.isDisplayed());
					Assert.assertTrue(deniedReasonComp5.isDisplayed());
					BaseClass.selectByIndex(deniedReasonComp1, BaseClass.generateRandomNumberAsInteger(4));
					BaseClass.logInfo("Denied reason is selected as ", BaseClass.getfirstselectedoption(deniedReasonComp1));
					BaseClass.selectByIndex(deniedReasonComp2, BaseClass.generateRandomNumberAsInteger(4));
					BaseClass.logInfo("Denied reason is selected as ", BaseClass.getfirstselectedoption(deniedReasonComp2));
					BaseClass.selectByIndex(deniedReasonComp3, BaseClass.generateRandomNumberAsInteger(4));
					BaseClass.logInfo("Denied reason is selected as ", BaseClass.getfirstselectedoption(deniedReasonComp3));
					BaseClass.selectByIndex(deniedReasonComp4, BaseClass.generateRandomNumberAsInteger(4));
					BaseClass.logInfo("Denied reason is selected as ", BaseClass.getfirstselectedoption(deniedReasonComp4));
					BaseClass.selectByIndex(deniedReasonComp5, BaseClass.generateRandomNumberAsInteger(4));
					BaseClass.logInfo("Denied reason is selected as ", BaseClass.getfirstselectedoption(deniedReasonComp5));
					
				}
				else if(hm.get("Scenario").equals("PartialDenied")){
					BaseClass.selectByVisibleText(denialTypeComp, "Partial");
					BaseClass.logInfo("Denial type 'Partial' is selected", "");
					Wait.waitTillPresent(BaseClass.getDriver(), partialDenial);
					Assert.assertEquals(BaseClass.getfirstselectedoption(denialTypeComp),"Partial");
					BaseClass.selectByIndex(partialDenial, BaseClass.generateRandomNumberAsInteger(4));
					BaseClass.logInfo("Partial Denied reason is selected as ", BaseClass.getfirstselectedoption(partialDenial));
					
				}
				else if(hm.get("Scenario").equals("DenialRescinded")){
					if(!denialRescindedComp.isSelected())
					{
					BaseClass.click(denialRescindedComp);
					BaseClass.logInfo("Claim denied rescinded checkbox is selected", "");
					Assert.assertTrue(denialRescindedComp.isSelected());
					denialRescindedDate= BaseClass.futuredatefromdate(BaseClass.getattribute(dateOfIncident, "value"), 5);
					BaseClass.settext(denialRescissionDateComp,denialRescindedDate );
					BaseClass.logInfo("Denial Rescinded Date set as : ", denialRescindedDate);
					}
					else{
						BaseClass.click(denialRescindedComp);
						BaseClass.logInfo("Claim denied rescinded checkbox is de-selected", "");
						Assert.assertFalse(denialRescindedComp.isSelected());
						Assert.assertFalse(BaseClass.verifyEnableElement(denialRescissionDateComp));
						BaseClass.logInfo("Denial Rescinded Date field became disbaled","");
					}
					
				}//Denial rescinded if-else finished here
				
			}//Denied as Yes finished here
			
		
		else{
			BaseClass.click(claimDeniedNo);
			BaseClass.logInfo("Claim denied radio button 'No' is selected", "");
			Assert.assertEquals(true, claimDeniedNo.isSelected());
			
		}// all if-else conditions are finished here
		
		
		BaseClass.click(claimantSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		
	}
		
	public void claimSuspended(String Scenario){
		Wait.waitforelement(BaseClass.driver, claimSuspended);
		if(Scenario.equals("suspensionRescinded"))
		{
			if(!claimSuspended.isSelected())
			{
				BaseClass.click(claimSuspended);
				BaseClass.logInfo("Claim is Suspended","");
			}
				if (suspensionRescinded.isSelected())
				{
				BaseClass.logInfo("Claim is Rescinded","");
				BaseClass.click(suspensionRescinded);
				BaseClass.logInfo("Clicked on suspension rescinded checkbox to de-select the rescinded checkbox","");
				Assert.assertFalse(suspensionRescinded.isSelected());
				Assert.assertEquals(BaseClass.getattribute(rescissionDate,"disabled"), "true");
				BaseClass.logInfo("Rescission Date field became disbaled","");
				}
				else {
					BaseClass.click(suspensionRescinded);
					BaseClass.logInfo("Clicked on suspension rescinded checkbox","");
					Assert.assertTrue(suspensionRescinded.isSelected());
					suspendedRescissionDate= BaseClass.futuredatefromdate(BaseClass.getattribute(dateOfIncident,"value"), 5);
					BaseClass.settext(rescissionDate,suspendedRescissionDate );
					BaseClass.logInfo("Rescission Date set as : ", suspendedRescissionDate);
					
				}
			
		}
		else if(Scenario.equals("claimSuspended"))
		{
			if(claimSuspended.isSelected())
			{
				BaseClass.click(claimSuspended);
				BaseClass.logInfo("Claim is not Suspended","");
			}
			BaseClass.click(claimSuspended);
			Wait.waitTillPresent(BaseClass.getDriver(), suspensionType);
			Assert.assertTrue(suspensionType.isDisplayed());
			Assert.assertTrue(suspensionLevel.isDisplayed());
			Assert.assertTrue(suspensionEffDate.isDisplayed());
			Assert.assertTrue(suspensionNarrative.isDisplayed());
			Assert.assertTrue(suspensionRescinded.isDisplayed());
			Assert.assertTrue(rescissionDate.isDisplayed());
			BaseClass.selectByIndex(suspensionLevel);
			BaseClass.logInfo("Suspension level is selected as :",BaseClass.getfirstselectedoption(suspensionLevel));
			BaseClass.selectByIndex(suspensionType);
			BaseClass.logInfo("Suspension Type is selected as :",BaseClass.getfirstselectedoption(suspensionType));
			BaseClass.settext(suspensionEffDate, BaseClass.getattribute(dateOfIncident, "value"));
			BaseClass.logInfo("Entered Suspension effective date as:",BaseClass.getattribute(suspensionEffDate,"value"));
			BaseClass.settext(suspensionNarrative, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo("Entered suspension narrative as ", BaseClass.getattribute(suspensionNarrative,"value"));
			
		}
		BaseClass.click(claimantSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
	}
	
	/*public void reOpenedStatusOfClaim(HashMap<String, String> hm){
		BaseClass.click(openClaimIcon);
		BaseClass.logInfo("Clicked on Open claim icon", "");
		BaseClass.acceptalert();
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		BaseClass.logInfo("Alert accepted for opening the claim", "");
		Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains( "ReOpened"));
		BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
	
	}*/
	
	public void openStatusOfClaim(HashMap<String, String> hm){
		if(hm.get("StatusChange").contains("Icon")){
			BaseClass.click(openClaimIcon);
			BaseClass.logInfo("Clicked on Open claim icon", "");
			BaseClass.acceptalert();
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			BaseClass.logInfo("Alert accepted for opening the claim", "");
			
		}
		else if (hm.get("StatusChange").contains("Drop-down")){
			BaseClass.selectByVisibleText(statusDropDown, "Open");
			BaseClass.logInfo("Claim status as Open selected from drop-down", "");
			BaseClass.click(claimantSave);
			BaseClass.logInfo("Save Button is Clicked", "");
			Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
			BaseClass.logInfo("Success Message should be displayed", "Success Message is displayed");
			}
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains( "Open"));
		BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
	
	}
	
	public void reClosedStatusOfClaim(HashMap<String, String> hm){
		DashboardPage.getinstance().navigateToFinancialSummary();
		Wait.waitForTitle(BaseClass.getDriver(), "Financial Summary");
		BaseClass.logInfo("Navigate to Financial Summary Screen", "");
		// or can be navigate to financial summary screen
		// if pending have value - then pending error message will display
		// if paid & reserves have value then outstanding reserves error message will display
		
		HashMap<String, String> financialSummaryTotal = FinancialSummaryPage.getinstance().getTotalAmountList();
		DashboardPage.getinstance().navigateToClaimantIncident();
		Wait.waitForTitle(BaseClass.getDriver(), "Claimant/Accident");
		BaseClass.logInfo("Navigate to Claimant/Accident Screen again", "");
		claimantAccidentPage=BaseClass.getcurrentwindow();
		BaseClass.selectByIndex(claimClosedReason, BaseClass.generateRandomNumberAsInteger(4));
		BaseClass.logInfo("Closed reason selected as: ", BaseClass.getfirstselectedoption(claimClosedReason));
		System.out.println("hm: "+Arrays.asList(hm));
		if(hm.get("StatusChange").contains("Icon")){
			
			BaseClass.click(closeClaimIcon);
			BaseClass.logInfo("Clicked on close claim icon", "");
			
			
		}
		else if (hm.get("StatusChange").contains("Drop-down")){
			BaseClass.selectByIndex(statusDropDown,2);
			BaseClass.logInfo("Claim status is selected from drop-down as: ", BaseClass.getfirstselectedoption(statusDropDown));
			BaseClass.click(claimantSave);
			BaseClass.logInfo("Save Button is Clicked", "");
							
		}
		BaseClass.acceptalert();
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		BaseClass.logInfo("Alert for closing the claim is accepted", "");
		
		
		if((!(financialSummaryTotal.get("Paid").contains("$0.00"))
				|| (!financialSummaryTotal.get("OS Res.").contains("$0.00"))))
		{
			//outstanding reserves error message will display
			//need to put assert condition
			BaseClass.logInfo("Getting outstanding reserve error message while closing the claim", "");
			BaseClass.logInfo("Claim having OS Reserve amount: ", financialSummaryTotal.get("OS Res."));
			BaseClass.logInfo("Claim having Paid amount: ", financialSummaryTotal.get("Paid"));
		}
		
		else if ((!financialSummaryTotal.get("Pending").contains("$0.00"))
				|| hm.get("PaymentStatus").equals("7")
				||((!financialSummaryTotal.get("Paid").contains("$0.00"))
						&&(financialSummaryTotal.get("Pending").contains("$0.00")) 
								&& (financialSummaryTotal.get("OS Res.").contains("$0.00")))){
			//pending error message will display
			//need to put assert condition
			System.out.println("Entered in Pending condition verification");
			BaseClass.logInfo("Getting pending payment error message while closing the claim", "");
			BaseClass.logInfo("Claim having Pending amount: ", financialSummaryTotal.get("Pending"));
		}
		else {
			//open diaries pop-up handling
			if(BaseClass.getDriver().getWindowHandles().size()>1)
			{
				DiaryPage=BaseClass.switchToNextWindow(claimantAccidentPage,"Open Diaries", 2);
				BaseClass.logInfo("Diary Page is displayed", "");
				BaseClass.click(closedBtnForOpenDiaries);
				BaseClass.logInfo("Diaries pop-up is closed successfully", "");
				BaseClass.switchToWindow(claimantAccidentPage,1);
			}
			
			Assert.assertEquals(BaseClass.gettext(messageLabel), "The claim was closed successfully.");
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			System.out.println(BaseClass.gettext(claimStatusLabel));
			Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains("Closed")) ;
			BaseClass.logpass("Successfully closed the claim", "Successfully closed the claim");
		}
	
	}
	
	//public void closedStatusOfClaim(HashMap<String, String> hm){}
	
	
	/*public void claimStatusVerification(HashMap<String, String> hm)
	{
		if(hm.get("Scenario").contains("ReOpened")){
			if(BaseClass.gettext(claimStatusLabel).contains("Closed") ||
					BaseClass.gettext(claimStatusLabel).contains("ReClosed"))
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			}
				
		}
		else if(hm.get("Scenario").contains("ReClosed") ){
			if(BaseClass.gettext(claimStatusLabel).contains("ReOpened")){
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Closed")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
				
		}
		
		else if(hm.get("Scenario").contains("Closed")){
			if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				
			}
			
		}
		else if(hm.get("Scenario").contains("Open")){
			openStatusOfClaim(hm);
			}
	}
	*/	
	

	public void claimStatusVerification(HashMap<String, String> hm)
	{
		if(hm.get("Scenario").contains("ReOpened")){
			if(BaseClass.gettext(claimStatusLabel).contains("Closed") ||
					BaseClass.gettext(claimStatusLabel).contains("ReClosed"))
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
				
		}
		else if(hm.get("Scenario").contains("ReClosed") ){
			if(BaseClass.gettext(claimStatusLabel).contains("ReOpened")){
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Closed")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
				
		}
		
		else if(hm.get("Scenario").contains("Closed")){
			if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				
			}
			
		}
		else if(hm.get("Scenario").contains("Open")){
			openStatusOfClaim(hm);
			}
	}
	
	public void limitVerification(String Scenario) 
	{
		if(Scenario.equals("IncidentDescriptionLimit"))
		{
			BaseClass.settext(incidentDescription, BaseClass.stringGeneratorl(75));
			BaseClass.logInfo("Entered the incident desciption : ", BaseClass.gettext(incidentDescription));
			String countLabelValue =BaseClass.gettext(incidentDescCountLabel); 
			BaseClass.logInfo("Incident description length : ", BaseClass.gettext(incidentDescCountLabel));
			Assert.assertTrue(countLabelValue.contains("75/75"));
			BaseClass.logInfo("Not able to enter incident description more than it's length", "");
		}
		else if (Scenario.equals("ClaimSummaryLimit"))
		{
			BaseClass.settext(claimSummary, BaseClass.stringGeneratorl(1500));
			BaseClass.logInfo("Entered the claim summary : ", BaseClass.gettext(claimSummary));
			String countLabelValue =BaseClass.gettext(summaryDisplayCountLabel);
			BaseClass.logInfo("Claim summary length is : ", countLabelValue);
			Assert.assertTrue(countLabelValue.contains("1500/1500"));
			BaseClass.logInfo("Not able to enter Claim summary more than it's length", "");
		}
		BaseClass.click(claimantSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
	}
	
	public void invalidDataValidation(){
		BaseClass.settext(emailAddress, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered email address: ",BaseClass.getattribute(emailAddress, "value"));
		BaseClass.settext(homePhone, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered homePhone: ",BaseClass.getattribute(homePhone, "value"));
		BaseClass.settext(cellPhone, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered cellPhone: ",BaseClass.getattribute(cellPhone, "value"));
		BaseClass.settext(dob, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered dob: ",BaseClass.getattribute(dob, "value"));
		BaseClass.settext(dateOfIncident, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered dateOfIncident: ",BaseClass.getattribute(dateOfIncident, "value"));
		BaseClass.settext(dateOfKnowledge, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered dateOfKnowledge: ",BaseClass.getattribute(dateOfKnowledge, "value"));
		BaseClass.settext(deathDate, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered deathDate: ",BaseClass.getattribute(deathDate, "value"));
		BaseClass.settext(dateInitiallyReportedToXS, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered date Initially Reported To XS: ",BaseClass.getattribute(dateInitiallyReportedToXS, "value"));
		BaseClass.settext(dateLastReportedToXS, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered date Last Reported To XS: ",BaseClass.getattribute(dateLastReportedToXS, "value"));
		BaseClass.settext(firstExposureDate, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered first Exposure Date: ",BaseClass.getattribute(firstExposureDate, "value"));
		BaseClass.settext(lastExposureDate, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered last Exposure Date: ",BaseClass.getattribute(lastExposureDate, "value"));
		BaseClass.settext(deathDate, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered death Date: ",BaseClass.getattribute(deathDate, "value"));
		BaseClass.click(claimantSave);
		BaseClass.logInfo("Clicked on save button", "");
		Wait.waitforelements(BaseClass.getDriver(), listOfErrorMessages);
		List<String> temp = new ArrayList<String>();
		for(int i=0;i<listOfErrorMessages.size();i++){
		temp.add(i, listOfErrorMessages.get(i).getText());
		}
		System.out.println("List is: " + temp);
		BaseClass.logInfo("Error message displaed on claimant/incident screen after entering invalid data: ", "");
		Assert.assertTrue(temp.get(0).contains("Email Address is invalid"));
		BaseClass.logInfo("Invalid email address entered message displayed as : ",temp.get(0));
		Assert.assertTrue(temp.get(1).contains("Home Phone number has to be numeric"));
		BaseClass.logInfo("Invalid Home Phone number entered message displayed as : ",temp.get(1));
		Assert.assertTrue(temp.get(2).contains("Cell Phone number has to be numeric"));
		BaseClass.logInfo("Invalid Cell Phone number entered message displayed as : ",temp.get(2));
		Assert.assertTrue(temp.get(3).contains("Date Of Birth is invalid"));
		BaseClass.logInfo("Invalid Date Of Birth entered message displayed as : ",temp.get(3));
		Assert.assertTrue(temp.get(4).contains("Date And Time Of Incident is invalid"));
		BaseClass.logInfo("Invalid Date And Time Of Incident entered message displayed as : ",temp.get(4));
		Assert.assertTrue(temp.get(5).contains("Date of Knowledge is invalid"));
		BaseClass.logInfo("Invalid Date of Knowledge entered message displayed as : ",temp.get(5));
		Assert.assertTrue(temp.get(6).contains("Fatality Date is invalid"));
		BaseClass.logInfo("Invalid Fatality Date entered message displayed as : ",temp.get(6));
		Assert.assertTrue(temp.get(7).contains("Date Initially Reported To XS carrier is invalid"));
		BaseClass.logInfo("Invalid Date Initially Reported To XS carrierr entered message displayed as : ",temp.get(7));
		Assert.assertTrue(temp.get(8).contains("Date Last Reported To XS carrier is invalid"));
		BaseClass.logInfo("Invalid Date Last Reported To XS carrier entered message displayed as : ",temp.get(8));
		Assert.assertTrue(temp.get(9).contains("First Exposure Date is invalid"));
		BaseClass.logInfo("Invalid First Exposure Date entered message displayed as : ",temp.get(9));
		Assert.assertTrue(temp.get(10).contains("Last Exposure Date is invalid"));
		BaseClass.logInfo("Invalid Last Exposure Date entered message displayed as : ",temp.get(10));
		BaseClass.logpass("Validation message displayed for invalid data in claimant/incident screen", "");
	}
	
	
	
	public void cancelButtonVerify() 
	{
		String incidentDes = null;
		incidentDes =BaseClass.getattribute(incidentDescription, "value"); 
		BaseClass.logInfo("Incident description before entering new data: ", incidentDes);
		BaseClass.settext(incidentDescription, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered the incident desciption : ", BaseClass.getattribute(incidentDescription, "value"));
		//BaseClass.logInfo("Incident description length after entering data: ", BaseClass.getattribute(incidentDescCountLabel, "value"));
		BaseClass.click(cancel);
		BaseClass.logInfo("Clicked on cancel button", "");
		Wait.waitforelement(BaseClass.getDriver(), incidentDescription);
		Wait.waitForContainValue(incidentDescription, incidentDes);
		BaseClass.logInfo("Incident description after clicked on cancel button: ", BaseClass.getattribute(incidentDescription, "value"));
		Assert.assertEquals(incidentDes, BaseClass.getattribute(incidentDescription, "value"));
		BaseClass.logpass("Verified cancel button is working as expected","");
	}
	
	public void claimTypeVerification(){
		//need to pick pending claim for it then open it
		//navigatetoreserve screen
		//in reserve page code should be like - pass the category in method for which reserve amount need to be entered
		//open the worksheet, search the category  by name & then entered the amount in all sub categories
		// save the worksheet . navigate to claimant screen. validate the claim type
		//for only nylaw indeminity category in not present on reserve screen. but except litigation & recovery for all category claim type displa as imdeminity
		// for this scenario need to pick the new claim 
	}
	
	
		
		
			 
}
