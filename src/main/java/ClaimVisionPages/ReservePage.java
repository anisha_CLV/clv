package ClaimVisionPages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class ReservePage {
	
	private ReservePage(){}
	private static final ReservePage reserve=new ReservePage();
	String claimReserve, reserveWorksheet, createReserveChangeNote = "";

	public static ReservePage getinstance()
	{
		return reserve;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void navigateToReserve() {
		// TODO Auto-generated method stub
		
	}
	
	@FindBy(id="Reserves_WorksheetButton")
	WebElement worksheetbutton;
	
	@FindBy(id="ReservesForNonComp_WorksheetButton")
	WebElement worksheetbuttonNC;
	
	@FindBy(id="Reserves_claimNumberTextBox")
	WebElement reserveclaimnumber;
	
	@FindBy(id="ReservesForNonComp_claimNumberTextBox")
	WebElement reserveclaimnumberNC;
	
	@FindBy(id="PostButton")
	WebElement saveReserve;
	
	@FindBy(id="SaveNoteButton")
	WebElement saveNoteButton;
	
	@FindBy(id="CloseButton")
	WebElement closeReserve;
	
	
	@FindBy(css = "tbody#Reserves_ReservesGridView > tbody > tr:nth-child(6)")
	WebElement reserveTotal;
	
	public void getReserveDataForPayment(HashMap<String, String> hm) {
		if(hm.get("PolicyType").contains("Compensation"))
		{
			BaseClass.verifyTitle("Claim Reserves");
			BaseClass.logpass("Reserves screen should be displayed", "Reserves screen is displayed");
			setreserveWC(hm);
		}
		else
		{
			BaseClass.verifyTitle("Claim Reserves For Non Comp");
			BaseClass.logpass("Reserves screen should be displayed", "Reserves screen is displayed");
			setreserveNC(hm);
		}
			
		}
	@FindBy(id="ReservesForNonComp_ReservesGridView_ctl02_CategoryLabel")
	WebElement paymentCategory;
	private void setreserveNC(HashMap<String, String> hm) {
		BaseClass.waitForvaluePresent(reserveclaimnumberNC);
		Wait.waitFor(1);
		if(!hm.get("CustomerCode").contains("MDMTA"))
		{
		hm.put("PaymentCategoryNC", BaseClass.gettext(paymentCategory));
		if(hm.get("PaymentCategoryNC").equals("Boiler & Machinery"))
		{
			hm.put("PaymentCategoryNC", "Boiler and Machinery");
		}
		}
		claimReserve = BaseClass.getcurrentwindow();
		BaseClass.click(worksheetbuttonNC);
		reserveWorksheet = BaseClass.switchToNextWindow(claimReserve,"Reserve Worksheet For Non Comp",2);
		setReserveAll();
		/*if(Integer.parseInt(hm.get("LineItems"))>1)
		{
			setReserveAll();
		}
		else
		{
			hm.put("ReserveAmount",BaseClass.enterRandomNumber(4));
			System.out.println(hm.get("ReserveAmount"));
			int PaymentSubCategoryId=Integer.parseInt(BaseClass.getattribute(BaseClass.getDriver().findElement(By.xpath("//span[text()='"+hm.get("PaymentSubCategory")+"']")),"id").split("_")[1].substring(3));
			setReserve(PaymentSubCategoryId, hm.get("ReserveAmount"));
		}*/
		BaseClass.click(saveReserve);
		BaseClass.switchToNextWindow(reserveWorksheet,"Create Reserve Change Note?",3);
		BaseClass.click(saveNoteButton);
		BaseClass.switchToWindow(reserveWorksheet,2);
		BaseClass.click(closeReserve);
		BaseClass.switchToWindow(claimReserve,1);
		Wait.waitFor(3);	
	}

	private void setReserveAll()
	{
		List<WebElement> reserveamount = BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'ChangeReserveTextBox')]"));
		List<WebElement> reservereason = BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'ChangeReasonTextBox')]"));
		for(int i=0;i<reserveamount.size();i++)
		{
			BaseClass.settext(reserveamount.get(i),BaseClass.enterRandomNumber(4));
			BaseClass.settext(reservereason.get(i),BaseClass.stringGeneratorl(5));

		}
	}
	private void setreserveWC(HashMap<String, String> hm)
	{
		BaseClass.waitForvaluePresent(reserveclaimnumber);
				claimReserve = BaseClass.getcurrentwindow();
				BaseClass.click(worksheetbutton);
				reserveWorksheet = BaseClass.switchToNextWindow(claimReserve,"Reserve Worksheet",2);
				List<WebElement> expandbuttons = BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'HiddenImageButton')]"));
				for(int i=0;i<expandbuttons.size();i++)
				{
					Wait.waitFor(2);
					if(BaseClass.getattribute(BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'HiddenImageButton')]")).get(i), "src").contains("Expand"))
					{
						BaseClass.click(BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'HiddenImageButton')]")).get(i));					}
				}
				if(Integer.parseInt(hm.get("LineItems"))>1)
				{
					setReserveAll();
				}
				else
				{
				hm.put("ReserveAmount",BaseClass.enterRandomNumber(4));
				System.out.println(hm.get("ReserveAmount"));
				int PaymentCategoryId=Integer.parseInt(BaseClass.getattribute(BaseClass.getDriver().findElement(By.xpath("//span[text()='"+hm.get("PaymentCategory")+"']")),"id").split("_")[1].substring(3));
				if(!BaseClass.gettext(BaseClass.getDriver().findElement(By.xpath("//table[@id='ReservesGridView']/tbody/tr["+(PaymentCategoryId+1)+"]/td[2]"))).contains("$"))
				{
					setReserve(PaymentCategoryId, hm.get("ReserveAmount"));
				}
				else
				{
				if(hm.get("PaymentSubCategory").equals(""))
				{
						setReserve(PaymentCategoryId+1, hm.get("ReserveAmount"));
				}
				else
				{
					int PaymentSubCategoryId=Integer.parseInt(BaseClass.getattribute(BaseClass.getDriver().findElement(By.xpath("//span[text()='"+hm.get("PaymentSubCategory")+"']")),"id").split("_")[1].substring(3));
						setReserve(PaymentSubCategoryId, hm.get("ReserveAmount"));

				}
				}
				}
				BaseClass.click(saveReserve);
				try
				{
				BaseClass.switchToNextWindow(reserveWorksheet,"Create Reserve Change Note?",3);
				BaseClass.click(saveNoteButton);
				BaseClass.switchToWindow(reserveWorksheet,2);
				}
				catch(Exception e){}
				BaseClass.click(closeReserve);
				BaseClass.switchToWindow(claimReserve,1);
				Wait.waitFor(3);
	}
	public void setReserve(int value,String reserveAmount)
	{
		if(value<10)
		{
		BaseClass.settext(BaseClass.getDriver().findElement(By.id("ReservesGridView_ctl0"+value+"_ChangeReserveTextBox")),reserveAmount);
		BaseClass.settext(BaseClass.getDriver().findElement(By.id("ReservesGridView_ctl0"+value+"_ChangeReasonTextBox")),reserveAmount);
		}
		else
		{
		BaseClass.settext(BaseClass.getDriver().findElement(By.id("ReservesGridView_ctl"+value+"_ChangeReserveTextBox")),reserveAmount);
		BaseClass.settext(BaseClass.getDriver().findElement(By.id("ReservesGridView_ctl"+value+"_ChangeReasonTextBox")),reserveAmount);
		}
	}
	
	public void setReserveWC(String scenario)
	{
		BaseClass.waitForvaluePresent(reserveclaimnumber);
		claimReserve = BaseClass.getcurrentwindow();
		BaseClass.click(worksheetbutton);
		reserveWorksheet = BaseClass.switchToNextWindow(claimReserve,"Reserve Worksheet",2);
		List<WebElement> expandbuttons = BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'HiddenImageButton')]"));
		for(int i=0;i<expandbuttons.size();i++)
		{
			Wait.waitFor(2);
			if(BaseClass.getattribute(BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'HiddenImageButton')]")).get(i), "src").contains("Expand"))
			{
				BaseClass.click(BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'HiddenImageButton')]")).get(i));					}
		}
		if(scenario.equalsIgnoreCase("Mandatory"))
		{
			setReserveMandatory();
		}
		else
		{
			setReserveAll();
		}
		
		BaseClass.click(saveReserve);
		if(scenario.equalsIgnoreCase("Mandatory"))
		{
		Assert.assertEquals(BaseClass.gettext(mandatoryText), "Reserve Reason is required");
		}
		else
		{
		BaseClass.switchToNextWindow(reserveWorksheet,"Create Reserve Change Note?",3);
		BaseClass.click(saveNoteButton);
		BaseClass.switchToWindow(reserveWorksheet,2);
		BaseClass.click(closeReserve);
		BaseClass.switchToWindow(claimReserve,1);
		Wait.waitFor(3);
	}
	}
	@FindBy(xpath="//li[contains(text(),'Reserve Reason is required')]")
	WebElement mandatoryText;
	private void setReserveNC(String scenario){
		BaseClass.waitForvaluePresent(reserveclaimnumberNC);
		Wait.waitFor(1);
		claimReserve = BaseClass.getcurrentwindow();
		BaseClass.click(worksheetbuttonNC);
		reserveWorksheet = BaseClass.switchToNextWindow(claimReserve,"Reserve Worksheet For Non Comp",2);
		if(scenario.equalsIgnoreCase("Mandatory"))
		{
			setReserveMandatory();
		}
		else
		{
			setReserveAll();
		}
		BaseClass.click(saveReserve);
		if(scenario.equalsIgnoreCase("Mandatory"))
		{
		Assert.assertEquals(BaseClass.gettext(mandatoryText), "Reserve Reason is required.");
		}
		else
		{
		BaseClass.switchToNextWindow(reserveWorksheet,"Create Reserve Change Note?",3);
		BaseClass.click(saveNoteButton);
		BaseClass.switchToWindow(reserveWorksheet,2);
		BaseClass.click(closeReserve);
		BaseClass.switchToWindow(claimReserve,1);
		Wait.waitFor(3);
	}
	}
	
	public void setReserve(HashMap<String, String> hm) {
		if(hm.get("PolicyType").contains("Compensation"))
		{
			BaseClass.verifyTitle("Claim Reserves");
			BaseClass.logpass("Reserves screen should be displayed", "Reserves screen is displayed");
			setReserveWC(hm.get("Scenario"));
		}
		else
		{
			BaseClass.verifyTitle("Claim Reserves For Non Comp");
			BaseClass.logpass("Reserves screen should be displayed", "Reserves screen is displayed");
			setReserveNC(hm.get("Scenario"));
		}
			
		}
	
	public void setReserveMandatory()
	{
		List<WebElement> reserveamount = BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'ChangeReserveTextBox')]"));
		List<WebElement> reservereason = BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'ChangeReasonTextBox')]"));
		for(int i=0;i<reserveamount.size();i++)
		{
			BaseClass.settext(reserveamount.get(i),BaseClass.enterRandomNumber(4));
		}
	}
}
