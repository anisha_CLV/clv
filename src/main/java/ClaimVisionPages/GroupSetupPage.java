package ClaimVisionPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class GroupSetupPage {
	private static final GroupSetupPage groupSetupPage = new GroupSetupPage();

	private GroupSetupPage() {
	}

	public static GroupSetupPage getinstance() {
		return groupSetupPage;
	}
	public void initElement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Group Set-up")
	WebElement groupsetup;
	
	@FindBy(linkText="AutoGroup")
	WebElement groupname;
	
	@FindBy(id="GroupSetup_groupNameTextBox")
	WebElement groupnametextbox;
	
	@FindBy(id="tab4")
	WebElement permissiontab;
	
	@FindBy(id="GroupSetup_permissionGroup_ctl01_IsActivedCheckBox")
	WebElement paymentValidationCheckBox;
	
	@FindBy(id="GroupSetup_permissionGroup_ctl03_IsActivedCheckBox")
	WebElement markClaimasQuestionable;
	
	@FindBy(id="GroupSetup_permissionGroup_ctl02_IsActivedCheckBox")
	WebElement makePaymenttoUnauthorizeVendor;
	
	@FindBy(id="GroupSetup_permissionGroup_ctl04_IsActivedCheckBox")
	WebElement authorizePaymentOnQuestionableClaim;
	
	@FindBy(id="GroupSetup_permissionGroup_ctl05_IsActivedCheckBox")
	WebElement authorizePaymentOnDeniedClaim;
	
	@FindBy(id="GroupSetup_permissionGroup_ctl06_IsActivedCheckBox")
	WebElement approvePotentialDuplicatePayment;
	
	@FindBy(id="GroupSetup_saveButton")
	WebElement savegroupsetup;
	
	@FindBy(id="GroupSetup_messageLabel")
	WebElement groupsetupvalidationmessage;
	
	public void setGroupPermission(String Scenario,String Permission)
	{
		BaseClass.click(groupsetup);
		BaseClass.click(groupname);
		BaseClass.waitForvaluePresent(groupnametextbox);
		Wait.waitFor(5);
		BaseClass.click(permissiontab);
		Wait.waitforelement(BaseClass.getDriver(), paymentValidationCheckBox);
		if(!paymentValidationCheckBox.isSelected())
		{
			BaseClass.click(paymentValidationCheckBox);
			Wait.waitFor(2);
			BaseClass.click(paymentValidationCheckBox);
		}
		else
		{
			BaseClass.click(paymentValidationCheckBox);
		}
		BaseClass.click(markClaimasQuestionable);
		if(Scenario.equals("Unauthorized")&& Permission.equals("Yes"))
		{
			BaseClass.click(makePaymenttoUnauthorizeVendor);
		}
		if(Scenario.equals("Questionable")&& Permission.equals("Yes"))
		{
				BaseClass.click(authorizePaymentOnQuestionableClaim);
		}
		if(Scenario.equals("Denied")&& Permission.equals("Yes"))
		{
			BaseClass.click(authorizePaymentOnDeniedClaim);
		}
		if(Scenario.equals("Duplicate")&& Permission.equals("Yes"))
		{
			BaseClass.click(approvePotentialDuplicatePayment);
		}
		
		BaseClass.click(savegroupsetup);
		BaseClass.waitFortextPresent(groupsetupvalidationmessage);
		Assert.assertEquals(BaseClass.gettext(groupsetupvalidationmessage), "Saved successfully.");
		BaseClass.click(permissiontab);
		BaseClass.logpass("Permission should be set on Group Set-Up Screen", "Permission are set on Group Set-Up Screen");
	}
	
	public void setGroupPermissionForQuestionable()
	{
		BaseClass.click(groupsetup);
		BaseClass.click(groupname);
		BaseClass.waitForvaluePresent(groupnametextbox);
		BaseClass.logInfo("Group Name: ", BaseClass.getattribute(groupnametextbox, "value"));
		BaseClass.click(permissiontab);
		Wait.waitforelement(BaseClass.getDriver(), paymentValidationCheckBox);
		if(!paymentValidationCheckBox.isSelected())
		{
			BaseClass.click(paymentValidationCheckBox);
			Wait.waitFor(2);
			BaseClass.click(paymentValidationCheckBox);
		}
		else
		{
			BaseClass.click(paymentValidationCheckBox);
		}
		
		
		
		/*if(Scenario.equals("QuestionableWithPermission") ){
			if((!markClaimasQuestionable.isSelected()))
				{
					BaseClass.click(markClaimasQuestionable);
				}
			}
		else if(Scenario.equals("QuestionableWithoutPermission") ){
			if((markClaimasQuestionable.isSelected()))
			{
				BaseClass.click(markClaimasQuestionable);
			}
		}*/
		
		
		BaseClass.click(savegroupsetup);
		BaseClass.waitFortextPresent(groupsetupvalidationmessage);
		Assert.assertEquals(BaseClass.gettext(groupsetupvalidationmessage), "Saved successfully.");
		BaseClass.click(permissiontab);
		BaseClass.logpass("Permission should be set on Group Set-Up Screen", "Permission are set on Group Set-Up Screen");
	}

}
