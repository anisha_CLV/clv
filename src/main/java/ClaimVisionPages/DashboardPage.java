package ClaimVisionPages;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentTest;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class DashboardPage {

	private DashboardPage() {
	}

	private static final DashboardPage dashboard = new DashboardPage();

	public static DashboardPage getinstance() {
		return dashboard;
	}

	@FindBy(id = "ClaimSearch_claimNumberTextBox")
	public WebElement claimSearchTextbox;

	@FindBy(id = "superFindImageButton")
	public WebElement superfind;

	@FindBy(css = "div#menuTitleHome")
	public WebElement userActivity;

	@FindBy(css = "input#btnAgree")
	public WebElement agree;

	// Web Elements of Fin Approval Hub
	// input[@id='username']
	@FindBy(linkText = "Fin Approval Hub")
	public WebElement finApprovalHub;

	// Web Elements of Diary
	@FindBy(css = "a#diaryLinkButton")
	public WebElement diary;

	// Web Elements of IRF
	@FindBy(css = "div#menuTitleIRF.menuTitle")
	public WebElement irf;

	// Web Elements of Claims menu
	@FindBy(css = "img#claimsImage")
	public WebElement claimsArrow;

	// Web Elements of Claim link
	@FindBy(css = "a#claimsTreeViewt1")
	public WebElement claim;

	//@FindBy(css = "a#claimsTreeViewt1")
	//public WebElement claimantIncident;
	
	@FindBy(css = "a#claimsTreeViewt2")
	public WebElement employeeIncident;
	
	@FindBy(linkText="CLAIMANT / INCIDENT")
	public WebElement claimantIncident;

	// Web Elements of Claimant link
	@FindBy(css = "a#claimsTreeViewt2")
	public WebElement claimant;

	// Web Elements of Sub Claim link
	@FindBy(css = "a#claimsTreeViewt3")
	public WebElement subClaim;

	// Web Elements of Client/State Specific link
	@FindBy(css = "a#claimsTreeViewt4")
	public WebElement clientStateSpecific;

	// Web Elements of Financial Summary link
	@FindBy(css = "a#claimsTreeViewt5")
	public WebElement financialSummary;

	@FindBy(css = "a#claimsTreeViewt5")
	public WebElement medicalTreatment;

	// Web Elements of Financial Summary link
	@FindBy(css = "a#claimsTreeViewt4")
	public WebElement wc_financialSummary;

	// Web Elements of Notes link
	@FindBy(css = "a#claimsTreeViewt6")
	public WebElement notesforNonComp;

	// Web Elements of Reserves link
	@FindBy(linkText = "Reserves")
	public WebElement reserves;

	@FindBy(css = "a#claimsTreeViewt8")
	public WebElement reservesForWC;

	@FindBy(linkText = "Payments")
	public WebElement paymentForNComp;

	// Web Elements of Payments link
	@FindBy(css = "a#claimsTreeViewt8")
	public WebElement payments;

	// Web Elements of Payments link
	@FindBy(css = "a#claimsTreeViewt10")
	public WebElement paymentsForWC;

	@FindBy(css = "a#claimsTreeViewt11")
	public WebElement masterClaimAllocation;

	// Web Elements of Litigation link
	@FindBy(linkText = "Litigation")
	public WebElement litigation;

	// Web Elements of Claim Authority Limits link
	@FindBy(css = "a#claimsTreeViewt10")
	public WebElement claimAuthorityLimit;

	// Web Elements of Finance menu
	@FindBy(css = "img#financialImage")
	public WebElement financeArrow;

	// Web Elements of TransactionSearch link
	@FindBy(css = "a#financialTreeViewt1")
	public WebElement transactionSearch;

	// Web Elements of PaymentTransactionSearch link
	@FindBy(css = "a#financialTreeViewt2")
	public WebElement PaymentTransactionDetail;

	@FindBy(css = "a#financialTreeViewn0")
	public WebElement paymentHistory;

	// Web Elements of CheckHistory link
	@FindBy(css = "a#financialTreeViewt3")
	public WebElement checkHistory;
	
	
	// Web Elements of Recoveries link
	@FindBy(css = "a#financialTreeViewt4")
	public WebElement recoveries;

	// Web Elements of RecipientOfBenefit link
	@FindBy(linkText = "Recipient of Benefit")
	public WebElement recipientOfBenefit;

	@FindBy(css = "a#financialTreeViewn6")
	public WebElement checkAdminLink;

	// Web Elements of ReleasePayments link
	@FindBy(css = "a#financialTreeViewt7")
	public WebElement releasePayment;

	// Web Elements of SelectPayment link
	@FindBy(css = "a#financialTreeViewt8")
	public WebElement selectPaymentForCheck;

	// Web Elements of CheckWriting link
	@FindBy(css = "a#financialTreeViewt9")
	public WebElement checkWriting;

	// Web Elements of PostedCheckReversal link
	@FindBy(css = "a#financialTreeViewt10")
	public WebElement postedCheckReversal;

	// Web Elements of Correspondence menu
	@FindBy(css = "img#formsImage")
	public WebElement correspondenceArrow;

	// Web Elements of AttachCorrespondence link
	@FindBy(css = "a#formsTreeViewt0")
	public WebElement attachCorrespondence;

	// Web Elements of CorrespondenceHistory link
	@FindBy(css = "a#formsTreeViewt1")
	public WebElement correspondenceHistory;

	// Web Elements of PrintCorrespondence link
	@FindBy(css = "a#formsTreeViewt2")
	public WebElement printCorrespondence;

	// Web Elements of Admin link
	@FindBy(id = "adminImage")
	public WebElement adminArrow;

	// Web Elements of AccountClient link
	@FindBy(css = "a#adminTreeViewn0")
	public WebElement accountAndClient;

	// Web Elements of CompanyProfileSet link
	@FindBy(css = "a#adminTreeViewt1")
	public WebElement companyProfileSet;

	// Web Elements of CompanyHierarchy link
	@FindBy(css = "a#adminTreeViewt2")
	public WebElement companyHierarchy;

	// Web Elements of BankingInformation link
	@FindBy(css = "a#adminTreeViewn3")
	public WebElement bankingInformation;

	// Web Elements of Bank link
	@FindBy(css = "a#adminTreeViewt4")
	public WebElement bank;

	// Web Elements of BankAccount link
	@FindBy(css = "a#adminTreeViewt5")
	public WebElement bankAccount;

	// Web Elements of PaymentMode link
	@FindBy(css = "a#adminTreeViewt6")
	public WebElement paymentMode;

	// Web Elements of Permission link
	@FindBy(css = "a#adminTreeViewn7")
	public WebElement permission;

	// Web Elements of GroupSetup link
	@FindBy(css = "a#adminTreeViewt8")
	public WebElement groupSetUp;

	// Web Elements of UserSetup link
	@FindBy(css = "a#adminTreeViewt9")
	public WebElement userSetUp;

	// Web Elements of GroupUserSetup link
	@FindBy(css = "a#adminTreeViewt10")
	public WebElement groupUserSetup;

	// Web Elements of FieldLevelSecurity link
	@FindBy(css = "a#adminTreeViewt11")
	public WebElement fieldLevelSecurity;

	// Web Elements of AuditInformation link
	@FindBy(css = "a#adminTreeViewt12")
	public WebElement auditInformation;

	// Web Elements of DocumentService link
	@FindBy(css = "a#adminTreeViewt13")
	public WebElement documentService;

	// Web Elements of UserPolicySetup link
	@FindBy(css = "a#adminTreeViewt14")
	public WebElement userPolicySetUp;

	// Web Elements of AuthorizedApproverSetup link
	@FindBy(css = "a#adminTreeViewt15")
	public WebElement authorizedApproverSetup;

	// Web Elements of Customization link
	@FindBy(linkText = "Customization")
	public WebElement customization;

	// Web Elements of RequiredFieldSetup link
	@FindBy(css = "a#adminTreeViewt17")
	public WebElement requiredFieldSetup;

	// Web Elements of SpecialAnalysisField link
	@FindBy(css = "a#adminTreeViewt18")
	public WebElement specialAnalysisField;

	// Web Elements of StateSpecificFieldSetup link
	@FindBy(css = "a#adminTreeViewt19")
	public WebElement stateSpecificFielsSetUp;

	// Web Elements of SupportTable link
	@FindBy(css = "a#adminTreeViewt20")
	public WebElement supportTable;

	// Web Elements of PolicyType link
	@FindBy(css = "a#adminTreeViewt21")
	public WebElement policyType;

	// Web Elements of AggregateLimitDetail link
	@FindBy(css = "a#adminTreeViewt22")
	public WebElement aggregateLimitDetail;

	// Web Elements of CoverageLossType link
	@FindBy(css = "a#adminTreeViewt23")
	public WebElement coverageLossType;

	// Web Elements of Catastrophe link
	@FindBy(css = "a#adminTreeViewt24")
	public WebElement catastrophe;

	// Web Elements of MaintainPaymentCategory link
	@FindBy(css = "a#adminTreeViewt25")
	public WebElement paymentCategorySetup;

	// Web Elements of NonCompMaintainPaymentCategory link
	@FindBy(css = "a#adminTreeViewt26")
	public WebElement nonCompMaintainPaymentCategory;

	// Web Elements of ClassCode link
	@FindBy(css = "a#adminTreeViewt27")
	public WebElement classCode;

	// Web Elements of CustomizeItemField link
	@FindBy(css = "a#adminTreeViewt28")
	public WebElement customizeItemField;

	// Web Elements of PrivacyAuthorityLevel link
	@FindBy(css = "a#adminTreeViewt29")
	public WebElement privacyAuthorityLevel;

	// Web Elements of PrivacyReasonLevel link
	@FindBy(css = "a#adminTreeViewt30")
	public WebElement privacyReasonLevel;

	// Web Elements of ClaimFlagLevelSetup link
	@FindBy(css = "a#adminTreeViewt31")
	public WebElement claimFlagLevelSetup;

	@FindBy(css = "input#ClaimSearch_searchButton")
	public WebElement searchClaimButton;

	// Web Elements of PolicyNumberAssignment link
	@FindBy(linkText = "Policy Number Assignment")
	public WebElement policyNumberAssignment;

	// Web Elements of Vendor link
	@FindBy(linkText = "Vendors")
	public WebElement vendor;

	// Web Elements of VendorSetup link
	@FindBy(linkText = "Vendor Set-up")
	public WebElement vendorSetup;

	// Web Elements of VendorMerge link
	@FindBy(linkText = "Vendor Merge")
	public WebElement vendorMerge;

	// Web Elements of DocumentManagement Link
	@FindBy(css = "a#adminTreeViewn36")
	public WebElement documentMgt;

	// Web Elements of CorrespondenceDefintion Link
	@FindBy(css = "a#adminTreeViewt37")
	public WebElement correspondenceDefinition;

	// Web Elements of CorrespondenceAssignment Link
	@FindBy(css = "a#adminTreeViewt38")
	public WebElement correspondenceAssignment;

	// Web Elements of AutomatedDiaryDefinition Link
	@FindBy(css = "a#adminTreeViewt39")
	public WebElement automatedDiaryDefinition;

	// Web Elements of ConditionalFormAttachment Link
	@FindBy(css = "a#adminTreeViewt40")
	public WebElement conditionalFormAttachment;

	// Web Elements of BusinessRuleVariables Link
	@FindBy(css = "a#adminTreeViewt41")
	public WebElement businessRuleVariables;

	// Web Elements of PolicySetup Link
	@FindBy(linkText = "Policy Set Up")
	public WebElement policySetUp;

	

	/*// Web Elements of WCPolicy Link
	@FindBy(css = "a#adminTreeViewt44")
	public WebElement wCPolicy;

	// Web Elements of WCOrgPolicy Link
	@FindBy(css = "a#adminTreeViewt45")
	public WebElement wCOrgPolicy;

	// Web Elements of PolicyCoverage Link
	@FindBy(css = "a#adminTreeViewt46")
	public WebElement wCPolicyCoverage;

	// Web Elements of PolicyClassCode Link
	@FindBy(css = "a#adminTreeViewt47")
	public WebElement wCPolicyClassCode;

	// Web Elements of NonComp Link
	@FindBy(linkText = "Non Comp Policy")
	public WebElement nonComp;*/

	// Web Elements of NonCompPolicy Link
	@FindBy(xpath = "//a[contains(@href,'Non Comp Policy')]")
	public WebElement nCPolicy;
	
	// Web Elements of Policy Link of non comp
		@FindBy(css = "a#adminTreeViewt51")
		public WebElement nonCompPolicy;
	
	// Web Elements of WC Link
		@FindBy(linkText = "WC")
		public WebElement wC;
	
		//Web element of WC Policy link
	@FindBy(css = "a#adminTreeViewt46")
	public WebElement compPolicy;
	
	

	// Web Elements of NonCompOrgPolicy Link
	@FindBy(css = "a#adminTreeViewt50")
	public WebElement nonCompOrgPolicy;

	

	// Web Elements of NonCompPolicyItemCoverage Link
	@FindBy(css = "a#adminTreeViewt52")
	public WebElement nonCompPolicyItemCoverage;

	// Web Elements of NonCompPolicyView Link
	@FindBy(css = "a#adminTreeViewt53")
	public WebElement nonCompPolicyView;

	// Web Elements of State Link
	@FindBy(css = "a#adminTreeViewn54")
	public WebElement state;

	// Web Elements of BenefitGuidelines Link
	@FindBy(css = "a#adminTreeViewt55")
	public WebElement benefitGuidelines;

	// Web Elements of ReassignClaim Link
	@FindBy(css = "a#adminTreeViewt56")
	public WebElement ReassignClaim;

	@FindBy(css = "input#claimSearchImageButton")
	public WebElement searchClaim;

	@FindBy(linkText = "Payments")
	public WebElement payment;

	@FindBy(css = "a#claimsTreeViewt10")
	public WebElement paymentForWC;

	@FindBy(css = "a#claimsTreeViewt7")
	public WebElement notesForWC;

	@FindBy(id = "ClaimSearch_policyTypeDropDown")
	public WebElement claimsearch_policytype;

	@FindBy(id = "ClaimSearch_CustomizationGrid1_CustomGridView_ctl01_EntryDate")
	public WebElement claimsearchEntryDateSort;

	@FindBy(id = "ClaimSearch_statusDropDown")
	WebElement claimsearch_status;

	public void navigateToIRF(String parent) {
		BaseClass.click(irf);
		BaseClass.logInfo("IRF link is clicked", "");
		BaseClass.switchToNextWindow(parent, "IRF", 2);
		BaseClass.verifyTitle("IRF");
		BaseClass.logpass("IRF screen should be displayed", "IRF screen is displayed");
	}

	public void navigateToPayment() {
		BaseClass.click(payment);
		BaseClass.logInfo("Payments link is clicked", "");
		BaseClass.verifyTitle("Payments");
		BaseClass.logpass("Payments screen should be displayed", "Payments screen is displayed");
	}

	public void initElement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void navigateToClaimSearch() {
		while (!BaseClass.isVisible(claimSearchTextbox)) {
			BaseClass.click(searchClaim);
			Wait.waitFor(3);
		}
	}

	public void enterClaimNumber(String claimnumber) {
		BaseClass.settext(claimSearchTextbox, claimnumber);
	}

	public void selectPolicyType(String policyType) {
		BaseClass.selectByVisibleText(claimsearch_policytype, policyType);
	}

	public void sortByEntryDate() {
		BaseClass.click(claimsearchEntryDateSort);
		while (BaseClass.getattribute(claimsearchEntryDateSort, "src").contains("Sort")) {
			Wait.waitFor(1);
		}
	}

	public void selectStatus(String status) {

		BaseClass.selectByVisibleText(claimsearch_status, status);

	}

	public void clickSearch() {
		BaseClass.click(searchClaimButton);

	}

	@FindBy(id = "ClaimSearch_lnkBtnExportToExcel")
	WebElement exportToExcel;

	public void selectFirstClaim(String claimNumber) {
		// Wait for the Grid to be displayed
		BaseClass.gettext(exportToExcel);
		BaseClass.click(BaseClass.getDriver().findElement(By.linkText(claimNumber)));
		System.out.println("claim is clicked");
	}

	public void navigateToReserve() {

		BaseClass.click(reserves);
		BaseClass.logInfo("Reserves link is clicked", "");

	}

	@FindBy(id = "menuTitleFinancial")
	WebElement finance;

	public void navigateToVendorSetup() {
		BaseClass.click(adminArrow);
		BaseClass.logInfo("Admin Link is Clicked", "");
		Wait.waitFor(2);
		BaseClass.click(vendor);
		BaseClass.logInfo("Vendor Link is Clicked", "");
		Wait.waitFor(1);
		BaseClass.Scrolltoelement(vendorSetup);
		BaseClass.click(vendorSetup);
		BaseClass.logInfo("Vendor Setup Link is Clicked", "");
		Wait.waitForTitle(BaseClass.getDriver(), "Vendor Set-up");
		Assert.assertTrue(BaseClass.getDriver().getTitle().equals("Vendor Set-up"));
		BaseClass.logpass("Vendor Set-up screen should be displayed", "Vendor Set-up screen is displayed");
	}

	public void navigateToVendorMerge() {
		BaseClass.click(adminArrow);
		BaseClass.logInfo("Admin Link is Clicked", "");
		Wait.waitFor(2);
		BaseClass.click(vendor);
		BaseClass.logInfo("Vendor Link is Clicked", "");
		Wait.waitFor(1);
		BaseClass.Scrolltoelement(vendorMerge);
		BaseClass.click(vendorMerge);
		BaseClass.logInfo("Vendor Merge Link is Clicked", "");
		Wait.waitForTitle(BaseClass.getDriver(), "Vendor Merge");
		Assert.assertTrue(BaseClass.getDriver().getTitle().equals("Vendor Merge"));
		BaseClass.logpass("Vendor Merge screen should be displayed", "Vendor Merge screen is displayed");
	}

	public void navigateToROB() {
		BaseClass.click(finance);
		System.out.println("finance is clicked");
		BaseClass.click(recipientOfBenefit);
		System.out.println("ROB");

	}

	public void navigateToPTDS() {
		
			if(!PaymentTransactionDetail.isDisplayed())
			{
				if(!paymentHistory.isDisplayed())
				{
					BaseClass.click(finance);
					BaseClass.logInfo("Finance Link is Clicked", "");
					BaseClass.click(paymentHistory);
					BaseClass.logInfo("Payment History Link is Clicked", "");
					BaseClass.click(PaymentTransactionDetail);
					BaseClass.logInfo("Payment Transaction Detail Link is Clicked", "");
				}
				else
				{
					BaseClass.click(paymentHistory);
					BaseClass.logInfo("Payment History Link is Clicked", "");
					BaseClass.click(PaymentTransactionDetail);
					BaseClass.logInfo("Payment Transaction Detail Link is Clicked", "");
				}
			}
			else
			{
				BaseClass.click(PaymentTransactionDetail);
				BaseClass.logInfo("Payment Transaction Detail Link is Clicked", "");
			}		
		Wait.waitForTitle(BaseClass.getDriver(), "Payment Transaction Detail");
		BaseClass.logpass("Payment Transaction Detail Screen should be displayed", "Payment Transaction Detail Screen is displayed");
		}
		

	public void navigateToReleasePayment() {
		
			if(!releasePayment.isDisplayed())
			{
				if(!checkAdminLink.isDisplayed())
				{
					BaseClass.click(finance);
					BaseClass.logInfo("Finance Link is Clicked", "");
					BaseClass.click(checkAdminLink);
					BaseClass.logInfo("Check Admin Link is Clicked", "");
					BaseClass.click(releasePayment);
					BaseClass.logInfo("Release Payment Link is Clicked", "");
				}
				else
				{
					BaseClass.click(checkAdminLink);
					BaseClass.logInfo("Check Admin Link is Clicked", "");
					BaseClass.click(releasePayment);
					BaseClass.logInfo("Release Payment Link is Clicked", "");
				}
			
				
			}
			else
			{
				BaseClass.click(releasePayment);
				BaseClass.logInfo("Release Payment Link is Clicked", "");
			}
		
		Wait.waitForTitle(BaseClass.getDriver(), "Release Payments");
		BaseClass.logpass("Release Payment Screen should be displayed", "Release Payment Screen is displayed");
		
	}
	
	public void navigateToFinancialSummary() {
		BaseClass.click(wc_financialSummary);
		BaseClass.logInfo("Financial Summary link is clicked", "");
		
	}
	
	public void navigateToEmployeeIncident(HashMap<String, String> hm) {
		
		BaseClass.click(employeeIncident);
		if(hm.get("Scenario").equals("ReOpened") && hm.get("Browser").equals("IE"))
			BaseClass.acceptalert();
		BaseClass.logInfo("Employee Incident link is clicked", "");
		
	}
	
	public void navigateToMedicalTreatment(HashMap<String, String> hm) {
		
		BaseClass.click(medicalTreatment);
		if(hm.get("Scenario").equals("ReOpened") && hm.get("Browser").equals("IE"))
			BaseClass.acceptalert();
		BaseClass.logInfo("Medical Treatment link is clicked", "");
		
	}
	
	public void navigateToClaimantIncident() {
		BaseClass.click(claimantIncident);
		BaseClass.logInfo("Claimant Incident link is clicked", "");
		
	}
	public void navigateToClaim() {
		BaseClass.click(claim);
		BaseClass.logInfo("Claim link is clicked", "");
		
	}
	
	public void navigateToClaimant(HashMap<String, String> hm, Boolean status) {
		BaseClass.click(claimant);
		if(hm.get("Scenario").equals("ReOpened") && hm.get("Browser").equals("IE") &&
				status==true)
			BaseClass.acceptalert();
		BaseClass.logInfo("Claimant link is clicked", "");
		
	}
	
	public void navigateToLitigation() {
		BaseClass.click(litigation);
		BaseClass.logInfo("Litigation link is clicked", "");
	}

	public void navigateToCompPolicy() {
		BaseClass.click(adminArrow);
		BaseClass.logInfo("Admin link is clicked", "");
		BaseClass.click(policySetUp);
		BaseClass.logInfo("Policy SetUp link is clicked", "");
		BaseClass.click(wC);
		BaseClass.logInfo("WC link is clicked", "");
		BaseClass.click(compPolicy);
		BaseClass.logInfo("Clicked on Policy link", "");
		
	}

	
	public void navigateToNonCompPolicy() {
		BaseClass.click(adminArrow);
		BaseClass.logInfo("Admin link is clicked", "");
		BaseClass.click(policySetUp);
		BaseClass.logInfo("Policy SetUp link is clicked", "");
		BaseClass.click(nCPolicy);
		BaseClass.logInfo("Non Comp Policy link is clicked", "");
		BaseClass.click(nonCompPolicy);
		BaseClass.logInfo("Clicked on Policy link", "");
	}

	public void navigateToSelectPaymentForCheck() {
		
			if(!selectPaymentForCheck.isDisplayed())
			{
				if(!checkAdminLink.isDisplayed())
				{
					BaseClass.click(finance);
					BaseClass.logInfo("Finance Link is Clicked", "");
					BaseClass.click(checkAdminLink);
					BaseClass.logInfo("Check Admin Link is Clicked", "");
					BaseClass.click(selectPaymentForCheck);
					BaseClass.logInfo("Select Payment for Check Link is Clicked", "");
					Wait.waitForTitle(BaseClass.getDriver(), "Select Payment For Check");
				}
				else
				{
					BaseClass.click(checkAdminLink);
					BaseClass.logInfo("Check Admin Link is Clicked", "");
					BaseClass.click(selectPaymentForCheck);
					BaseClass.logInfo("Select Payment for Check Link is Clicked", "");
				}
			}
			else
			{
				BaseClass.click(selectPaymentForCheck);
				BaseClass.logInfo("Select Payment for Check Link is Clicked", "");
			}
		
		
		BaseClass.logpass("Select Payment For Check Screen should be displayed",
				"Select Payment For Check Screen is displayed");
	}

	@FindBy(id = "financialTreeViewt10")
	WebElement postalCheckReversal;

	public void navigateToPostalCheckScreen() {
		
			BaseClass.click(finance);
			BaseClass.logInfo("Finance Link is Clicked", "");
			BaseClass.click(postalCheckReversal);
			BaseClass.logInfo("Postal Check Reversal Link is Clicked", "");
			Wait.waitForTitle(BaseClass.getDriver(), "Posted Check Reversal");
			BaseClass.logpass("Posted Check Reversal Screen should be displayed",
					"Posted Check Reversal Screen is displayed");
	}

	public void navigateTocheckWritingScreen() {
		
			if(!checkWriting.isDisplayed())
			{
				if(!checkAdminLink.isDisplayed())
				{
					BaseClass.click(finance);
					BaseClass.logInfo("Finance Link is Clicked", "");
					BaseClass.click(checkAdminLink);
					BaseClass.logInfo("Check Admin Link is Clicked", "");
					BaseClass.click(checkWriting);
					BaseClass.logInfo("Check Writing Link is Clicked", "");
				}
				else
				{
					BaseClass.click(checkAdminLink);
					BaseClass.logInfo("Check Admin Link is Clicked", "");
					BaseClass.click(checkWriting);
					BaseClass.logInfo("Check Writing Link is Clicked", "");
				}
			}
			else
			{
				BaseClass.click(checkWriting);
				BaseClass.logInfo("Check Writing Link is Clicked", "");
			}
		
		Wait.waitForTitle(BaseClass.getDriver(), "Check Writing");
		BaseClass.logpass("Check Writing Screen should be displayed",
				"Check Writing Screen is displayed");
		
	}
}
