package ClaimVisionPages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class MedicalTreatmentPage {
	
	private MedicalTreatmentPage(){}
	private static final MedicalTreatmentPage medicalTreatment=new MedicalTreatmentPage();

	public static MedicalTreatmentPage getinstance()
	{
		return medicalTreatment;
	}
	
	@FindBy(css="input#Medical_PBMTerminationDateTextBox")
	public WebElement pbmTerminationDate;
	
	@FindBy(css="span#Medical_isTreatmentRequired>input")
	public List<WebElement> treatmentRequired;
	
	@FindBy(css="select#Medical_treatmentTypeDropDown")
	public WebElement initialTreatmentType;
	
	@FindBy(css="select#Medical_choiceTypeDropDownList")
	public WebElement choiceDropdown;
	
	@FindBy(css="select#Medical_physicianDataList_ctl01_typeDropDown")
	public WebElement firstTreatmentType;
	
	@FindBy(css="input[id$='nameTextBox']")
	public WebElement nameOfPhysician;
	
	@FindBy(css="select[id$='specialtyDropDown']")
	public WebElement specialtyDropdown;
	
	@FindBy(css="input#Medical_physicianDataList_ctl01_changeReasonTextBox")
	public WebElement changeReason;
	
	@FindBy(css="select[id$='choiceDropDown']")
	public WebElement choiceValueDropdown;
	
	@FindBy(css="input[id$='addICD9CodeButton']")
	public List<WebElement> addICD9CodeButton;
	
	@FindBy(css="input#Medical_Icd9UserControl_icd9CodeTextBox")
	public WebElement searchICD9CodeTextBox;
	
	@FindBy(css="input#Medical_Icd9UserControl_searchButton")
	public WebElement searchButton_ICD9Code;
	
	@FindBy(css="#Medical_Icd9UserControl_searchIcd9Repeater_ctl01_selectIcd9ImageButton")
	public WebElement selectButton_ICD9Code;
	
	@FindBy(css="#Medical_Icd9UserControl_existingIcd9Row > td > div > fieldset > table > tbody > tr > td:nth-child(1)")
	public List<WebElement> valueOfICD9Code_Displayed;
	
	@FindBy(css="#Medical_Icd9UserControl_searchResultsRow > td > div > fieldset > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	public WebElement selectedValueOfIDC9Code;
	
	@FindBy(css="span#Medical_Icd9UserControl_icd9Label")
	public WebElement icd9MessageLabel;
	
	//Icd9 Code already Exists. Please select another one
	
	@FindBy(css="input[id$='addICD10CodeButton']")
	public List<WebElement> addICD10CodeButton;
	
	@FindBy(css="input#Medical_Icd10UserControl_icd10CodeTextBox")
	public WebElement searchICD10CodeTextBox;
	
	@FindBy(css="input#Medical_Icd10UserControl_searchButton")
	public WebElement searchButton_ICD10Code;
	
	@FindBy(css="#Medical_Icd10UserControl_searchIcd10Repeater_ctl01_selectIcd10ImageButton")
	public WebElement selectButton_ICD10Code;
	
	//@FindBy(css="#Medical_Icd10UserControl_existingIcd10Row > td > div > fieldset > table > tbody > tr:nth-child(2) > td:nth-child(1)")
	@FindBy(css="#Medical_Icd10UserControl_existingIcd10Row > td > div > fieldset > table > tbody > tr > td:nth-child(1)")
	public List<WebElement> valueOfICD10Code_Displayed;
	
	@FindBy(css="#Medical_Icd10UserControl_searchResultsRow > td > div > fieldset > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	public WebElement selectedValueOfIDC10Code;

	@FindBy(css="span#Medical_Icd10UserControl_icd10Label")
	public WebElement icd10MessageLabel;	
	
	//Icd10 Code already Exists. Please select another one
	
	@FindBy(id = "Medical_saveButton")
	public WebElement saveButton;
	
	@FindBy(id = "Medical_saveMessageLabel")
	public WebElement messageLabel;
	
	
	
	@FindBy(css="div#Medical_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(3)")
	public WebElement incidentDateValue;

	@FindBy(css="div#Medical_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	public WebElement claimStatusLabel;
	
	@FindBy(css="input#Medical_cancelButton")
	public WebElement cancelButton;
	
	@FindBy(css="input#Medical_physicianDataList_ctl01_changeDateTextBox")
	public WebElement changeDate;
	
	//@FindBy(css="input#Medical_Icd9UserControl_existingIcd9Repeater_ctl01_deleteImageButton")
	@FindBy(css="[id^='Medical_Icd9UserControl'][id$='_deleteImageButton']")//tr[id^="section_"][id$="_dummy"]
	public List<WebElement> deleteICD9Code;
	
	//@FindBy(css="input#Medical_Icd10UserControl_existingIcd10Repeater_ctl01_deleteImageButton")
	@FindBy(css="[id^='Medical_Icd10UserControl'][id$='_deleteImageButton']")//tr[id^="section_"][id$="_dummy"]
	public List<WebElement> deleteICD10Code;
	
	@FindBy(css="span#Medical_Icd9UserControl_noExistingIcd9Label")
	public WebElement noICD9CodeMessage;
	
	@FindBy(css="span#Medical_Icd10UserControl_noExistingIcd10Label")
	public WebElement noICD10CodeMessage;
	
	@FindBy(css="div#Medical_SaveValidationSummary>ul>li")
	public List<WebElement> listOfErrorMessages;
	
	@FindBy(id = "openClaimImageButton")
	WebElement openClaimIcon;
	
	@FindBy(css = "input#closeClaimImageButton")
	WebElement closeClaimIcon;
	
	@FindBy(id="btnClose")
	WebElement closedBtnForOpenDiaries;
	
	String medicalTreatmentPage,DiaryPage;
	
	@FindBy(id="Medical_isTreatmentRequired_1")
	WebElement isTreatmentRequired;
	
	public String getMessageText(){
		return messageLabel.getText();
	}
	
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public void openStatusOfClaim(HashMap<String, String> hm){
		if(hm.get("StatusChange").contains("Icon")){
			BaseClass.click(openClaimIcon);
			BaseClass.logInfo("Clicked on Open claim icon", "");
			BaseClass.acceptalert();
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			BaseClass.logInfo("Alert accepted for opening the claim", "");
			
		}
			Assert.assertEquals(BaseClass.gettext(messageLabel), "The claim was opened successfully.");
			BaseClass.logInfo("Success Message should be displayed", "The claim was opened successfully.");
			
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains( "Open"));
		BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
	
	}
	
	public void reClosedStatusOfClaim(HashMap<String, String> hm){
		DashboardPage.getinstance().navigateToFinancialSummary();
		Wait.waitForTitle(BaseClass.getDriver(), "Financial Summary");
		BaseClass.logInfo("Navigate to Financial Summary Screen", "");
		// or can be navigate to financial summary screen
		// if pending have value - then pending error message will display
		// if paid & reserves have value then outstanding reserves error message will display
		
		HashMap<String, String> financialSummaryTotal = FinancialSummaryPage.getinstance().getTotalAmountList();
		DashboardPage.getinstance().navigateToMedicalTreatment(hm);
		Wait.waitForTitle(BaseClass.getDriver(), "Medical Treatment");
		BaseClass.logInfo("Navigate to Medical Treatment Screen again", "");
		medicalTreatmentPage=BaseClass.getcurrentwindow();
		System.out.println("hm: "+Arrays.asList(hm));
		if(hm.get("StatusChange").contains("Icon")){
			
			BaseClass.click(closeClaimIcon);
			BaseClass.logInfo("Clicked on close claim icon", "");
			
			
		}
		BaseClass.acceptalert();
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		BaseClass.logInfo("Alert for closing the claim is accepted", "");
		
		
		if(!(financialSummaryTotal.get("Paid").contains("$0.00")
				|| financialSummaryTotal.get("OS Res.").contains("$0.00")))
		{
			//outstanding reserves error message will display
			//need to put assert condition
			BaseClass.logInfo("Getting outstanding reserve error message while closing the claim", "");
			BaseClass.logInfo("Claim having OS Reserve amount: ", financialSummaryTotal.get("OS Res."));
			BaseClass.logInfo("Claim having Paid amount: ", financialSummaryTotal.get("Paid"));
		}
		
		else if ((!financialSummaryTotal.get("Pending").contains("$0.00"))
				|| hm.get("PaymentStatus").equals("7")
				||((!financialSummaryTotal.get("Paid").contains("$0.00"))
						&&(financialSummaryTotal.get("Pending").contains("$0.00")) 
								&& (financialSummaryTotal.get("OS Res.").contains("$0.00")))){
			//pending error message will display
			//need to put assert condition
			System.out.println("Entered in Pending condition verification");
			BaseClass.logInfo("Getting pending payment error message while closing the claim", "");
			BaseClass.logInfo("Claim having Pending amount: ", financialSummaryTotal.get("Pending"));
		}
		else {
			//open diaries pop-up handling
			if(BaseClass.getDriver().getWindowHandles().size()>1)
			{
				DiaryPage=BaseClass.switchToNextWindow(medicalTreatmentPage,"Open Diaries", 2);
				BaseClass.logInfo("Diary Page is displayed", "");
				BaseClass.click(closedBtnForOpenDiaries);
				BaseClass.logInfo("Diaries pop-up is closed successfully", "");
				BaseClass.switchToWindow(medicalTreatmentPage,1);
			}
			Assert.assertEquals(BaseClass.gettext(messageLabel), "The claim was closed successfully.");
			BaseClass.logInfo("Success Message should be displayed", "The claim was closed successfully.");
			
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			System.out.println(BaseClass.gettext(claimStatusLabel));
			Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains("Closed")) ;
			BaseClass.logpass("Successfully closed the claim", "Successfully closed the claim");
		}
	
	}
	
	public void claimStatusVerification(HashMap<String, String> hm)
	{
		if(hm.get("Scenario").contains("ReOpened")){
			if(BaseClass.gettext(claimStatusLabel).contains("Closed") ||
					BaseClass.gettext(claimStatusLabel).contains("ReClosed"))
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			}
				
		}
		else if(hm.get("Scenario").contains("ReClosed") ){
			if(BaseClass.gettext(claimStatusLabel).contains("ReOpened")){
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Closed")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
				
		}
		
		else if(hm.get("Scenario").contains("Closed")){
			if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				
			}
			
		}
		else if(hm.get("Scenario").contains("Open")){
			openStatusOfClaim(hm);
			}
	}
	
	public void cancelButtonVerify() 
	{
		if(treatmentRequired.get(0).isSelected())
		{
			BaseClass.logInfo("Treatment required is set as Yes", "");
			BaseClass.click(treatmentRequired.get(1));
			BaseClass.logInfo("Clicked on Treatment required as No", "");
			BaseClass.click(cancelButton);
			BaseClass.logInfo("Clicked on cancel button", "");
			try{
				Wait.waitforelement(BaseClass.getDriver(), treatmentRequired.get(0));
				Wait.waitForIsSelected(BaseClass.getDriver(), treatmentRequired.get(0));}
				catch(org.openqa.selenium.StaleElementReferenceException ex)
				{
					Wait.waitforelement(BaseClass.getDriver(), treatmentRequired.get(0));
					Wait.waitForIsSelected(BaseClass.getDriver(), treatmentRequired.get(0));
				}
			BaseClass.logInfo("Treatment required is set as Yes after clicked on cancel button: ", "");
			Assert.assertEquals(treatmentRequired.get(0).isSelected(),true);
		}
		else {
			BaseClass.logInfo("Treatment required is set as No", "");
			BaseClass.click(treatmentRequired.get(0));
			BaseClass.logInfo("Clicked on Treatment required as Yes", "");
			BaseClass.click(cancelButton);
			BaseClass.logInfo("Clicked on cancel button", "");
			try{
			Wait.waitforelement(BaseClass.getDriver(), treatmentRequired.get(1));
			Wait.waitForIsSelected(BaseClass.getDriver(), treatmentRequired.get(1));}
			catch(org.openqa.selenium.StaleElementReferenceException ex)
			{
				Wait.waitforelement(BaseClass.getDriver(), treatmentRequired.get(1));
				Wait.waitForIsSelected(BaseClass.getDriver(), treatmentRequired.get(1));
			}
			BaseClass.logInfo("Treatment required is set as No after clicked on cancel button: ", "");
			Assert.assertEquals(treatmentRequired.get(1).isSelected(),true);
		}
		BaseClass.logpass("Verified cancel button is working as expected","");
	}
	
	public void verifyRequiredFieldValidationMessages(){
		if(treatmentRequired.get(0).isSelected())
			BaseClass.logInfo("Treatment required is set as Yes", "");
			
		else{
			BaseClass.logInfo("Treatment required is set as No", "");
			BaseClass.click(treatmentRequired.get(0));
			BaseClass.logInfo("Clicked on Treatment required as Yes", "");
		}
		BaseClass.selectByIndex(initialTreatmentType,0);
		BaseClass.logInfo("Initial Treatment Type is selcted as: ", BaseClass.getfirstselectedoption(initialTreatmentType));
		BaseClass.selectByIndex(firstTreatmentType,0);
		BaseClass.logInfo("Treatment type is selcted as: ", BaseClass.getfirstselectedoption(firstTreatmentType));
		BaseClass.clear(nameOfPhysician);
		BaseClass.logInfo("Name Of Physician is cleared out","");
		BaseClass.selectByIndex(specialtyDropdown,0);
		BaseClass.logInfo("Specialty is selcted as: ", BaseClass.getfirstselectedoption(specialtyDropdown));
		BaseClass.clear(changeReason);
		BaseClass.logInfo("Change Reason is cleared out","");
		
		BaseClass.click(saveButton);
		BaseClass.logInfo("Clicked on save button", "");
		Wait.waitforelements(BaseClass.getDriver(), listOfErrorMessages);
		List<String> temp = new ArrayList<String>();
		for(int i=0;i<listOfErrorMessages.size();i++){
		temp.add(i, listOfErrorMessages.get(i).getText());
		}
		System.out.println("List is: " + temp);
		BaseClass.logInfo("Error message displayed on medical treatment screen if trying to save without entering data in mandatory fields: ", "");
		Assert.assertTrue(temp.get(0).contains("Treatment Type Required is required."));
		BaseClass.logInfo("Initial Treatment Type  is required message displayed as : ",temp.get(0));
		Assert.assertTrue(temp.get(1).contains("Treatment Type is required."));
		BaseClass.logInfo("Treatment Type is required message displayed as : ",temp.get(1));
		Assert.assertTrue(temp.get(2).contains("Name is required."));
		BaseClass.logInfo("Name is required message displayed as : ",temp.get(2));
		Assert.assertTrue(temp.get(3).contains("Specialty is required."));
		BaseClass.logInfo("Specialty is required message displayed as : ",temp.get(3));
		Assert.assertTrue(temp.get(4).contains("Change Reason is required."));
		BaseClass.logInfo("Change Reason is required message displayed as : ",temp.get(4));
		BaseClass.logpass("Validation message displayed for required fields in medical treatment screen", "");
		
	}

	public void invalidDataValidation(){
		if(treatmentRequired.get(0).isSelected())
			BaseClass.logInfo("Treatment required is set as Yes", "");
			
		else{
			BaseClass.logInfo("Treatment required is set as No", "");
			BaseClass.click(treatmentRequired.get(0));
			BaseClass.logInfo("Clicked on Treatment required as Yes", "");
		}
		BaseClass.selectByIndex(initialTreatmentType);
		BaseClass.logInfo("Initial Treatment Type is selcted as: ", BaseClass.getfirstselectedoption(initialTreatmentType));
		BaseClass.selectByIndex(choiceDropdown);
		BaseClass.logInfo("Choice type is selcted as: ", BaseClass.getfirstselectedoption(choiceDropdown));
		BaseClass.selectByIndex(firstTreatmentType);
		BaseClass.logInfo("First Treatment type is selcted as: ", BaseClass.getfirstselectedoption(firstTreatmentType));
		BaseClass.settext(nameOfPhysician, BaseClass.stringGeneratorl(4));
		BaseClass.logInfo("Name Of Physician is set as: ", BaseClass.getattribute(nameOfPhysician, "value"));
		BaseClass.selectByIndex(specialtyDropdown);
		BaseClass.logInfo("Specialty is selcted as: ", BaseClass.getfirstselectedoption(specialtyDropdown));
		BaseClass.settext(changeReason, BaseClass.stringGeneratorl(4));
		BaseClass.logInfo("Change Reason is set as: ", BaseClass.getattribute(changeReason, "value"));
		BaseClass.selectByIndex(choiceValueDropdown);
		BaseClass.logInfo("Choice Value is selcted as: ", BaseClass.getfirstselectedoption(choiceValueDropdown));
		BaseClass.settext(pbmTerminationDate, BaseClass.stringGeneratorl(4));
		BaseClass.logInfo("PBM Termination Date is set as: ",BaseClass.getattribute(pbmTerminationDate, "value"));
		BaseClass.settext(changeDate, BaseClass.stringGeneratorl(4));
		BaseClass.logInfo("Change Date is set as: ", BaseClass.getattribute(changeDate, "value"));
		BaseClass.click(saveButton);
		BaseClass.logInfo("Clicked on save button", "");
		Wait.waitforelements(BaseClass.getDriver(), listOfErrorMessages);
		List<String> temp = new ArrayList<String>();
		for(int i=0;i<listOfErrorMessages.size();i++){
		temp.add(i, listOfErrorMessages.get(i).getText());
		}
		System.out.println("List is: " + temp);
		BaseClass.logInfo("Error message displayed on medical treatment screen if entered invalid data", "");
		Assert.assertEquals(temp.get(0), "PBMTermination Date is invalid.");
		BaseClass.logpass("Validation message displayed for invalid data in 'PBM Termination date' field", "");
		Assert.assertEquals(temp.get(1) , "Change Date is invalid.");
		BaseClass.logpass("Validation message displayed for invalid data in 'Change date' field", "");
	
	}
	
	public void medicalTreatmentRequiredYesOrNo(HashMap<String, String> hm){
		if(hm.get("Scenario").contains("TreatmentRequiredAsYes") ){
			if(treatmentRequired.get(0).isSelected())
				BaseClass.logInfo("Treatment required is set as Yes", "");
				
			else{
				BaseClass.logInfo("Treatment required is set as No", "");
				BaseClass.click(treatmentRequired.get(0));
				BaseClass.logInfo("Clicked on Treatment required as Yes", "");
			}
			BaseClass.selectByIndex(initialTreatmentType);
			BaseClass.logInfo("Initial Treatment Type is selcted as: ", BaseClass.getfirstselectedoption(initialTreatmentType));
			BaseClass.selectByIndex(choiceDropdown);
			BaseClass.logInfo("Choice type is selcted as: ", BaseClass.getfirstselectedoption(choiceDropdown));
			BaseClass.selectByIndex(firstTreatmentType);
			BaseClass.logInfo("First Treatment type is selcted as: ", BaseClass.getfirstselectedoption(firstTreatmentType));
			BaseClass.settext(nameOfPhysician, BaseClass.stringGeneratorl(4));
			BaseClass.logInfo("Name Of Physician is set as: ", BaseClass.getattribute(nameOfPhysician, "value"));
			BaseClass.selectByIndex(specialtyDropdown);
			BaseClass.logInfo("Specialty is selcted as: ", BaseClass.getfirstselectedoption(specialtyDropdown));
			BaseClass.settext(changeReason, BaseClass.stringGeneratorl(4));
			BaseClass.logInfo("Change Reason is set as: ", BaseClass.getattribute(changeReason, "value"));
			BaseClass.selectByIndex(choiceValueDropdown);
			BaseClass.logInfo("Choice Value is selcted as: ", BaseClass.getfirstselectedoption(choiceValueDropdown));
			String incidentDate = BaseClass.gettext(incidentDateValue).substring(BaseClass.gettext(incidentDateValue).indexOf(':')+1);
			String pbmDate = BaseClass.futuredatefromdate(incidentDate, 10);
			System.out.println("newHireDate: "+pbmDate);
			BaseClass.settext(pbmTerminationDate, pbmDate);
			BaseClass.logInfo("PBM Termination Date is set as: ",BaseClass.getattribute(pbmTerminationDate, "value"));
			BaseClass.settext(changeDate, pbmDate+1);
			BaseClass.logInfo("Change Date is set as: ", BaseClass.getattribute(changeDate, "value"));
			
			BaseClass.click(saveButton);
			BaseClass.logInfo("Clicked on save button", "");
			Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
			BaseClass.logpass("Success Message should be displayed", "Medical Treatment required as Yes is set.");
			
		}
		else if(hm.get("Scenario").contains("TreatmentRequiredAsNo") ){
			if(treatmentRequired.get(1).isSelected())
				BaseClass.logInfo("Treatment required is set as No", "");
				
			else{
				BaseClass.logInfo("Treatment required is set as Yes", "");
				BaseClass.click(treatmentRequired.get(1));
				BaseClass.logInfo("Clicked on Treatment required as No", "");
			}
			String incidentDate = BaseClass.gettext(incidentDateValue).substring(BaseClass.gettext(incidentDateValue).indexOf(':')+1);
			String pbmDate = BaseClass.futuredatefromdate(incidentDate, 10);
			System.out.println("newHireDate: "+pbmDate);
			BaseClass.settext(pbmTerminationDate, pbmDate);
			BaseClass.logInfo("PBM Termination Date is set as: ",BaseClass.getattribute(pbmTerminationDate, "value"));
			BaseClass.click(saveButton);
			BaseClass.logInfo("Clicked on save button", "");
			Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
			BaseClass.logpass("Success Message should be displayed", "Medical Treatment required as No is set.");
			
			
		}
	}
	
	public void addICDCode9And10(HashMap<String, String> hm){
		if(addICD9CodeButton.size()>0)
		{
			BaseClass.logInfo("ICD9 code button is present", "");
			List<String> resultSetList = new ArrayList<String>();
			resultSetList = BaseClass.getCompletequeryresult("select top 3 icd.Code from claim c"+
					"join claimant cl on c.claimid = cl.claimid"+
					"join ClaimantIcd9 cicd on cicd.ClaimantId = cl.ClaimantId"+
					"join Icd10 icd on cicd.ClaimantIcd9Code <> icd.Code"+
					"where c.ClaimNumber like '%"+hm.get("ClaimNumber")+"%'");
			
			if(resultSetList.isEmpty())
			{
				BaseClass.logInfo("No ICD9 code already present in claim", "");
				resultSetList = BaseClass.getCompletequeryresult("select top 3 code from Icd9 where IsDeleted is null");
				if(resultSetList.isEmpty())
				{
					BaseClass.logInfo("No ICD9 code present in database", "");
				}
			}
			System.out.println(resultSetList);
			if(!resultSetList.isEmpty())
				{
				int loopcount =0;
			
				while(loopcount <3)
				{
					BaseClass.click(addICD9CodeButton.get(0));
					BaseClass.logInfo("Clicked on ICD9 code button", "");
					BaseClass.settext(searchICD9CodeTextBox, resultSetList.get(loopcount));
					BaseClass.logInfo("Search for ICD9 code: ", resultSetList.get(loopcount));
					BaseClass.click(searchButton_ICD9Code);
					BaseClass.logInfo("Clicked on search button of ICD9 code ", "");
					/*String beforeSelectedICD = BaseClass.gettext(selectedValueOfIDC10Code);
					BaseClass.logInfo("ICD10 Code about to select: " , beforeSelectedICD);
					*/BaseClass.click(selectButton_ICD9Code);
					BaseClass.logInfo("Clicked on select button of ICD9 code ", "");
					String afterSelectedICD = BaseClass.gettext(valueOfICD9Code_Displayed.get(loopcount));
					BaseClass.logInfo("Selected ICD9 Code: " , afterSelectedICD);
					Assert.assertEquals(afterSelectedICD.trim(), resultSetList.get(loopcount).trim());
					BaseClass.logInfo("ICD9 code added successfully " , afterSelectedICD);
					loopcount++;
				}
			}
		}
		if(addICD10CodeButton.size()>0)
		{
			BaseClass.logInfo("ICD10 code button is present", "");
			List<String> resultSetList = new ArrayList<String>();
			resultSetList = BaseClass.getCompletequeryresult("select top 3 icd.Code from claim c"+
					"join claimant cl on c.claimid = cl.claimid"+
					"join ClaimantIcd10 cicd on cicd.ClaimantId = cl.ClaimantId"+
					"join Icd10 icd on cicd.ClaimantIcd10Code <> icd.Code"+
					"where c.ClaimNumber like '%"+hm.get("ClaimNumber")+"%'");
			
			if(resultSetList.isEmpty())
			{
				BaseClass.logInfo("No ICD 10code already present in claim", "");
				resultSetList = BaseClass.getCompletequeryresult("select top 3 code from Icd10 where IsDeleted is null");
				if(resultSetList.isEmpty())
				{
					BaseClass.logInfo("No ICD 10code present in database", "");
				}
			}
			System.out.println(resultSetList);
			if(!resultSetList.isEmpty())
				{
				int loopcount =0;
			
				while(loopcount <3)
				{
					BaseClass.click(addICD10CodeButton.get(0));
					BaseClass.logInfo("Clicked on ICD code 10 button", "");
					BaseClass.settext(searchICD10CodeTextBox, resultSetList.get(loopcount));
					BaseClass.logInfo("Search for ICD10 code: ", resultSetList.get(loopcount));
					BaseClass.click(searchButton_ICD10Code);
					BaseClass.logInfo("Clicked on search button of ICD code 10", "");
					/*String beforeSelectedICD = BaseClass.gettext(selectedValueOfIDC10Code);
					BaseClass.logInfo("ICD10 Code about to select: " , beforeSelectedICD);
					*/BaseClass.click(selectButton_ICD10Code);
					BaseClass.logInfo("Clicked on select button of ICD code 10", "");
					String afterSelectedICD = BaseClass.gettext(valueOfICD10Code_Displayed.get(loopcount));
					BaseClass.logInfo("Selected ICD10 Code: " , afterSelectedICD);
					Assert.assertEquals(afterSelectedICD.trim(), resultSetList.get(loopcount).trim());
					BaseClass.logInfo("ICD 10 code added successfully " , afterSelectedICD);
					loopcount++;
				}
			}
		}
		BaseClass.click(saveButton);
		BaseClass.logInfo("Clicked on save button", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "ICD codes are added successfully");
		
		
	}
	public void deleteICDCode10(HashMap<String, String> hm){
		if(valueOfICD10Code_Displayed.isEmpty()){
			addICDCode9And10(hm);
		}
		int addedICDCodeInClaim = valueOfICD10Code_Displayed.size();
		System.out.println("Claim having number of ICD code added:"+ addedICDCodeInClaim);
		int indexSelectedForDeletion = BaseClass.generateRandomNumberAsInteger(addedICDCodeInClaim);
		String beforeDeletingCode = BaseClass.gettext(valueOfICD10Code_Displayed.get(indexSelectedForDeletion));
		BaseClass.logInfo("Selecting value for deletion is : ",beforeDeletingCode);
		WebElement temp = valueOfICD10Code_Displayed.get(indexSelectedForDeletion);

		BaseClass.click(deleteICD10Code.get(indexSelectedForDeletion));
		BaseClass.logInfo("Clicked on delete icon", "");
		BaseClass.acceptalert();
		Wait.waitForElementdisappear(temp);
		BaseClass.logInfo("Alert is accepted for deleting ICD10 code", "");
		List<String> ICD10Codes = new ArrayList<String>();
		for(WebElement e: valueOfICD10Code_Displayed)
			 ICD10Codes.add(e.getText());
		Assert.assertFalse(ICD10Codes.contains("beforeDeletingCode"));
		//Assert.assertFalse(valueOfICD10Code_Displayed.contains(temp));
		BaseClass.click(saveButton);
		BaseClass.logInfo("Clicked on save button", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "ICD code is deleted successfully");
		
	}
	
}