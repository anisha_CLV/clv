package ClaimVisionPages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class PostalCheckReversalPage {

	private static final PostalCheckReversalPage postalCheckReversal = new PostalCheckReversalPage();

	private PostalCheckReversalPage() {
	}

	public void initElement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public static PostalCheckReversalPage getinstance() {
		return postalCheckReversal;
	}

	@FindBy(id = "PostedCheckReversal_ClaimNumberTextBox")
	WebElement claimNumber;

	@FindBy(id = "PostedCheckReversal_SearchButton")
	WebElement searchButton;

	@FindBy(id = "PostedCheckReversal_checkReversePager_lbtnLast")
	WebElement last;

	@FindBy(xpath = "//input[contains(@id,'ReverseCheckBox')]")
	List<WebElement> reversalCheckBox;

	@FindBy(xpath = "//select[contains(@id,'ReasonDropDown')]")
	List<WebElement> reversalReason;

	@FindBy(xpath = "//span[contains(@id,'claimNumberLabel')]")
	List<WebElement> claimNumberLabel;

	@FindBy(id = "PostedCheckReversal_saveButton")
	WebElement saveButton;

	@FindBy(id = "PostedCheckReversal_BatchReverseButton")
	WebElement batchReverseButton;

	@FindBy(id = "ReasonDropDown")
	WebElement batchReverseReason;

	@FindBy(id = "CorrectionCodeDropDown")
	WebElement batchReverseCorrectionCode;
	@FindBy(id = "OkButton")
	WebElement batchReverseOkButton;

	@FindBy(id = "PostedCheckReversal_messageLabel")
	WebElement SuccessMessage;

	@FindBy(id = "PostedCheckReversal_searchResultLabel")
	WebElement noResult;

	@FindBy(id = "PostedCheckReversal_checkReversePager_lblResults")
	WebElement records;

	public String reversePayment() {
		return saveReversePayment();
	}

	public String reversePayment(String ClaimNumber) {
		BaseClass.settext(claimNumber, ClaimNumber);
		BaseClass.logInfo("ClaimNumber is set as", ClaimNumber);
		return saveReversePayment();
	}

	public void batchReversal(String ClaimNumber) {
		BaseClass.settext(claimNumber, ClaimNumber);
		BaseClass.logInfo("ClaimNumber is set as", ClaimNumber);
		BaseClass.click(searchButton);
		BaseClass.logInfo("Search Button is Clicked", "");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		BaseClass.logInfo("Records are Searched Successfully", "");
		String parent = BaseClass.getcurrentwindow();
		BaseClass.click(batchReverseButton);
		BaseClass.switchToNextWindow(parent, "Batch Reverse", 2);
		BaseClass.selectByIndex(batchReverseReason);
		BaseClass.logInfo("Batch Reversal Reson is selected as", BaseClass.getfirstselectedoption(batchReverseReason));
		BaseClass.selectByIndex(batchReverseCorrectionCode);
		BaseClass.logInfo("Batch Reversal Correction Code is selected as",
				BaseClass.getfirstselectedoption(batchReverseCorrectionCode));
		BaseClass.click(batchReverseOkButton);
		BaseClass.logInfo("Batch Reversal Ok Button is Clicked", "");
		BaseClass.switchToWindow(parent, 1);
		Wait.waitFortextPresent(BaseClass.getDriver(), SuccessMessage);
		Assert.assertEquals(BaseClass.gettext(SuccessMessage), "Batch has been reversed");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		Assert.assertEquals(BaseClass.gettext(noResult), "No results returned.");
		BaseClass.logpass("All Payments should be Reversed", "All payments are Reversed");

	}

	private String saveReversePayment() {
		BaseClass.click(searchButton);
		BaseClass.logInfo("Search Button is Clicked", "");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		BaseClass.logInfo("Records are Searched Successfully", "");
		if (last.isEnabled()) {
			BaseClass.click(last);
			Wait.waitFor(10);
		}
		BaseClass.click(reversalCheckBox.get(reversalCheckBox.size() - 1));
		BaseClass.logInfo("Reverse CheckBox is selected", "");
		BaseClass.selectByIndex(reversalReason.get(reversalReason.size() - 1));
		BaseClass.logInfo("Reversal Reason is selected as:",
				BaseClass.getfirstselectedoption(reversalReason.get(reversalReason.size() - 1)));
		String ClaimNumber = BaseClass.gettext(claimNumberLabel.get(claimNumberLabel.size() - 1));
		BaseClass.click(saveButton);
		BaseClass.logInfo("Save Button is Clicked", "");
		Wait.waitFortextPresent(BaseClass.getDriver(), SuccessMessage);
		Assert.assertEquals(BaseClass.gettext(SuccessMessage), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		List<String> paymentDetails = BaseClass
				.getqueryresult("Select TOP 1 ClaimPaymentId,PaymentStatusId  from ClaimPayment where ClaimNumber='"
						+ ClaimNumber + "' order by UpdateDate desc");
		Assert.assertEquals(BaseClass
				.getqueryresult(
						"Select PaymentStatusCode from Paymentstatus where PaymentStatusid=" + paymentDetails.get(1))
				.get(0), "Voided");
		BaseClass.logpass("Payment should be changed to Voided Status",
				"PaymentID " + paymentDetails.get(0) + " changed to Voided Status");
		return paymentDetails.get(0);

	}

}
