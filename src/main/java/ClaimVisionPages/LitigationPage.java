package ClaimVisionPages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LitigationPage {
	private LitigationPage(){}
	private static final LitigationPage litigation=new LitigationPage();

	public static LitigationPage getinstance()
	{
		return litigation;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "a#claimsTreeViewt11")
	public WebElement litigationMenu;
	
	@FindBy(css = "select#Litigation_LitigationStatusDropDownList")
	public WebElement statusDropDown;
	
	@FindBy(css = "input#Litigation_attorneyRepresentedDateTextBox")
	public WebElement attorneyRepresentedDate;
	
	@FindBy(css = "input#Litigation_dateSuitFiledTextBox")
	public WebElement dateSuitFiled;
	
	@FindBy(css = "input#Litigation_dateCaseClosedTextBox")
	public WebElement dateCaseClosed;
	
	@FindBy(css = "input#Litigation_dateServedTextBox")
	public WebElement dateServed;
	
	@FindBy(css = "select#Litigation_disputeTypeDropDown")
	public WebElement disputeType;
	
	@FindBy(css = "input#Litigation_disputeNameTextBox")
	public WebElement disputeName;
	
	@FindBy(css = "input#Litigation_trialDateTextBox")
	public WebElement trialDate;
	
	@FindBy(css = "input#Litigation_demandAmountTextBox")
	public WebElement demandAmount;
	
	@FindBy(css = "input#Litigation_demandDateTextBox")
	public WebElement demandDate;
	
	@FindBy(css = "input#Litigation_authorityAmountTextBox")
	public WebElement authorityLevel;
	
	@FindBy(css = "input#Litigation_authorityDateTextBox")
	public WebElement authorityDate;
	
	@FindBy(css = "input#Litigation_fundingDateTextBox")
	public WebElement fundingDate;
	
	@FindBy(css = "input#Litigation_settlementAmountTextBox")
	public WebElement settlementAmount;
	
	@FindBy(css = "input#Litigation_settlementDateTextBox")
	public WebElement settlementDate;
	
	@FindBy(css = "select#Litigation_settlementTypeDropDown")
	public WebElement settlementType;
	
	@FindBy(css = "select#Litigation_findingDropDown")
	public WebElement judgementFindingDropDown;
	
	@FindBy(css = "input#Litigation_amountAwardedTextBox")
	public WebElement judgementAmountAwarded;
	
	@FindBy(css = "input#Litigation_judgmentDateTextBox")
	public WebElement judgementDate;
	
	@FindBy(css = "input#Litigation_perdisTextBox")
	public WebElement permanentDisability;
	
	@FindBy(css = "input#Litigation_litigationAppearancesRepeater_ctl01_litigationAppearancesTimeTextBox")
	public WebElement time;
	
	@FindBy(css = "table#Litigation_radLitigationType > tbody > tr > td:nth-child(1) > label")
	public WebElement settlementRadioBtn;
	
	@FindBy(css = "table#Litigation_radLitigationType > tbody > tr > td:nth-child(2) > label")
	public WebElement judgementRadioBtn;
	
	@FindBy(css = "input#Litigation_CaseSubStatusImage")
	public WebElement caseSubStatusExpandIcon;
	
	@FindBy(css = "input#Litigation_CaseSubStatusRepeater_ctl01_CaseSubStatusTypeLineNumberTextBox")
	public WebElement caseSubStatusLineNum1;
	
	@FindBy(css = "input#Litigation_CaseSubStatusRepeater_ctl02_CaseSubStatusTypeLineNumberTextBox")
	public WebElement caseSubStatusLineNum2;
	
	@FindBy(css = "select#Litigation_CaseSubStatusRepeater_ctl01_CaseSubStatusTypeStatusDropDown")
	public WebElement caseSubStatusDrpDwn;
	
	@FindBy(css = "input#Litigation_CaseSubStatusRepeater_ctl01_CaseSubStatusTypeStatusDateTextBox")
	public WebElement caseSubStatusDate;
	
	@FindBy(css = "input#Litigation_CaseSubStatusButton")
	public WebElement caseSubStatusAddBtn;
	
	@FindBy(css = "input#Litigation_legalTypeImage")
	public WebElement legalTypeExpandIcon;
	
	@FindBy(css = "input#Litigation_LegalTypeRepeater_ctl01_LegalTypeLineNumberTextBox")
	public WebElement legalTypeLineNum1;
	
	@FindBy(css = "input#Litigation_LegalTypeRepeater_ctl02_LegalTypeLineNumberTextBox")
	public WebElement legalTypeLineNum2;
	
	@FindBy(css = "input#Litigation_legalTypeAddButton")
	public WebElement legalTypeAddBtn;
	
	@FindBy(css = "select#Litigation_LegalTypeRepeater_ctl01_legalTypeStatusDropDown")
	public WebElement legalTypeDrpDwn;
	
	@FindBy(css = "input#Litigation_LegalTypeRepeater_ctl01_legalTypeStatusDateTextBox")
	public WebElement legalTypeDate;
	
	@FindBy(css = "input#Litigation_Image8")
	public WebElement workStatusExpandIcon;
	
	@FindBy(css = "input#Litigation_WorkStatusButton")
	public WebElement workStatusAddBtn;
	
	@FindBy(css = "input#Litigation_WorkStatusRepeater_ctl01_WorkStatusTypeLineNumberTextBox")
	public WebElement workStatusLineNum1;
	
	@FindBy(css = "input#Litigation_WorkStatusRepeater_ctl02_WorkStatusTypeLineNumberTextBox")
	public WebElement workStatusLineNum2;
	
	@FindBy(css = "select#Litigation_WorkStatusRepeater_ctl01_WorkStatusTypeStatusDropDown")
	public WebElement workStatusDrpDwn;
	
	@FindBy(css = "input#Litigation_WorkStatusRepeater_ctl01_WorkStatusTypeStatusDateTextBox")
	public WebElement workStatusDate;
	
	@FindBy(css = "input#Litigation_WorkStatusRepeater_ctl01_WorkStatusMedicalExamTextBox")
	public WebElement workStatusMedicalExam;
	
	@FindBy(css = "input#Litigation_BodyPartImgButton")
	public WebElement appearancesExpandIcon;
	
	@FindBy(css = "input#Litigation_litigationAppearancesButton")
	public WebElement appearancesAddBtn;
	
	@FindBy(css = "input#Litigation_litigationAppearancesRepeater_ctl01_litigationAppearancesLineNumberTextBox")
	public WebElement appearancesLineNum;
	
	@FindBy(css = "input#Litigation_litigationAppearancesRepeater_ctl02_litigationAppearancesLineNumberTextBox")
	public WebElement appearancesLineNum2;
	
	@FindBy(css = "input#Litigation_litigationAppearancesRepeater_ctl01_litigationAppearancesDate")
	public WebElement appearancesDate;
	
	@FindBy(css = "select#Litigation_litigationAppearancesRepeater_ctl01_litigationAppearancesPlaceDropDown")
	public WebElement appearancesVenue;
	
	@FindBy(css = "select#Litigation_litigationAppearancesRepeater_ctl01_litigationAppearanceTypeDropdown")
	public WebElement appearancesType;
	
	@FindBy(css = "input#Litigation_litigationAppearancesRepeater_ctl01_litigationAppearancesJudgeTextBox")
	public WebElement appearancesJudge;
	
	@FindBy(css = "input#Litigation_saveButton")
	public WebElement saveBtn;
	
	@FindBy(css = "input#Litigation_CloseButton")
	public WebElement cancelBtn;
	
	@FindBy(css = "input#saveClaimImageButton")
	public WebElement saveIcon;
	
	@FindBy(css = "div#Litigation_SaveValidationSummary > ul > li")
	public List<WebElement> listOfErrorMessages;
	
	@FindBy(css = "input#Litigation_courtTextBox")
	public WebElement courtName;
	
	@FindBy(css = "span#Litigation_messageLabel")
	public WebElement saveMessage;
	
	@FindBy(css="table#Litigation_DefenseAttorneyRadioButtonList > tbody > tr > td")
	public List<WebElement> listOfDefenseAttorney;
	
	@FindBy(css="tr#Litigation_trExternalCounsel > td:nth-child(2)")
	public WebElement externalCounselDefenseFirm;

	@FindBy(css="img#defenseFirmImg")
	public WebElement defenseFirmImage;
	
	@FindBy(css="td#Litigation_tdAttorneyName")
	public WebElement attorneyName;
	
	@FindBy(css="img#defenseAttorneyImg")
	public WebElement attorneyFirmImage;
	
	@FindBy(css="tr#Litigation_trInternalCounsel > td:nth-child(2)")
	public WebElement internalCounselDefenseFirm;
	
	@FindBy(css="select#Litigation_AttorneyDropDown")
	public WebElement attorneyDropDown;
	
	@FindBy(css="select#Litigation_ParalegalDropDown")
	public WebElement paralegalDropDown;
	
	@FindBy(css="select#Litigation_LegalSecretariesDropDown")
	public WebElement legalSecretariesDropDown;
	
	@FindBy(css = "input#searchButton")
	public WebElement searchButton;
	
	@FindBy(css = "table#CustomizationGrid1_CustomGridView > tbody")
	public WebElement table;
	
	@FindBy(css = "table#CustomizationGrid1_CustomGridView > tbody > tr:nth-child(2) > td:nth-child(1) > input[type='image']")
	public WebElement firstCellInVendorLookup;
	
	@FindBy(css = "input#Litigation_defenseAttorneyFirstNameTextBox")
	public WebElement attorneyFirstName;
	
	@FindBy(css = "input#Litigation_defenseAttorneyLastNameTextBox")
	public WebElement attorneyLastName;
	
	@FindBy(css = "input#vendorNameTextBox")
	public WebElement vendorName;
	
	@FindBy(css = "table#attorneyDataList > tbody > tr > td > table > tbody > tr")
	public List<WebElement> attorneyNameTable;
	
	@FindBy(css="img#plaintiffCounseImg")
	public WebElement plaintiffCounselImage;
	
	@FindBy(css = "input#Litigation_plaintiffAttorneyFirstNameTextBox")
	public WebElement plaintiffAttorneyFirstName;
	
	@FindBy(css = "input#Litigation_plaintiffAttorneyLastNameTextBox")
	public WebElement plaintiffAttorneyLastName;
	
	@FindBy(css="img#plaintiffAttorneyImg1")
	public WebElement plaintiffAttorneyImage;
	
	@FindBy(css = "span#Litigation_verdictAppealledRadioList>input")
	public List<WebElement> verdictAppealledRadioList;

	@FindBy(css = "input#Litigation_appealDateTextBox")
	public WebElement dateOfAppeal;

	@FindBy(css = "select#Litigation_appellateVenueDropDown")
	public WebElement appellateVenue;

	@FindBy(css = "input#Litigation_appellateCaseTextBox")
	public WebElement appellateCase;

	@FindBy(css = "textarea#Litigation_appealDisputeTextBox")
	public WebElement appealDispute;
	
	@FindBy(css = "img#appealDisputeImageButton")
	public WebElement appealDisputeSearchIcon;
	
	@FindBy(css = "input#CloseButton")
	public WebElement CloseBtn;
	
	@FindBy(css = "input#closeButton")
	public WebElement counselCloseBtn;
	
	@FindBy(css = "span#Litigation_coDefendantsRadioList>input")
	public List<WebElement> coDefendantsRadioList;
	
	@FindBy(css = "input#Litigation_ctl77_FirstNameTextBox")
	public WebElement coDefendantsFirstName;
	
	@FindBy(css = "input#Litigation_ctl77_counselTextBox")
	public WebElement coDefendantsCounsel;
	
	@FindBy(css = "img#counseImg")
	public WebElement coDefendantsCounselIcon;
	
	@FindBy(css = "input#Litigation_saveNewButton")
	public WebElement coDefendantsButton;
	
	@FindBy(css = "input#Litigation_ctl78_FirstNameTextBox")
	public WebElement coDefendantsAdditionalFirstName;
	
	@FindBy(css = "textarea#Litigation_ctl77_codefDisputeTextBox")
	public WebElement coDefendantsDispute;
	
	@FindBy(css = "img#coDefDisputeTextBoxImageButton")
	public WebElement coDefendantsDisputeIcon;
	
	@FindBy(css = "input#Litigation_ctl77_apportionmentFaultTextBox")
	public WebElement coDefendantsApportionmentFault;
	
	@FindBy(css = "div#Litigation_codefendantDiv > table > tbody > tr:nth-child(1) > td")
	public List<WebElement> coDefendantsFields;
	
	@FindBy(css = "textarea#Litigation_ctl77_codefDisputeTextBox")
	public WebElement coDefDispute;
	
	@FindBy(css = "input#Litigation_newButton")
	public WebElement newbtn;
	
	@FindBy(css = "input#Litigation_isTPOCheckBox")
	public WebElement TPOCheckBox;
	
	
	@FindBy(css = "span#Litigation_messageLabel")
	public WebElement TPOMessageLabel;
	
	
	@FindBy(xpath = "(//*[@id='CustomizationGrid1_searchResultDiv']//td/input)[1]")
	public WebElement vendorSearchList1;
}
