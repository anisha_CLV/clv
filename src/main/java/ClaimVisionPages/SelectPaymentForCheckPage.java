package ClaimVisionPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class SelectPaymentForCheckPage {
	private SelectPaymentForCheckPage(){}
	private static final SelectPaymentForCheckPage selectPaymentForCheckPage=new SelectPaymentForCheckPage();

	public static SelectPaymentForCheckPage getinstance()
	{
		return selectPaymentForCheckPage;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="SelectForCheck_bankAccountDropDown")
	WebElement bankAccount;
	
	@FindBy(id="SelectForCheck_adjusterDropDownList")
	WebElement adjuster;
	
	@FindBy(id="SelectForCheck_claimNumberTextBox")
	WebElement claimNumber;
	
	@FindBy(id="SelectForCheck_dateFromTextBox")
	WebElement releaseDateFrom;
	
	@FindBy(id="SelectForCheck_dateToTextBox")
	WebElement releaseDateTo;
	
	@FindBy(id="SelectForCheck_payCatDropDownList")
	WebElement payCategory;
	
	@FindBy(id="SelectForCheck_paySubCatDropDownList")
	WebElement paySubCategory;
	
	@FindBy(id="SelectForCheck_searchButton")
	WebElement search;
	
	@FindBy(id="SelectForCheck_clearButton")
	WebElement clear;
	
	@FindBy(id="Button1")
	WebElement selectAll;
	
	@FindBy(id="Button2")
	WebElement clearAll;
	
	@FindBy(id="SelectForCheck_SaveButton")
	WebElement save;
	
	@FindBy(id="SelectForCheck_Button3")
	WebElement revertToPending;
	
	@FindBy(xpath="//input[@type='checkbox']")
	List<WebElement> paymentcheckboxes;
	
	@FindBy(id="SelectForCheck_paymentPager_lblResults")
	WebElement records;
	

	public void verifySelectAll() {
	
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
			}
			
		}
		BaseClass.click(selectAll);
		BaseClass.logInfo("Select All Button is clicked","");
		Wait.waitFor(2);
		for(int i=0;i<paymentcheckboxes.size();i++)
		{
			BaseClass.Scrolltoelement(paymentcheckboxes.get(i));
			Assert.assertTrue(paymentcheckboxes.get(i).isSelected());
		}
		BaseClass.logpass("All Payments should be Selected", "All Payments are Selected");
		
	}
	
	
	public void verifyDeselectAll() {
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
			}
			
		}
		BaseClass.click(selectAll);
		BaseClass.logInfo("Select All Button is clicked","");
		Wait.waitFor(2);
		BaseClass.click(clearAll);
		BaseClass.logInfo("Clear All Button is clicked","");
		Wait.waitFor(2);
		for(int i=0;i<paymentcheckboxes.size();i++)
		{
			BaseClass.Scrolltoelement(paymentcheckboxes.get(i));
			Assert.assertFalse(paymentcheckboxes.get(i).isSelected());
		}
		BaseClass.logpass("All Payments should be Deselected", "All Payments are Deselected");
		
	}
	
	public void searchWithClaimNumber()
	{   
		String claimnumber=BaseClass.getqueryresult("Select TOP 1 ClaimNumber from Claimpayment where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Released' )	 order by ClaimNumber").get(0);
		BaseClass.settext(claimNumber, claimnumber);
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
			}
			
		}
	   Assert.assertTrue(BaseClass.gettext(records).contains("Results 1 - "));
       BaseClass.logpass("Payments should be displayed", "Payments are displayed");
	}
	
	public void searchWithReleaseDate()
	{  
		BaseClass.settext(releaseDateFrom, BaseClass.pastdatefromTodayDate(1500));
		BaseClass.settext(releaseDateTo, BaseClass.getCurrentDate());
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
			}
			
		}
	   Assert.assertTrue(BaseClass.gettext(records).contains("Results 1 - "));
       BaseClass.logpass("Payments should be displayed", "Payments are displayed");
	}
	
	public void searchWithPaymentCategory()
	{   
		List<String> paymentCategoryData=BaseClass.getqueryresult("Select TOP 1 pc.Description,psc.Description from ClaimPaymentDetail as cpd JOIN Finance.PaymentCategory as pc ON  cpd.PaymentCategoryId=pc.PaymentCategoryId JOIN Finance.PaymentSubCategory psc ON cpd.PaymentSubCategoryId=psc.PaymentSubCategoryId where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Released')");
		System.out.println(paymentCategoryData.get(0));
		BaseClass.selectByVisibleText(payCategory, paymentCategoryData.get(0));
		Wait.waitforOptions(paySubCategory);
		BaseClass.selectByVisibleText(paySubCategory, paymentCategoryData.get(1));
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
			}
			
		}
	   Assert.assertTrue(BaseClass.gettext(records).contains("Results 1 - "));
       BaseClass.logpass("Payments should be displayed", "Payments are displayed");
	}
	@FindBy(id="SelectForCheck_noResultsLabel")
	WebElement noResultMessage;
	public void SelectPaymenForCheck(String ClaimNumber,String PaymentID)
	{
		BaseClass.settext(claimNumber, ClaimNumber);
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
			}
			
		}
		if(PaymentID.equals(""))
		{
			BaseClass.click(selectAll);
			BaseClass.logInfo("Select All Button is clicked","");
			Wait.waitFor(2);
		}
		else
		{
		List<String> transactionID=BaseClass.getqueryresult("Select ClaimTransactionId from ClaimTransaction where ClaimPaymentid='"+PaymentID+"'");
		for(int i=0;i<transactionID.size();i++)
		{
		   BaseClass.click(BaseClass.getDriver().findElement(By.xpath("//p[contains(text(),'"+transactionID.get(i)+"')]/parent::td/parent::tr//input")));
		}
		}
		String totalrecords=BaseClass.gettext(records);
		BaseClass.click(save);
		BaseClass.logInfo("Save Button is clicked","");
		if(PaymentID.equals(""))
		{
			Wait.waitFortextPresent(BaseClass.getDriver(), noResultMessage);
			Assert.assertTrue(BaseClass.gettext(noResultMessage).equals("No results returned."));
			BaseClass.logpass("All Payments should be Released Successfully", "All Payments are Released Successfully");
		}
		else
		{
			Wait.waitFortextPresent(BaseClass.getDriver(), successMessage);
			Assert.assertEquals(BaseClass.gettext(successMessage), "Saved successfully.");
	        BaseClass.logpass("Success Message should be displayed as Saved successfully.","Success Message is displayed as Saved successfully.");  
		   String paymentStatus=BaseClass.getqueryresult("Select PaymentStatusCode from PaymentStatus where PaymentStatusId=(Select PaymentStatusId from claimpayment where claimpaymentid='"+PaymentID+"')").get(0);
	        Assert.assertEquals(paymentStatus, "Selected");	
		}
	}
	
	@FindBy(id="SelectForCheck_paymentForCheckGridView_ctl02_SelectCheckBox")
	WebElement firstPaymentCheckBox;
	@FindBy(xpath="//input[@id='SelectForCheck_paymentForCheckGridView_ctl02_SelectCheckBox']//ancestor::td/following-sibling::td/p")
	WebElement firstPaymentID;
	@FindBy(id="SelectForCheck_messageLabel")
	WebElement successMessage;
	public void revertToPending(String ClaimNumber,String PaymentID)
	{
		String transactionID="";
		BaseClass.settext(claimNumber, ClaimNumber);
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
				System.out.println("Hello");
			}
			
		}
		if(PaymentID.equals(""))
		{
			BaseClass.click(firstPaymentCheckBox);
			transactionID=BaseClass.gettext(firstPaymentID);
			
		}
		else
		{
			List<String> transactions=BaseClass.getqueryresult("Select ClaimTransactionId from ClaimTransaction where ClaimPaymentid='"+PaymentID+"'");
			for(int i=0;i<transactions.size();i++)
			{
			   BaseClass.click(BaseClass.getDriver().findElement(By.xpath("//p[contains(text(),'"+transactions.get(i)+"')]/parent::td/parent::tr//input")));
			}
			transactionID=transactions.get(0);
			}
		String totalrecords=BaseClass.gettext(records);
		BaseClass.click(revertToPending);
		BaseClass.logInfo("Revert to Pending Button is clicked","");
		//Wait.waitForTextChanged(records, totalrecords);
		Wait.waitFortextPresent(BaseClass.getDriver(), successMessage);
		Assert.assertEquals(BaseClass.gettext(successMessage), "Selected Transactions Reverted to Pending Status");
        BaseClass.logpass("Success Message should be displayed as Selected Transactions Reverted to Pending Status","Success Message is displayed as Selected Transactions Reverted to Pending Status");   
		String paymentStatus=BaseClass.getqueryresult("Select PaymentStatusCode from PaymentStatus	 where PaymentStatusId=(Select PaymentStatusId from claimpayment where claimpaymentid=(Select ClaimPaymentId from ClaimTransaction where ClaimTransactionId='"+transactionID+"'))").get(0);
        Assert.assertEquals(paymentStatus, "Pending");
        BaseClass.logpass("Payment should be Reverted to Pending Status","Payment is Reverted to Pending Status");   
		}
	
	
	

	
	

}
