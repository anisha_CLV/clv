package ClaimVisionPages;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class EmployeeIncidentPage {
	
	private EmployeeIncidentPage(){}
	private static final EmployeeIncidentPage employeeIncident=new EmployeeIncidentPage();

	public static EmployeeIncidentPage getinstance()
	{
		return employeeIncident;
	}
	
	
	@FindBy(id="EmployeeInjury_saveButton")
	WebElement employeeSave; 
	
	// hireDate
		@FindBy(id = "EmployeeInjury_hireDateTextBox")
		public WebElement hireDate;

		// hireState
		@FindBy(id = "EmployeeInjury_hireStateDropDown")
		public WebElement hireState;

	
	@FindBy(id="EmployeeInjury_saveMessageLabel")
	WebElement messageLabel; 
	
	@FindBy(css="input#saveClaimImageButton")
	WebElement saveIcon;
	
	
	@FindBy(id="EmployeeInjury_reportedEmailTextBox")
	WebElement emailAddress;
	
	@FindBy(css = "input#EmployeeInjury_reportedPhoneNumberTextBox")
	WebElement phone;
	
	@FindBy(css="div#EmployeeInjury_SaveValidationSummary > ul > li")
	List<WebElement> listOfErrorMessages;
	
	String employeeIncidentPage,DiaryPage;
	
	@FindBy(id = "EmployeeInjury_cancelButton")
	WebElement cancel;
	
	@FindBy(css = "input#openClaimImageButton")
	WebElement openClaimIcon;
	
	@FindBy(css = "input#closeClaimImageButton")
	WebElement closeClaimIcon;
	
	//#EmployeeInjury_saveMessageLabel > ul > li
	@FindBy(css = "div#EmployeeInjury_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	WebElement claimStatusLabel;
		
	@FindBy(css = "span#EmployeeInjury_saveMessageLabel > ul > li")
	WebElement pendingPaymentMsg;
	
	@FindBy(id="btnClose")
	WebElement closedBtnForOpenDiaries;
	
	// jurisdictionState
	@FindBy(id = "EmployeeInjury_jurisdictionStateDropDown")
	public WebElement jurisdictionState;
	
	@FindBy(css = "select#EmployeeInjury_stateOfEmploymentDropDown")
	public WebElement stateOfEmployment;
	
	@FindBy(css = "select#EmployeeInjury_employeeStatusDropDown")
	public WebElement employmentStatus;
	
	@FindBy(css = "select#EmployeeInjury_injuryCodeDropDown")
	public WebElement typeOfIncidentCode;
	
	@FindBy(css = "input#EmployeeInjury_alternateClaimNumberTextBox")
	public WebElement alternateClaim;
	
	@FindBy(css = "input#EmployeeInjury_stateClaimNumberTextBox")
	public WebElement stateClaim;
	
	@FindBy(css = "#EmployeeInjury_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(3)")
	public WebElement incidentDate;
	
	@FindBy(id = "EmployeeInjury_isPremisesRadioList_0")
	public WebElement isPremissedYes;
	
	@FindBy(id = "EmployeeInjury_isPremisesRadioList_1")
	public WebElement isPremissedNo;
	
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	
	/*public void reOpenedStatusOfClaim(HashMap<String, String> hm){
		BaseClass.click(openClaimIcon);
		BaseClass.logInfo("Clicked on Open claim icon", "");
		BaseClass.acceptalert();
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		BaseClass.logInfo("Alert accepted for opening the claim", "");
		//Assert.assertEquals(BaseClass.gettext(claimStatusLabel), "ReOpened");
		Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains( "ReOpened"));
		BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
	
	}*/
	
	public void openStatusOfClaim(HashMap<String, String> hm){
		if(hm.get("StatusChange").contains("Icon")){
			BaseClass.click(openClaimIcon);
			BaseClass.logInfo("Clicked on Open claim icon", "");
			BaseClass.acceptalert();
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			BaseClass.logInfo("Alert accepted for opening the claim", "");
			
		}
			Assert.assertEquals(BaseClass.gettext(messageLabel), "The claim was opened successfully.");
			BaseClass.logInfo("Success Message should be displayed", "The claim was opened successfully.");
			
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains( "Open"));
		BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
	
	}
	
	public void reClosedStatusOfClaim(HashMap<String, String> hm){
		DashboardPage.getinstance().navigateToFinancialSummary();
		Wait.waitForTitle(BaseClass.getDriver(), "Financial Summary");
		BaseClass.logInfo("Navigate to Financial Summary Screen", "");
		// or can be navigate to financial summary screen
		// if pending have value - then pending error message will display
		// if paid & reserves have value then outstanding reserves error message will display
		
		HashMap<String, String> financialSummaryTotal = FinancialSummaryPage.getinstance().getTotalAmountList();
		DashboardPage.getinstance().navigateToEmployeeIncident(hm);
		Wait.waitForTitle(BaseClass.getDriver(), "Employee/Incident");
		BaseClass.logInfo("Navigate to Employee/Incident Screen again", "");
		employeeIncidentPage=BaseClass.getcurrentwindow();
		System.out.println("hm: "+Arrays.asList(hm));
		if(hm.get("StatusChange").contains("Icon")){
			
			BaseClass.click(closeClaimIcon);
			BaseClass.logInfo("Clicked on close claim icon", "");
			
			
		}
		BaseClass.acceptalert();
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		BaseClass.logInfo("Alert for closing the claim is accepted", "");
		
		
		if((!(financialSummaryTotal.get("Paid").contains("$0.00"))
				|| (!financialSummaryTotal.get("OS Res.").contains("$0.00"))))
		{
			//outstanding reserves error message will display
			//need to put assert condition
			BaseClass.logInfo("Getting outstanding reserve error message while closing the claim", "");
			BaseClass.logInfo("Claim having OS Reserve amount: ", financialSummaryTotal.get("OS Res."));
			BaseClass.logInfo("Claim having Paid amount: ", financialSummaryTotal.get("Paid"));
		}
		
		else if ((!financialSummaryTotal.get("Pending").contains("$0.00"))
				|| hm.get("PaymentStatus").equals("7")
				||((!financialSummaryTotal.get("Paid").contains("$0.00"))
						&&(financialSummaryTotal.get("Pending").contains("$0.00")) 
								&& (financialSummaryTotal.get("OS Res.").contains("$0.00")))){
			//pending error message will display
			//need to put assert condition
			System.out.println("Entered in Pending condition verification");
			BaseClass.logInfo("Getting pending payment error message while closing the claim", "");
			BaseClass.logInfo("Claim having Pending amount: ", financialSummaryTotal.get("Pending"));
		}
		else {
			//open diaries pop-up handling
			if(BaseClass.getDriver().getWindowHandles().size()>1)
			{
				DiaryPage=BaseClass.switchToNextWindow(employeeIncidentPage,"Open Diaries", 2);
				BaseClass.logInfo("Diary Page is displayed", "");
				BaseClass.click(closedBtnForOpenDiaries);
				BaseClass.logInfo("Diaries pop-up is closed successfully", "");
				BaseClass.switchToWindow(employeeIncidentPage,1);
			}
			Assert.assertEquals(BaseClass.gettext(messageLabel), "The claim was closed successfully.");
			BaseClass.logInfo("Success Message should be displayed", "The claim was closed successfully.");
			
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			System.out.println(BaseClass.gettext(claimStatusLabel));
			Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains("Closed")) ;
			BaseClass.logpass("Successfully closed the claim", "Successfully closed the claim");
		}
	
	}
	
	/*public void claimStatusVerification(HashMap<String, String> hm)
	{
		if(hm.get("Scenario").contains("ReOpened")){
			if(BaseClass.gettext(claimStatusLabel).contains("Closed") ||
					BaseClass.gettext(claimStatusLabel).contains("ReClosed"))
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			}
				
		}
		else if(hm.get("Scenario").contains("ReClosed") ){
			if(BaseClass.gettext(claimStatusLabel).contains("ReOpened")){
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Closed")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
				
		}
		
		else if(hm.get("Scenario").contains("Closed")){
			if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				
			}
			
		}
		else if(hm.get("Scenario").contains("Open")){
			openStatusOfClaim(hm);
			}
	}
*/	
	

	public void claimStatusVerification(HashMap<String, String> hm)
	{
		if(hm.get("Scenario").contains("ReOpened")){
			if(BaseClass.gettext(claimStatusLabel).contains("Closed") ||
					BaseClass.gettext(claimStatusLabel).contains("ReClosed"))
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
				
		}
		else if(hm.get("Scenario").contains("ReClosed") ){
			if(BaseClass.gettext(claimStatusLabel).contains("ReOpened")){
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Closed")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
				
		}
		
		else if(hm.get("Scenario").contains("Closed")){
			if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				
			}
			
		}
		else if(hm.get("Scenario").contains("Open")){
			openStatusOfClaim(hm);
			}
	}
	
	
	
	public void claimStatusVerification(HashMap<String, String> hmScenario,HashMap<String, String> hmFinSum)
	{
		if(hmScenario.get("Scenario").contains("ReOpened")){
			BaseClass.click(openClaimIcon);
			BaseClass.logInfo("Clicked on Open claim icon", "");
			BaseClass.acceptalert();
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			BaseClass.logInfo("Alert accepted for opening the claim", "");
			Assert.assertEquals(BaseClass.gettext(claimStatusLabel), "ReOpened");
			BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
			
		}
		else if(hmScenario.get("Scenario").contains("ReClosed") || hmScenario.get("Scenario").contains("Closed")){
			DashboardPage.getinstance().navigateToFinancialSummary();
			hmFinSum =FinancialSummaryPage.getinstance().getTotalAmountList();
			DashboardPage.getinstance().navigateToEmployeeIncident(hmScenario);
				BaseClass.click(closeClaimIcon);
				BaseClass.logInfo("Clicked on close claim icon", "");
				BaseClass.acceptalert();
				if(!hmFinSum.get("Pending").contains("$0.00"))
				{
					Wait.waitFortextPresent(BaseClass.getDriver(), pendingPaymentMsg);
					//pending error message display
					BaseClass.logInfo("Alert for pending payments present on the claim", "");
				}
				else if(!hmFinSum.get("OS Res.").contains("$0.00"))
				{
					//outstanding reserve error message displayed
				}
				else{
					BaseClass.acceptalert();
					//send diary pop-up will appear
				}
				
						
		}
		else if(hmScenario.get("Scenario").contains("Open")){
			if(hmScenario.get("StatusChange").contains("Icon")){
				BaseClass.click(openClaimIcon);
				BaseClass.logInfo("Clicked on Open claim icon", "");
				BaseClass.acceptalert();
				Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
				BaseClass.logInfo("Alert accepted for opening the claim", "");
				Assert.assertEquals(BaseClass.gettext(claimStatusLabel), "Open");
				BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
			}
			
		}
	}
	
		
	
	
	public void invalidDataValidation(){
		
		
		BaseClass.settext(emailAddress, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered email address: ",BaseClass.getattribute(emailAddress, "value"));
		BaseClass.settext(phone, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered Phone: ",BaseClass.getattribute(phone, "value"));
		BaseClass.click(employeeSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Wait.waitforelements(BaseClass.getDriver(), listOfErrorMessages);
		List<String> temp = new ArrayList<String>();
		for(int i=0;i<listOfErrorMessages.size();i++){
		temp.add(i, listOfErrorMessages.get(i).getText());
		}
		System.out.println("List is: " + temp);
		BaseClass.logInfo("Error message displaed on employee/incident screen after entering invalid data: ", "");
		Assert.assertTrue(temp.get(1).contains("Email Address is invalid"));
		BaseClass.logInfo("Ìnvalid email address entered message displayed as : ",temp.get(0));
		Assert.assertTrue(temp.get(0).contains("Phone number has to be numeric"));
		BaseClass.logInfo("Invalid Phone number entered message displayed as : ",temp.get(1));
		BaseClass.logpass("Validation message displayed for invalid data in employee/incident screen", "");
	}
	
	
	
	public void cancelButtonVerify() 
	{
		/*String reportedEmailAddress =BaseClass.getattribute(emailAddress, "value"); 
		System.out.println(reportedEmailAddress);
		BaseClass.logInfo("Reported Email Address before entering new data: ", BaseClass.getattribute(emailAddress, "value"));
		BaseClass.settext(emailAddress, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered the Reported Email Address : ", BaseClass.getattribute(emailAddress, "value"));
		*/
		if(isPremissedYes.isSelected())
		{
			BaseClass.logInfo("Incident Occurred On The Employer's Premises is set as Yes", "");
			BaseClass.click(isPremissedNo);
			BaseClass.logInfo("Clicked on Incident Occurred On The Employer's Premises as No", "");
			BaseClass.click(cancel);
			BaseClass.logInfo("Clicked on cancel button", "");
			Wait.waitforelement(BaseClass.getDriver(), isPremissedYes);
			Wait.waitForIsSelected(BaseClass.getDriver(), isPremissedYes);
			BaseClass.logInfo("Incident Occurred On The Employer's Premises is set as Yes after clicked on cancel button: ", "");
			Assert.assertEquals(isPremissedYes.isSelected(),true);
		}
		else {
			BaseClass.logInfo("Incident Occurred On The Employer's Premises is set as No", "");
			BaseClass.click(isPremissedYes);
			BaseClass.logInfo("Clicked on Incident Occurred On The Employer's Premises as Yes", "");
			BaseClass.click(cancel);
			BaseClass.logInfo("Clicked on cancel button", "");
			Wait.waitforelement(BaseClass.getDriver(), isPremissedYes);
			Wait.waitForIsSelected(BaseClass.getDriver(), isPremissedNo);
			BaseClass.logInfo("Incident Occurred On The Employer's Premises is set as No after clicked on cancel button: ", "");
			Assert.assertEquals(isPremissedNo.isSelected(),true);
		}
		
		
		BaseClass.logpass("Verified cancel button is working as expected","");
	}
	
	public void updateEmployeeIncidentDetails(){
		BaseClass.settext(alternateClaim, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered the alternate Claim : ", BaseClass.getattribute(alternateClaim, "value"));
		BaseClass.settext(stateClaim, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered the state Claim : ", BaseClass.getattribute(stateClaim, "value"));
		BaseClass.selectByIndex(stateOfEmployment, BaseClass.generateRandomNumberAsInteger(4));
		BaseClass.logInfo("Entered the state of Employment : ", BaseClass.getattribute(stateOfEmployment, "value"));
		System.out.println("Substring after separator = "+BaseClass.gettext(incidentDate).substring(BaseClass.gettext(incidentDate).indexOf(':')+1));
		String incidentDateValue = BaseClass.gettext(incidentDate).substring(BaseClass.gettext(incidentDate).indexOf(':')+1);
		String newHireDate = BaseClass.pastdatefromdate(incidentDateValue, 30);
		System.out.println("newHireDate: "+newHireDate);
		BaseClass.settext(hireDate, newHireDate);
		BaseClass.logInfo("Claimant Hire Date is set as: ", BaseClass.getattribute(hireDate, "value"));
		BaseClass.selectByIndex(hireState);
		BaseClass.logInfo("Claimant Hire State is selected as: ", BaseClass.getfirstselectedoption(hireState));
		BaseClass.selectByIndex(jurisdictionState);
		BaseClass.logInfo("Claimant Jurisdiction State is selected as: ",
				BaseClass.getfirstselectedoption(jurisdictionState));
		BaseClass.selectByIndex(employmentStatus);
		BaseClass.logInfo("Employment Status is selected as: ", BaseClass.getfirstselectedoption(employmentStatus));
		BaseClass.selectByIndex(typeOfIncidentCode);
		BaseClass.logInfo("Type Of Incident Code is selected as: ",BaseClass.getfirstselectedoption(typeOfIncidentCode));
		
		BaseClass.click(employeeSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		
		
	}
	
	
	
		
		
			 
}
