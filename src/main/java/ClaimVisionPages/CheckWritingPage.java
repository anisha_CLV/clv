package ClaimVisionPages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class CheckWritingPage {

	private static final CheckWritingPage checkWriting = new CheckWritingPage();

	private CheckWritingPage() {
	}

	public void initElement(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public static CheckWritingPage getinstance() {
		return checkWriting;
	}

	@FindBy(id = "CheckWriting_BankAccountDropDown")
	WebElement bankAccount;

	@FindBy(id = "CheckWriting_CheckNoTextBox")
	WebElement checkNumber;
	
	

	@FindBy(id = "CheckWriting_SelectAllCheckBox")
	WebElement selectAll;

	//td[contains(text(),'130977')]
	
	//td[contains(text(),'130977')]/parent::tr/td[1]/input

	@FindBy(id = "CheckWriting_CheckPrintGridView_ctl02_SelectCheckBox")
	WebElement selectPaymentCheckBoxe;

	@FindBy(id = "CheckWriting_CheckPrintGridView_ctl02_checkAmountLabel")
	WebElement paymentAmount;
	
	@FindBy(xpath = "//input[@id='CheckWriting_CheckPrintGridView_ctl02_SelectCheckBox']/parent::td/following-sibling::td")
	WebElement transactions;

	@FindBy(id="CheckWriting_SelectMessage")
	WebElement selectMessage;
	
	@FindBy(id="CheckWriting_printButton")
	WebElement print;

	@FindBy(css="input#ContinueButton")
	public WebElement okButtonOnPopup;
	
	public void printPayment(String PaymentID)
	{
		
		List<String> transactionID=BaseClass.getqueryresult("Select ClaimTransactionId from ClaimTransaction where ClaimPaymentid='"+PaymentID+"'");
		String payAmount="", paymentStatusExpected="Sent";
		for(int i=1;i<BaseClass.getAllOptions(bankAccount).size();i++)
		{
			
			try
			{
				BaseClass.selectByIndex(bankAccount, i);
			Wait.waitforelement(BaseClass.getDriver(), selectMessage);
			if(PaymentID.equals(""))
			{
			   BaseClass.click(selectPaymentCheckBoxe);
			   payAmount=BaseClass.gettext(paymentAmount);
			   transactionID=Arrays.asList(BaseClass.gettext(transactions).split(","));
			}
			else
			{
				BaseClass.click(BaseClass.getDriver().findElement(By.xpath("//td[contains(text(),'"+transactionID.get(0)+"')]/parent::tr/td[1]/input")));			
			}

			BaseClass.logInfo("Bank Account is selected as", BaseClass.getfirstselectedoption(bankAccount));
			BaseClass.logInfo("Transaction ID is selected as", transactionID.get(0));
			Assert.assertEquals(BaseClass.gettext(selectMessage), "there is 1 check to print for "+payAmount);
			if(!BaseClass.getfirstselectedoption(bankAccount).contains("Third Party"))
			{
				BaseClass.settext(checkNumber, BaseClass.enterRandomNumber(4));
				BaseClass.logInfo("CheckNumber is set as:", BaseClass.getattribute(checkNumber, "value"));
				paymentStatusExpected="Printed";
			}
			String parent = BaseClass.getcurrentwindow();
			BaseClass.click(print);
			BaseClass.logInfo("Print button is clicked", "");
			BaseClass.switchToNextWindow(parent, "Check Print Validation", 2);
			Wait.waitFor(10);
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"//Print.exe");
			BaseClass.click(okButtonOnPopup);
			BaseClass.logInfo("Ok button is clicked on Check Print Validation", "");
			BaseClass.switchToWindow(parent, 1);
			String paymentStatus=BaseClass.getqueryresult("Select PaymentStatusCode from PaymentStatus	 where PaymentStatusId=(Select PaymentStatusId from claimpayment where claimpaymentid=(Select ClaimPaymentId from ClaimTransaction where ClaimTransactionId='"+transactionID.get(0)+"'))").get(0);
	        Assert.assertEquals(paymentStatus, paymentStatusExpected);
	        BaseClass.logpass("Payment status should be "+paymentStatusExpected,"Payment status is set as "+paymentStatusExpected);   
			break;
			}
			catch(Exception e)
			{
				if(i==BaseClass.getAllOptions(bankAccount).size()-1)
				{
					BaseClass.logskip("Payments are not present in Selected Status");
				}
			}
			
		}
	}
}
