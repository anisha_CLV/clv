package Payment;

import java.util.HashMap;

import org.testng.annotations.Test;

import ClaimVisionPages.ClaimPage;
import ClaimVisionPages.ClaimantIncidentPage;
import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.GroupSetupPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.PaymentPage;
import ClaimVisionPages.ROBPage;
import ClaimVisionPages.ReleasePaymentPage;
import ClaimVisionPages.ReservePage;
import ClaimVisionPages.SelectPaymentForCheckPage;
import ClaimVisionPages.WageWorkRTWPage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;

public class SelectPaymentForCheck {
	@Test(dataProvider = "SelectPaymentForCheck", dataProviderClass = ClaimVisionDataProvider.class)
	public void OneTimePayment(HashMap<String, String> hm) {
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		DashboardPage.getinstance().navigateToSelectPaymentForCheck();
		if(hm.get("Scenario").equals("Select All"))
		{
		SelectPaymentForCheckPage.getinstance().verifySelectAll();
		}
		if(hm.get("Scenario").equals("Clear All"))
		{
			SelectPaymentForCheckPage.getinstance().verifyDeselectAll();
		}
		if(hm.get("Scenario").equals("RevertToPending"))
		{
			SelectPaymentForCheckPage.getinstance().revertToPending(hm.get("ClaimNumber"), hm.get("PaymentID"));
		}
		if(hm.get("Scenario").equals("Save"))
		{
			SelectPaymentForCheckPage.getinstance().SelectPaymenForCheck(hm.get("ClaimNumber"), hm.get("PaymentID"));
		}
		if(hm.get("Scenario").equals("Search"))
		{
	      switch(hm.get("SearchFieldName"))
	      {
	      case "Claim":
	      {
	    	  SelectPaymentForCheckPage.getinstance().searchWithClaimNumber();
	    	  break;
	      }
	      case "Release Date":
	      {
	    	  SelectPaymentForCheckPage.getinstance().searchWithReleaseDate();
	    	  break;
	      }
	      default:
	      {
	    	  SelectPaymentForCheckPage.getinstance().searchWithPaymentCategory();
	    	  break;
	      }
	      }
		}
		
	}
}
