package Payment;

import java.util.HashMap;

import org.testng.annotations.Test;

import ClaimVisionPages.ClaimPage;
import ClaimVisionPages.ClaimantIncidentPage;
import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.GroupSetupPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.PaymentPage;
import ClaimVisionPages.ROBPage;
import ClaimVisionPages.ReleasePaymentPage;
import ClaimVisionPages.ReservePage;
import ClaimVisionPages.WageWorkRTWPage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;

public class ReleasePayment {
	@Test(dataProvider = "ReleasePayment", dataProviderClass = ClaimVisionDataProvider.class)
	public void OneTimePayment(HashMap<String, String> hm) {
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		DashboardPage.getinstance().navigateToReleasePayment();
		if(hm.get("Scenario").equals("Select All"))
		{
		ReleasePaymentPage.getinstance().verifySelectAll();
		}
		if(hm.get("Scenario").equals("Clear All"))
		{
		ReleasePaymentPage.getinstance().verifyDeselectAll();
		}
		if(hm.get("Scenario").equals("Delete"))
		{
		ReleasePaymentPage.getinstance().deletePayment(hm.get("ClaimNumber"), hm.get("PaymentID"));
		}
		if(hm.get("Scenario").equals("Search"))
		{
	      switch(hm.get("SearchFieldName"))
	      {
	      case "Adjuster":
	      {
	    	  ReleasePaymentPage.getinstance().seachByAdjuster();
	    	  break;
	      }
	      case "Claim":
	      {
	    	  ReleasePaymentPage.getinstance().searchWithClaimNumber();
	    	  break;
	      }
	      case "Release Date":
	      {
	    	  ReleasePaymentPage.getinstance().searchWithReleaseDate();
	    	  break;
	      }
	      default:
	      {
	    	  ReleasePaymentPage.getinstance().searchWithPaymentCategory();
	    	  break;
	      }
	      }
		}
		
	}
}
