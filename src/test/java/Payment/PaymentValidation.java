package Payment;


import java.util.HashMap;


import org.testng.annotations.Test;

import ClaimVisionPages.*;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;
import CommonModules.Wait;

public class PaymentValidation {

	@Test(dataProvider = "Payment", dataProviderClass = ClaimVisionDataProvider.class)
	public void OneTimePayment(HashMap<String, String> hm) {
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		if(!hm.get("Scenario").equals("Select One"))
		{
		GroupSetupPage.getinstance().setGroupPermission(hm.get("Scenario"), hm.get("Permission"));
		}
		DashboardPage.getinstance().navigateToClaimSearch();
		if(hm.get("PolicyType").contains("Compensation"))
		{
		hm.put("ClaimNumber",BaseClass.getqueryresult("Select top 10 ClaimNumber from claim where ClaimStatusCode in ('OPN','ROP')	"
				+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation') order by claimid desc").get(0));
		}
		else
		{
			hm.put("ClaimNumber",BaseClass.getqueryresult("Select TOP 10 a.ClaimNumber,b.ClaimantId from claim as a "
+"join claimant as b	 ON a.ClaimId=b.ClaimId "
+"join ClaimantSubClaim as c ON b.ClaimantId=c.ClaimantId "
+"where a.ClaimStatusCode in ('OPN','ROP') "
+"and a.PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription='"+hm.get("PolicyType")+"') "
+"and c.ClaimantId not in (Select claimantid from ClaimantSubClaim where SubClaimNumber=2 OR ClaimStatusCode not in ('OPN','ROP')) "
+"order by a.EntryDate desc").get(0));
		}
		DashboardPage.getinstance().enterClaimNumber(hm.get("ClaimNumber"));
		DashboardPage.getinstance().clickSearch();
		DashboardPage.getinstance().selectFirstClaim(hm.get("ClaimNumber"));
		if(hm.get("PolicyType").contains("Compensation"))
		{
		ClaimantIncidentPage.getinstance().updateClaimWithPermission(hm.get("Scenario"));
		WageWorkRTWPage.getinstance().updatewagerate();
		}
		else
		{
			ClaimPage.getinstance().updateClaimWithPermission(hm.get("Scenario"));
		}
		if(hm.get("PayeeType").contains("ROB"))
		{
			DashboardPage.getinstance().navigateToROB();
			ROBPage.getinstance().createROB();
		}
		if(!hm.get("CustomerCode").contains("NYLAW"))
		{
		DashboardPage.getinstance().navigateToReserve();
		ReservePage.getinstance().getReserveDataForPayment(hm);
		}
		DashboardPage.getinstance().navigateToPayment();
		PaymentPage.getinstance().createPayment(hm);
		if(!(hm.get("1stPaymentStatus").equals("Draft") || hm.get("1stPaymentStatus").equals("")))
		{
		while(!hm.get("1stPaymentStatus").contains(hm.get("PaymentStatus")))
		{
			if(hm.get("PaymentStatus").equals("Pending"))
			{
				DashboardPage.getinstance().navigateToReleasePayment();
				ReleasePaymentPage.getinstance().releasePayment(hm.get("ClaimNumber"), "");
				if(hm.get("ModeType").equals("Journal Voucher"))
				{
					hm.put("PaymentStatus","Printed");
					PaymentTransactionDetailPage.getinstance().verifyPayment(hm);
					break;
				}
				else
				{
				hm.put("PaymentStatus","Released");
				PaymentTransactionDetailPage.getinstance().verifyPayment(hm);
				}
			}
			if(hm.get("PaymentStatus").equals("Released"))
			{
				DashboardPage.getinstance().navigateToSelectPaymentForCheck();
				SelectPaymentForCheckPage.getinstance().SelectPaymenForCheck(hm.get("ClaimNumber"), "");
				hm.put("PaymentStatus","Selected");
				PaymentTransactionDetailPage.getinstance().verifyPayment(hm);
			}
			if(hm.get("PaymentStatus").equals("Selected"))
			{
				DashboardPage.getinstance().navigateTocheckWritingScreen();
				CheckWritingPage.getinstance().printPayment(BaseClass.getqueryresult("Select top 1 claimpaymentid from claimpayment where ClaimNumber='"+hm.get("ClaimNumber")+"' order by EntryDate desc").get(0));
				if(hm.get("ModeType").equals("Third Party"))
				{
				hm.put("PaymentStatus","Sent");
				}
				else
				{
					hm.put("PaymentStatus","Printed");
				}
				PaymentTransactionDetailPage.getinstance().verifyPayment(hm);
				break;
			}
		}
		}
	}


}
