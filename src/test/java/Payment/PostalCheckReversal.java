package Payment;

import java.util.HashMap;

import org.testng.annotations.Test;

import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.PaymentTransactionDetailPage;
import ClaimVisionPages.PostalCheckReversalPage;
import CommonModules.ClaimVisionDataProvider;

public class PostalCheckReversal {
	@Test(dataProvider = "PostalCheckReversal", dataProviderClass = ClaimVisionDataProvider.class)
	public void OneTimePayment(HashMap<String, String> hm) {
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		DashboardPage.getinstance().navigateToPostalCheckScreen();
		if(hm.get("Scenario").equals("Select One"))
		{
			if(!hm.get("ClaimNumber").equals(""))
			{
				PaymentTransactionDetailPage.getinstance().verifyVoidedPayment(PostalCheckReversalPage.getinstance().reversePayment(hm.get("ClaimNumber")));
			}
			else
			{
				PaymentTransactionDetailPage.getinstance().verifyVoidedPayment(PostalCheckReversalPage.getinstance().reversePayment());
			}
		}
		if(hm.get("Scenario").equals("Batch"))
		{
			PostalCheckReversalPage.getinstance().batchReversal(hm.get("ClaimNumber"));
		}
		

	}}
