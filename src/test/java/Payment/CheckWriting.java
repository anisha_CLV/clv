package Payment;

import java.util.HashMap;

import org.testng.annotations.Test;

import ClaimVisionPages.CheckWritingPage;
import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.PostalCheckReversalPage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;

public class CheckWriting {
	@Test(dataProvider = "CheckWriting", dataProviderClass = ClaimVisionDataProvider.class)
	public void OneTimePayment(HashMap<String, String> hm) {
		LoginPage.getinstance().login(hm.get("CustomerCode"), hm.get("UserName"), hm.get("Password"));
		DashboardPage.getinstance().navigateTocheckWritingScreen();
		CheckWritingPage.getinstance().printPayment("");
	}
}
