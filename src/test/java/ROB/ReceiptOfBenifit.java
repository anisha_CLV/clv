package ROB;


import java.util.HashMap;


import org.testng.annotations.Test;

import ClaimVisionPages.*;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;
import CommonModules.Wait;

public class ReceiptOfBenifit {

	@Test(dataProvider = "ROB", dataProviderClass = ClaimVisionDataProvider.class)
	public void OneTimePayment(HashMap<String, String> hm) {
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		DashboardPage.getinstance().navigateToClaimSearch();
		hm.put("ClaimNumber",BaseClass.getqueryresult("Select top 10 ClaimNumber from claim where ClaimStatusCode in ('OPN','ROP')").get(0));
		DashboardPage.getinstance().enterClaimNumber(hm.get("ClaimNumber"));
		DashboardPage.getinstance().clickSearch();
		DashboardPage.getinstance().selectFirstClaim(hm.get("ClaimNumber"));
		Wait.waitFor(3);
			DashboardPage.getinstance().navigateToROB();
			ROBPage.getinstance().validateROB(hm);
	}
}
